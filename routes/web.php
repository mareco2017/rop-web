<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->name('dashboard');

// Web
Route::group(['namespace' => 'Web', 'as' => 'web.'], function() {
	// Logged in
	Route::group(['middleware' => 'auth'], function() {
		Route::get('binary', 'BinaryController@show')->name('binary');
	});
});
	
Route::group(['namespace' => 'Auth'], function() {
	Route::post('login', 'AuthController@login')->name('login');
	Route::get('logout', 'AuthController@logout')->name('logout');
});

// Webview
Route::group(['namespace' => 'Webview', 'prefix' => 'webview', 'as' => 'webview.'], function() {
	Route::get('password/reset/{token}', 'ResetPasswordController@resetPage')->name('reset.password.page');
	Route::post('password/reset', 'ResetPasswordController@reset')->name('reset.password');

	Route::get('terms-conditions', 'AboutAppController@termsConditions')->name('terms-conditions');
	Route::get('privacy-policy', 'AboutAppController@privacyPolicy')->name('privacy-policy');
	Route::get('about-us', 'AboutAppController@aboutUs')->name('about-us');

	Route::get('/promo/{promo}', 'PromoController@detail')->name('promo-detail');

	// Logged in
	Route::group(['middleware' => 'auth.params'], function() {
		Route::get('binary', 'BinaryController@show')->name('binary');
		Route::get('user', 'AboutAppController@user')->name('user');
		Route::get('user/{any}', 'AboutAppController@user')->where('any', '.*');
	});
});

// Admin
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
	
	Route::group(['middleware' => 'guest:web-admin', 'guard' => 'web-admin'], function() {
		Route::get('login', 'AuthController@loginPage')->name('login.page');
		Route::post('login', 'AuthController@login')->name('login');
	});


	// Logged In
	Route::group(['middleware' => 'auth:web-admin', 'guard' => 'web-admin'], function() {
		Route::post('logout', 'AuthController@logout')->name('logout');

		Route::get('', 'DashboardController@index')->name('dashboard');
		
		// Admin
		Route::group(['namespace' => 'Admin', 'prefix' => 'admins', 'as' => 'admins.'], function() {
			Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
				// List
				Route::get('/', 'AdminController@index')->name('index');
				Route::post('/', 'AdminController@ajax')->name('ajax');
				
				Route::get('/new', 'AdminController@edit')->name('create');
				Route::get('/{admin}/edit', 'AdminController@edit')->name('edit');
				Route::post('/store', 'AdminController@update')->name('store');
				Route::post('/{admin}/update', 'AdminController@update')->name('update');
				Route::delete('/{admin}/delete', 'AdminController@delete')->name('delete');
			});
		});
		
		// User
		Route::group(['namespace' => 'User', 'prefix' => 'users', 'as' => 'users.'], function() {
			// User List
			Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
				Route::get('/', 'UserController@index')->name('index');
				Route::post('/', 'UserController@ajax')->name('ajax');
				Route::get('/{user}/detail', 'UserController@detail')->name('detail');
				Route::get('/new', 'UserController@edit')->name('create');
				Route::get('/{user}/edit', 'UserController@edit')->name('edit');
				Route::post('/store', 'UserController@update')->name('store');
				Route::post('/{user}/update', 'UserController@update')->name('update');
				Route::delete('/{user}/delete', 'UserController@delete')->name('delete');
			});
			// User upgrade request
			Route::group(['prefix' => 'upgrade-request', 'as' => 'upgrade-request.'], function() {
				Route::get('/', 'UpgradeRequestController@index')->name('index');
				Route::post('/', 'UpgradeRequestController@ajax')->name('ajax');
				
				Route::get('/{upgradeRequest}/edit', 'UpgradeRequestController@edit')->name('edit');
				Route::post('/{upgradeRequest}/approve', 'UpgradeRequestController@approve')->name('approve');
				Route::post('/{upgradeRequest}/decline', 'UpgradeRequestController@decline')->name('decline');
				Route::delete('/{upgradeRequest}/delete', 'UpgradeRequestController@delete')->name('delete');
			});
			// User verify identity
			Route::group(['prefix' => 'verify-identity', 'as' => 'verify-identity.'], function() {
				Route::get('/', 'VerifyIdentityController@index')->name('index');
				Route::post('/', 'VerifyIdentityController@ajax')->name('ajax');
				
				Route::get('/{user}/edit', 'VerifyIdentityController@edit')->name('edit');
				Route::post('/{user}/approve', 'VerifyIdentityController@approve')->name('approve');
				Route::post('/{user}/decline', 'VerifyIdentityController@decline')->name('decline');
				Route::delete('/{user}/delete', 'VerifyIdentityController@delete')->name('delete');
			});
			// User verify identity
			Route::group(['prefix' => 'pair', 'as' => 'pair.'], function() {
				Route::get('/', 'PairController@index')->name('index');
				Route::post('/', 'PairController@ajax')->name('ajax');
				
				Route::post('/store', 'PairController@update')->name('store');
				Route::post('/{pair}/claim', 'PairController@claim')->name('claim');
				Route::delete('/{pair}/delete', 'PairController@delete')->name('delete');
			});
		});
		// Config
		Route::group(['prefix' => 'config', 'as' => 'config.'], function() {
			Route::get('/', 'ConfigController@index')->name('index');
			Route::post('/', 'ConfigController@ajax')->name('ajax');
			Route::get('/new', 'ConfigController@edit')->name('create');
			Route::get('/{config}/edit', 'ConfigController@edit')->name('edit');
			Route::post('/store', 'ConfigController@update')->name('store');
			Route::post('/{config}/update', 'ConfigController@update')->name('update');
			Route::delete('/{config}/delete', 'ConfigController@delete')->name('delete');
		});
		// Bank
		Route::group(['prefix' => 'bank', 'as' => 'bank.'], function() {
			Route::get('/', 'BankController@index')->name('index');
			Route::post('/', 'BankController@ajax')->name('ajax');
			Route::get('/new', 'BankController@edit')->name('create');
			Route::get('/{bank}/edit', 'BankController@edit')->name('edit');
			Route::post('/store', 'BankController@update')->name('store');
			Route::post('/{bank}/update', 'BankController@update')->name('update');
			Route::delete('/{bank}/delete', 'BankController@delete')->name('delete');
		});
		// Bank Account
		Route::group(['prefix' => 'bank-account', 'as' => 'bank-account.'], function() {
			Route::get('/', 'BankAccountController@index')->name('index');
			Route::post('/', 'BankAccountController@ajax')->name('ajax');
			Route::get('/new', 'BankAccountController@edit')->name('create');
			Route::get('/{bankAccount}/edit', 'BankAccountController@edit')->name('edit');
			Route::post('/store', 'BankAccountController@update')->name('store');
			Route::post('/{bankAccount}/update', 'BankAccountController@update')->name('update');
			Route::delete('/{bankAccount}/delete', 'BankAccountController@delete')->name('delete');
		});
		// Payment
		Route::group(['prefix' => 'payment', 'as' => 'payment.'], function() {
			Route::get('/', 'PaymentController@index')->name('index');
			Route::post('/', 'PaymentController@ajax')->name('ajax');
			Route::get('/new', 'PaymentController@edit')->name('create');
			Route::post('/store', 'PaymentController@update')->name('store');
			Route::get('/statement/{payment}', 'PaymentController@confirmation')->name('confirmation');
			Route::post('/send/{payment}', 'PaymentController@sendEmail')->name('send-email');
		});
		// Top Up
		Route::group(['prefix' => 'top-up', 'as' => 'top-up.'], function() {
			Route::get('/', 'TopUpController@index')->name('index');
			Route::post('/', 'TopUpController@ajax')->name('ajax');
			Route::get('/new', 'TopUpController@edit')->name('create');
			Route::get('/{order}/edit', 'TopUpController@edit')->name('edit');
			Route::post('/store', 'TopUpController@store')->name('store');
			Route::get('/history', 'TopUpController@history')->name('history');
			Route::post('/history', 'TopUpController@historyAjax')->name('history-ajax');
			Route::get('/{order}/history', 'TopUpController@historyView')->name('history-view');
			Route::get('/{order}/confirmation', 'TopUpController@confirmation')->name('confirmation');
			Route::post('/{order}/update', 'TopUpController@update')->name('update');
			Route::post('/{order}/approve', 'TopUpController@approve')->name('approve');
			Route::post('/{order}/recheck', 'TopUpController@recheck')->name('recheck');
			Route::post('/{order}/decline', 'TopUpController@decline')->name('decline');
			Route::get('/{order}/print/{size}', 'TopUpController@print')->name('print');
		});
		// Withdraw
		Route::group(['prefix' => 'withdraw', 'as' => 'withdraw.'], function() {
			Route::get('/', 'WithdrawController@index')->name('index');
			Route::post('/', 'WithdrawController@ajax')->name('ajax');
			Route::get('/{order}/edit', 'WithdrawController@edit')->name('edit');
			Route::get('/history', 'WithdrawController@history')->name('history');
			Route::post('/history', 'WithdrawController@historyAjax')->name('history-ajax');
			Route::get('/{order}/history', 'WithdrawController@historyView')->name('history-view');
			Route::post('/store', 'WithdrawController@update')->name('store');
			Route::post('/{order}/update', 'WithdrawController@update')->name('update');
			Route::post('/{order}/approve', 'WithdrawController@approve')->name('approve');
			Route::post('/{order}/recheck', 'WithdrawController@recheck')->name('recheck');
			Route::post('/{order}/revise', 'WithdrawController@revise')->name('revise');
			Route::post('/{order}/decline', 'WithdrawController@decline')->name('decline');
			Route::get('/test', 'WithdrawController@test')->name('test');
		});

		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
			Route::get('/', 'OrderController@index')->name('index');
			Route::post('/', 'OrderController@ajax')->name('ajax');
			Route::post('/{order}/refund', 'OrderController@refund')->name('refund');
		});

		// Payment Product
		Route::group(['prefix' => 'payment-product', 'as' => 'payment-product.'], function() {
			Route::get('/', 'PaymentProductController@index')->name('index');
			Route::post('/', 'PaymentProductController@ajax')->name('ajax');
			Route::get('/new', 'PaymentProductController@edit')->name('create');
			Route::post('/store', 'PaymentProductController@update')->name('store');
			// Route::post('/import', 'PaymentProductController@import')->name('import');
			Route::get('/{paymentProduct}/edit', 'PaymentProductController@edit')->name('edit');
			Route::post('/{paymentProduct}/update', 'PaymentProductController@update')->name('update');
			Route::delete('/{paymentProduct}/delete', 'PaymentProductController@delete')->name('delete');
		});

		// Promo
		Route::group(['namespace' => 'Promo', 'prefix' => 'promos', 'as' => 'promos.'], function() {
			// Voucher
			// Route::group(['prefix' => 'voucher', 'as' => 'voucher.'], function(){
			// 	Route::get('/', 'VoucherController@index')->name('index');
			// 	Route::get('/new', 'VoucherController@edit')->name('create');
			// 	Route::get('/{promo}/push-notif', 'VoucherController@pushNotif')->name('push-notif');
			// 	Route::get('/{promo}/authorize', 'VoucherController@authorizeVoucher')->name('authorize');
			// 	Route::get('/{promo}/deauthorize', 'VoucherController@deauthorizeVoucher')->name('deauthorize');
			// 	Route::post('/store', 'VoucherController@update')->name('store');
			// 	Route::get('/{promo}/edit', 'VoucherController@edit')->name('edit');
			// 	Route::post('/{promo}/save', 'VoucherController@update')->name('update');
			// 	Route::post('/{promo}/push-notif', 'VoucherController@postPushNotif')->name('post-push-notif');
			// 	Route::post('/list', 'VoucherController@ajax')->name('ajax');
			// 	Route::delete('/{promo}/delete', 'VoucherController@delete')->name('delete');
			// });
			// Banner
			Route::group(['prefix' => 'banner', 'as' => 'banner.'], function(){
				Route::get('/', 'BannerController@index')->name('index');
				Route::get('/new', 'BannerController@edit')->name('create');
				Route::post('/store', 'BannerController@update')->name('store');
				Route::get('/{promo}/push-notif', 'BannerController@pushNotif')->name('push-notif');
				Route::get('/{promo}/edit', 'BannerController@edit')->name('edit');
				Route::post('/{promo}/save', 'BannerController@update')->name('update');
				Route::post('/{promo}/push-notif', 'BannerController@postPushNotif')->name('post-push-notif');
				Route::post('/list', 'BannerController@ajax')->name('ajax');
				Route::delete('/{promo}/delete', 'BannerController@deletePromo')->name('delete');
			});
		});
	});
});