<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1'], function() {
	Route::post('register', 'AuthController@register')->name('register');
	Route::post('refresh', 'AuthController@refresh')->name('refresh');
	Route::post('forgot-password', 'ForgotPasswordController@getResetToken')->name('forgot-password');

	// Guest
	Route::group(['middleware' => 'guest:api'], function() {
		Route::post('activate', 'AuthController@activate')->name('user.activate');
		Route::post('otp', 'AuthController@requestOTP')->name('user.request-otp');
		Route::post('verify-otp', 'AuthController@verifyOTPAndLogin')->name('user.verify-otp');
		Route::post('login', 'AuthController@login')->name('login');
	});

	// Logged In
	Route::group(['middleware' => 'auth:api'], function() {
		Route::post('logout', 'AuthController@logout')->name('logout');

		Route::get('home', 'HomeController@index')->name('home');
    Route::get('contact-us', 'HomeController@contactUs')->name('contact-us');

		// User
		Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
			Route::get('me', 'UserController@profile')->name('profile');
			Route::get('point', 'UserController@getPoint')->name('point');
			Route::get('binaries', 'UserController@binaries')->name('binaries');
			Route::post('upload-identity', 'UserController@uploadIdentity')->name('upload-identity');

			Route::post('check-password', 'UserController@checkPassword')->name('check-password');
			Route::post('change-password', 'UserController@changePassword')->name('change-password');

			Route::post('verify-phone-number', 'UserController@verifyPhoneNumber')->name('verify-phone-number');
			Route::post('resend-otp', 'UserController@resendOTP')->name('resend-otp');

			Route::post('change-email', 'UserController@changeEmail')->name('change-email');
			Route::post('change-phone-number', 'UserController@changePhoneNumber')->name('change-phone-number');
			Route::post('update-email', 'UserController@updateEmail')->name('update-email');
			Route::post('update-phone-number', 'UserController@updatePhoneNumber')->name('update-phone-number');

			Route::post('initialize-wallets', 'UserController@initializeWallets')->name('initialize-wallets');
			Route::post('set-pin', 'UserController@setPin')->name('set-pin');
		});

		// Package
		Route::group(['prefix' => 'package', 'as' => 'package.'], function() {
			Route::get('history', 'UserController@historyPackage')->name('history');
			Route::post('upgrade', 'UserController@upgradePackage')->name('upgrade');
			Route::post('upgrade/{upgradeRequest}/pay', 'UserController@payUpgradePackage')->name('upgrade.pay');
			Route::post('upgrade/{upgradeRequest}/confirm-payment', 'UserController@confirmPaymentUpgradePackage')->name('upgrade.confirm-payment');
			Route::post('upgrade/{upgradeRequest}/cancel', 'UserController@cancelUpgradePackage')->name('upgrade.cancel');
		});

		// Transaction
		Route::group(['prefix' => 'transaction', 'as' => 'transaction.'], function() {
			Route::get('owned', 'TransactionController@owned')->name('owned');
		});

		// Bank
		Route::group(['prefix' => 'bank', 'as' => 'bank.'], function() {
			Route::get('', 'BankController@list')->name('list');
		});

		// Bank Account
		Route::group(['prefix' => 'bank-account', 'as' => 'bank-account.'], function() {
			Route::get('', 'BankAccountController@list')->name('list');
			Route::get('payment', 'BankAccountController@listPayment')->name('list-payment');
			Route::post('create', 'BankAccountController@create')->name('create');
			Route::post('{bankAccount}/remove', 'BankAccountController@remove')->name('remove');
			Route::post('{bankAccount}/set-primary', 'BankAccountController@setPrimary')->name('set-primary');
		});

		// Balance
		Route::group(['prefix' => 'balance', 'as' => 'balance.'], function() {
			// Topup
			Route::get('topup-list', 'BalanceController@topupList')->name('topup-list');
			Route::post('topup', 'BalanceController@topup')->name('topup');
			Route::post('topup/{order}/paid', 'BalanceController@topupPaid')->name('topup.paid');
			Route::post('topup/{order}/cancel', 'BalanceController@topupCancel')->name('topup.cancel');

			// Withdraw
			Route::post('withdraw', 'BalanceController@withdraw')->name('withdraw');
			Route::post('bonus-withdraw', 'BalanceController@bonusWithdraw')->name('withdraw');
		});

		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
			Route::get('owned', 'OrderController@owned')->name('owned');
			Route::get('statement', 'OrderController@statement')->name('statement');
			Route::get('history', 'OrderController@history')->name('history');
			Route::get('{order}/detail', 'OrderController@detail')->name('detail');
			Route::get('{order}/ongoing-detail', 'OrderController@onGoingdetail')->name('ongoing-detail');
		});

		//PaymentProduct
		Route::group(['prefix' => 'payment', 'as' => 'payment.'], function(){
			Route::get('{productType}', 'PaymentProductController@getPaymentProductCategories')->name('get-list');
			Route::get('{productType}/{categories}', 'PaymentProductController@getPaymentProduct')->name('get-product');
			Route::get('myPayment', 'PaymentProductController@myPayment')->name('my-payment');
			Route::post('{inquiryType}/inquiry', 'PaymentProductController@inquiry')->name('inquiry');
			Route::post('{productType}/payment', 'PaymentProductController@payment')->name('payment');
		});

		//Payment
		Route::group(['prefix' => 'pay', 'as' => 'pay.'], function(){
			// Route::get('get-wallet', 'MarecoPayController@myWallet')->name('get-wallet');
			// Route::get('{business}/set-payment', 'MarecoPayController@setPayment')->name('set-payment');
			// Route::post('{business}/finish-payment', 'MarecoPayController@finishPayment')->name('finish-payment');
			// Route::post('check-voucher', 'MarecoPayController@checkingVoucher')->name('check-voucher');
			Route::post('{phoneNumber}/pay-by-phone-number', 'PayController@payByPhoneNumber')->name('pay-by-phone-number');
		});

	});
});
