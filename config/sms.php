<?php

return [
    'sid' => env('SMS_SID', ''),
    'token' => env('SMS_TOKEN', ''),
    'number' => env('SMS_NUMBER', '')
];
