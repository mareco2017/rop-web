<?php

return [
	'fastpay' => [
		// 'url' => env('BIMASAKTI_URL', 'https://partnerlink.fastpay.co.id:4343/devel/'),
		'url' => env('BIMASAKTI_URL', 'https://partnerlink.fastpay.co.id:4343/mareco/'),
		'id' => env('BIMASAKTI_ID', 'SP130570'),
		'pin' => env('BIMASAKTI_PIN', '314493')
	],
];
