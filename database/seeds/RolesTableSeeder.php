<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rootRole = Role::firstOrCreate([
            'name' => 'Root',
            'description' => 'Creator of the world',
            'redirect_on_login' => '/admin',
            'redirect_on_logout' => '/admin/login'
        ]);
        Role::firstOrCreate([
            'name' => 'Super Admin',
            'description' => 'Creator of the world',
            'redirect_on_login' => '/admin',
            'redirect_on_logout' => '/admin/login'
        ]);
        Role::firstOrCreate([
            'name' => 'Admin',
            'description' => 'Creator of the world',
            'redirect_on_login' => '/admin',
            'redirect_on_logout' => '/admin/login'
        ]);
        Role::firstOrCreate([
            'name' => 'Business Owner',
            'description' => 'Creator of the world',
            'redirect_on_login' => '/business',
            'redirect_on_logout' => '/business/login'
        ]);
        Role::firstOrCreate([
            'name' => 'User',
            'description' => 'Creator of the world',
            'redirect_on_login' => '',
            'redirect_on_logout' => '/login'
        ]);
        Role::firstOrCreate([
            'name' => 'Customer Service',
            'description' => 'Creator of the world',
            'redirect_on_login' => '/admin',
            'redirect_on_logout' => '/admin/login'
        ]);
        
        $routeList = Route::getRoutes();
        foreach ($routeList as $route) {
            $rootRole->permissions()->firstOrCreate([
                'method' => $route->methods()[0],
                'name' => $route->getName(),
                'uri' => $route->uri()
            ]);
        }
    }
}
