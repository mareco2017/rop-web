<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::updateOrCreate([
            'email' => 'wallet@gmail.com',
            'phone_number' => '+6285335566888'
        ], [
            'first_name' => 'Rich On Pay',
            'last_name' => 'Wallet',
            'email' => 'wallet@gmail.com',
            'phone_number' => '+6285335566888',
            'password' => bcrypt('qr5yk1d6'),
            'role_id' => 1
        ]);

        Admin::updateOrCreate([
            'email' => 'root@gmail.com',
            'phone_number' => '+6285335566778'
        ], [
            'first_name' => 'ROP Root',
            'last_name' => 'Admin',
            'email' => 'root@gmail.com',
            'phone_number' => '+6285335566778',
            'password' => bcrypt('qr5yk1d6'),
            'role_id' => 1
        ]);
    }
}
