<?php

use App\Models\PaymentProductType;
use Illuminate\Database\Seeder;
use App\Helpers\Enums\PaymentProductType as Type;

class PaymentProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentProductType::firstOrCreate([ 
            'product_code' => Type::PHONE,
            'method' => 'fastpay.pulsa'
        ]); 
        PaymentProductType::firstOrCreate([ 
            'product_code' => Type::POSTPAID_PHONE,
            'method' => 'fastpay.pay' 
        ]);
        PaymentProductType::firstOrCreate([ 
            'product_code' => Type::PLN,
            'method' => 'fastpay.pay'
        ]);
        PaymentProductType::firstOrCreate([ 
            'product_code' => Type::GAME, 
            'method' => 'fastpay.game' 
        ]); 
    }
}
