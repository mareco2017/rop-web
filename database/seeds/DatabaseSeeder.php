<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(ConfigsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
        $this->call(WalletsTableSeeder::class);
        $this->call(PaymentProductTypesTableSeeder::class); 
        $this->call(PaymentProductCategoriesTableSeeder::class); 
    }
}
