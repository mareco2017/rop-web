<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\BankAccount;
use App\Models\Admin;
use App\Models\Config as AppConfig;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
        $walletAdmin = Admin::find($id);

        if ($walletAdmin) {
            $walletAdmin->bankAccounts()->firstOrCreate([
        		'account_name' => 'PT. ROP GROUP INDONESIA',
        		'account_no' => '8335492222',
        		'bank_id' => 1,
        		'is_primary' => 1
        	]);
        	$walletAdmin->bankAccounts()->firstOrCreate([
        		'account_name' => 'PT. ROP GROUP INDONESIA',
        		'account_no' => '1090090278888',
        		'bank_id' => 2,
        		'is_primary' => 1
        	]);
        }

    }
}
