<?php

use App\Models\Admin;
use App\Models\Config as AppConfig;
use App\Models\Wallet;
use Illuminate\Database\Seeder;

class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Wallet::firstOrCreate([
    		'ownable_type' => 'admins',
    		'ownable_id' => 1,
    		'balance' => 0,
    		'type' => 0,
        ]);
        Wallet::firstOrCreate([
    		'ownable_type' => 'admins',
    		'ownable_id' => 1,
    		'balance' => 0,
    		'type' => 1,
        ]);
        Wallet::firstOrCreate([
    		'ownable_type' => 'admins',
    		'ownable_id' => 1,
    		'balance' => 0,
    		'type' => 2,
        ]);
        Wallet::firstOrCreate([
    		'ownable_type' => 'admins',
    		'ownable_id' => 1,
    		'balance' => 0,
    		'type' => 3,
        ]);
        Wallet::firstOrCreate([
    		'ownable_type' => 'admins',
    		'ownable_id' => 1,
    		'balance' => 0,
    		'type' => 4,
        ]);
    }
}
