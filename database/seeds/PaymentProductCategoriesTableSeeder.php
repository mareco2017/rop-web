<?php

use App\Models\PaymentProductCategory;
use Illuminate\Database\Seeder;

class PaymentProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Telkomsel',
            'data' => array('+62811','+62812','+62821','+62822'),
            'picture_id' => null
        ]);
		PaymentProductCategory::firstOrCreate([ 
            'category' => 'XL',
            'data' => array('+62817','+62818','+62819','+62877','+62878'),
            'picture_id' => null
        ]);
		PaymentProductCategory::firstOrCreate([ 
            'category' => 'Axis',
            'data' => array('+62831','+62832','+62833','+62837'),
            'picture_id' => null
        ]);
		PaymentProductCategory::firstOrCreate([ 
            'category' => 'Indosat',
            'data' => array('+62814','+62815','+62816','+62816','+62855','+62856','+62857','+62858'),
            'picture_id' => null
        ]);
		PaymentProductCategory::firstOrCreate([ 
            'category' => 'Three',
            'data' => array('+62895','+62896','+62897','+62898','+62899'),
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Mobile Legends',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Steam Wallet IDR',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Google Play',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'iTunes',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Garena Shells',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Megaxus',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'LYTO Game',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'MOL Points',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'WaveGame',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Facebook Game Card',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Mogplay',
            'data' => null,
            'picture_id' => null
        ]);
        PaymentProductCategory::firstOrCreate([ 
            'category' => 'Unipin',
            'data' => null,
            'picture_id' => null
        ]);
    }
}
