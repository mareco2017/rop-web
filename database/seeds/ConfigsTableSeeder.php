<?php

use Illuminate\Database\Seeder;
use App\Models\Config as AppConfig;
use App\Models\Admin;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppConfig::firstOrCreate([
            'key' => 'root_register_role',
            'value' => '1'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'super_admin_register_role',
            'value' => '2'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'admin_register_role',
            'value' => '3'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'business_register_role',
            'value' => '4'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'user_register_role',
            'value' => '5'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'cs_register_role',
            'value' => '6'
        ]);

        $wallet = Admin::where('email', 'wallet@gmail.com')->first();

        AppConfig::firstOrCreate([
            'key' => 'rop_wallet_id',
            'value' => $wallet ? $wallet->id : null
        ]);

        AppConfig::firstOrCreate([
            'key' => 'whatsapp_number',
            'value' => '+628121007200'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'support_email',
            'value' => 'support@mareco.id'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'office_number',
            'value' => '+62778488222'
        ]);

        AppConfig::firstOrCreate([
            'key' => 'about_us',
            'value' => ''
        ]);

        AppConfig::firstOrCreate([
            'key' => 'terms_conditions',
            'value' => ''
        ]);

        AppConfig::firstOrCreate([
            'key' => 'privacy_policy',
            'value' => ''
        ]);
    }
}
