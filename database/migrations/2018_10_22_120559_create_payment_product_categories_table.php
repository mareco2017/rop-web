<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->longText('data')->nullable();
            $table->unsignedInteger('picture_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('picture_id')->references('id')->on('attachments');
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_product_categories');
    }
}
