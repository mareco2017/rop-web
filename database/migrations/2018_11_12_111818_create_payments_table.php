<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->json('options')->nullable();
            $table->string('statement_no')->unique();
            $table->unsignedInteger('bank_account_id')->nullable();
            $table->unsignedInteger('reviewed_by_id')->nullable();
            $table->double('amount', 24, 2)->default(0);
            $table->dateTime('claimed_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('reviewed_by_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
