<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id')->nullable();
            $table->unsignedInteger('debit_wallet_id')->nullable();
            $table->unsignedInteger('credit_wallet_id')->nullable();
            $table->unsignedInteger('owner_wallet_id')->nullable();
            $table->double('debit_current_balance')->default(0);
            $table->double('credit_current_balance')->default(0);
            $table->softDeletes();

            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('debit_wallet_id')->references('id')->on('wallets');
            $table->foreign('credit_wallet_id')->references('id')->on('wallets');
            $table->foreign('owner_wallet_id')->references('id')->on('wallets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutations');
    }
}
