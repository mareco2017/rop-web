<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('left_binary_id')->nullable();
            $table->unsignedInteger('right_binary_id')->nullable();
            $table->unsignedInteger('leader_binary_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('sponsor_user_id')->nullable();
            $table->unsignedInteger('type')->default(0);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('left_binary_id')->references('id')->on('binaries');
            $table->foreign('right_binary_id')->references('id')->on('binaries');
            $table->foreign('leader_binary_id')->references('id')->on('binaries');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('sponsor_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binaries');
    }
}
