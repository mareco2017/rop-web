<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leader_user_id');
            $table->unsignedInteger('leader_package_type')->default(0);
            $table->unsignedInteger('is_leader_claimed')->default(0);
            $table->unsignedInteger('left_binary_user_id')->nullable();
            $table->unsignedInteger('right_binary_user_id')->nullable();
            $table->dateTime('claimed_at')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('leader_user_id')->references('id')->on('users');
            $table->foreign('left_binary_user_id')->references('id')->on('users');
            $table->foreign('right_binary_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairs');
    }
}
