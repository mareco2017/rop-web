<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_product_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_code');
            $table->tinyInteger('status')->default(0);
            $table->string('method')->nullable();
            $table->unsignedInteger('cover_id')->nullable();
            $table->json('options')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cover_id')->references('id')->on('attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_product_types');
    }
}
