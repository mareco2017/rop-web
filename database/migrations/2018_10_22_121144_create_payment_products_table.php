<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->double('amount', 24, 2)->default(0);
            $table->double('price', 24, 2)->default(0);
            $table->double('fee', 24, 2)->default(0);
            $table->longText('data')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('payment_product_categories');
            $table->foreign('type_id')->references('id')->on('payment_product_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_products');
    }
}
