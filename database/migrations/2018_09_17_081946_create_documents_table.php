<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('ownable');
            $table->nullableMorphs('verifiable');
            $table->dateTime('verified_date')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->unsignedInteger('attachment_id')->nullable();
            $table->json('options')->nullable();
            $table->string('description')->nullable()->default('');
            $table->string('identification_number')->nullable()->default('');
            $table->foreign('attachment_id')->references('id')->on('attachments');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
