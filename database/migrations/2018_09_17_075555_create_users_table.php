<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sponsor_id')->nullable();
            $table->unsignedInteger('leader_id')->nullable();
            $table->unsignedInteger('role_id');
            $table->string('phone_number')->unique();
            $table->string('device_type')->nullable()->default('');
            $table->string('email', 150)->unique()->nullable()->default('');
            $table->string('first_name', 50)->nullable()->default('');
            $table->string('last_name', 50)->nullable()->default('');
            $table->tinyInteger('packages')->default(0);
            $table->string('password')->nullable();
            $table->tinyInteger('gender')->default(0);
            $table->date('dob')->nullable();
            $table->unsignedInteger('profile_picture_id')->nullable();
            $table->string('referral_code', 50)->unique();
            $table->dateTime('last_login')->nullable();
            $table->string('pin', 500)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('sponsor_id')->references('id')->on('users');
            $table->foreign('leader_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
