<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('authorized_by_id')->nullable();
            $table->unsignedInteger('picture_id')->nullable();
            $table->unsignedInteger('cover_id')->nullable();
            $table->string('name', 250)->default('');
            $table->string('city', 35)->nullable()->default('');
            $table->string('tagline', 150)->nullable()->default('');
            $table->string('phone_number')->nullable();
            $table->json('socmed')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('slug_url');
            $table->tinyInteger('force_close')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->unsignedInteger('head_branch_id')->nullable();
            $table->double('lat')->default(0);
            $table->double('lng')->default(0);

            $table->foreign('authorized_by_id')->references('id')->on('admins');
            $table->foreign('picture_id')->references('id')->on('attachments');
            $table->foreign('cover_id')->references('id')->on('attachments');
            $table->foreign('head_branch_id')->references('id')->on('businesses');
            $table->text('delete_reason')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
