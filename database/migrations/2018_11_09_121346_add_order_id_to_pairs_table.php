<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderIdToPairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->nullable()->after('leader_user_id');
            $table->unsignedInteger('bank_account_id')->nullable()->after('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pairs', function (Blueprint $table) {
            $table->dropForeign('pairs_order_id_foreign');
            $table->dropForeign('pairs_bank_account_id_foreign');
            $table->dropColumn('order_id');
            $table->dropColumn('bank_account_id');
        });
    }
}
