<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('productable');
            $table->unsignedInteger('promo_id')->nullable();
            $table->unsignedInteger('owner_id');
            $table->tinyInteger('type'); //Tipe Cart : Produk / Tiket dll
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('promo_id')->references('id')->on('promos');
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
