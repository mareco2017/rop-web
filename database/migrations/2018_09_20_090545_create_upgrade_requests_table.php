<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpgradeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upgrade_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('package');
            $table->double('price', 24, 2)->default(0);
            $table->json('options')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->unsignedInteger('requested_by_id')->nullable();
            $table->unsignedInteger('reviewed_by_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('requested_by_id')->references('id')->on('users');
            $table->foreign('reviewed_by_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upgrade_requests');
    }
}
