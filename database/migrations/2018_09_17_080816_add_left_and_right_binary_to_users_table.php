<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeftAndRightBinaryToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('left_binary_id')->nullable()->after('leader_id');
            $table->unsignedInteger('right_binary_id')->nullable()->after('left_binary_id');
            $table->foreign('left_binary_id')->references('id')->on('left_binaries');
            $table->foreign('right_binary_id')->references('id')->on('right_binaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_left_binary_id_foreign');
            $table->dropForeign('users_right_binary_id_foreign');
            $table->dropColumn('left_binary_id');
            $table->dropColumn('right_binary_id');
        });
    }
}
