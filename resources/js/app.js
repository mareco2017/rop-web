
require('./bootstrap');

window.Vue = require('vue');

import App from './components/App.vue';

// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// VueRoute
import VueRouter from 'vue-router'
import router from './routes.js'

// VuexModule
import { store } from './_store'

// Packages
import Ripple from 'vue-ripple-directive'
import VueMoment from 'vue-moment'
import Vuelidate from 'vuelidate'
import Vue2Filters from 'vue2-filters'
import VueCountdownTimer from 'vuejs-countdown-timer'
import { MdField, MdButton, MdContent, MdTabs } from 'vue-material/dist/components'

library.add(fas, far , fab);
Vue.use(VueRouter)
Vue.use(VueMoment)
Vue.use(Vuelidate)
Vue.use(Vue2Filters)
Vue.use(VueCountdownTimer)
Vue.use(MdTabs)
Vue.use(MdField)
Vue.use(MdButton)
Vue.use(MdContent)
Vue.directive('ripple', Ripple);
Vue.component('font-awesome-icon', FontAwesomeIcon)

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
