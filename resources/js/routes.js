import VueRouter from 'vue-router'
import UserIndex from './components/User/UserIndex.vue';
import Payment from './components/User/Payment/Payment.vue';
import PaymentDetail from './components/User/Payment/PaymentDetail.vue';
import TopUp from './components/User/TopUp/TopUp.vue';
import Dashboard from './components/User/Dashboard.vue';
import TransactionList from './components/User/Transaction/TransactionList.vue';
import TransactionDetail from './components/User/Transaction/TransactionDetail.vue';
import TransactionOngoingDetail from './components/User/Transaction/TransactionOngoingDetail.vue';
import Profile from './components/User/Profile/Profile.vue';
import ResetPin from './components/User/Profile/ResetPin.vue';

export default new VueRouter ({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/webview/user',
            component: UserIndex,
            children: [
                {
                    path: '',
                    name: 'dashboard',
                    component: Dashboard,
                },
                {
                    path: 'pay',
                    name: 'user.payment',
                    component: Payment,
                },
                {
                    path: 'payment-detail/:order_id',
                    name: 'user.payment-detail',
                    component: PaymentDetail,
                },
                {
                    path: 'top-up',
                    name: 'user.topup',
                    component: TopUp,
                },
                {
                    path: 'transaction-list',
                    name: 'user.transaction-list',
                    component: TransactionList,
                },
                {
                    path: 'transaction/:order_id',
                    name: 'user.transaction-detail',
                    component: TransactionDetail,
                },
                {
                    path: 'transaction-ongoing/:order_id',
                    name: 'user.transaction-ongoing-detail',
                    component: TransactionOngoingDetail,
                },
                {
                    path: 'profile',
                    name: 'user.profile',
                    component: Profile,
                },
                {
                    path: 'profile/reset-pin',
                    name: 'user.profile.reset-pin',
                    component: ResetPin,
                },
            ]
        },
    ]
})