const state = {
    isLoading: null,
};

const actions = {
    setLoading({ dispatch, commit }, act) {
        commit('setLoadingState', act);
        return new Promise((resolve, reject) => {
            resolve(true);
        });
        
    },
};

const mutations = {
    setLoadingState(state, act) {
        state.isLoading = act;
    },
};

export const loading = {
    namespaced: true,
    state,
    actions,
    mutations
};