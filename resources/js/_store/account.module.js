
const state = {
    isGettingUser: null,
    user: null,
    token: null,
};

const actions = {
    me({ dispatch, commit }, token) {
        dispatch('setToken', token);
        commit('getUserRequest');
        return new Promise((resolve, reject) => {
            window.axios.get('/api/v1/user/me')
                .then((response)  =>  {
                    let res = response.data;
                    commit('getUserSuccess', res.data.user);
                    resolve(res);
                }, (error) => {
                    commit('getUserFail');
                    reject(error);
                });
        });
        
    },
    setToken({ dispatch, commit }, token) {
        commit('updateToken', token);
        let bearerToken = token ? "Bearer " + token : "";
        window.axios.defaults.headers.common['Authorization'] = bearerToken;
    },
    removeToken({ dispatch, commit }) {
        commit('updateToken', "");
        let bearerToken = "Bearer ";
        window.axios.defaults.headers.common['Authorization'] = bearerToken;
    }
};

const mutations = {
    getUserRequest(state) {
        state.isGettingUser = true;
    },
    getUserSuccess(state, user) {
        state.isGettingUser = false;
        state.user = user;
    },
    getUserFail(state) {
        state.isGettingUser = false;
        state.user = null;
    },
    updateToken(state, token) {
        state.token = token;
    },
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};