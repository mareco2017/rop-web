<?php

return [
    'active' => 'Active',
    'inactive' => 'Inactive',
    'pending' => 'Pending',
    'waiting_for_payment' => 'Waiting for Payment',
    'on_process' => 'On Process',
    'verified' => 'Verified',
    'accepted' => 'Accepted',
    'declined' => 'Declined',
    'cancelled' => 'Cancelled',
    'refunded' => 'Refunded',
    'phone' => 'Phone',
    'postpaid_phone' => 'Postpaid Phone',
    'pln' => 'Electricity',
    'game_voucher' => 'Game Voucher',
];