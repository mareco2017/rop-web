<?php

return [
    'head_1' => 'Welcome to Mareco!<br>Your Future Mobile Payment',
    'head_1_desc' => 'Get your new experience with Mareco and transform your Smartphone into your wallet.',
    'head_2' => 'Features of Mareco',
    'head_2_features' => [
        0 => [
            'title' => 'Transfer',
            'desc' => 'Transfer your credits to other account quickly, easily, and safely.',
        ],
        1 => [
            'title' => 'Split Bill',
            'desc' => 'Create room to "Split" the bill with your companion',
        ],
        2 => [
            'title' => 'Receive',
            'desc' => 'Accept balance from other account by clicking only.',
        ],
        3 => [
            'title' => 'Scan',
            'desc' => 'Pay anything at anytime without waiting in line.',
        ],
    ],
    'head_3' => 'Mareco App',
    'head_3_desc' => 'Mareco is a payment system using QR Code technology. Mareco wants you to join Cashless
        Future to help you to pay easily, safely, and quickly. By clicking only, your
        Smartphone will be your wallet simultaneously.',
    'head_4' => 'How it Works',
    'head_4_sub' => '4 Step to Pay',
    'head_4_payment' => [
        0 => [
            'title' => 'Click Scan',
            'desc' => 'To pay the bill, click “Scan”, your Smartphone camera will automatically active.',
        ],
        1 => [
            'title' => 'Scan Store QR Code',
            'desc' => 'Aim your Smartphone’s camera to the QR Code of the store.',
        ],
        2 => [
            'title' => 'Input Amount and Continue',
            'desc' => 'You will be prompt to input the total amount of your bill.',
        ],
        3 => [
            'title' => 'Pop Up of Successful Transaction',
            'desc' => 'A notification will pop up to notice you that your transaction has been succeed.',
        ],
    ],
    'head_5' => 'Enjoy our Exciting ongoing Promos',
    'head_6' => 'Merchants who have joined us',
];