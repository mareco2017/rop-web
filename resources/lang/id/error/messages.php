<?php

return [
	"no_user" => "Anda belum login.",
	"invalid_credential" => "Invalid credential.",
    "create_user_email_used" => "Alamat Email telah digunakan.",
    "create_user_number_used" => "Nomor Handphone telah digunakan.",
    "went_wrong" => "Terjadi kesalahan.",
    "went_wrong_pls_contact" => "Something went wrong, please contact customer support.",
    "upgrade_first" => "Upgrade your account before you can access this feature",
    "insufficient_balance" => "insufficient balance.",
    "order_cannot_cancel" => "Only order with status :object can be cancelled.",

    //use object
    "not_verified" => ":object belum diverifikasi.",
    "not_found" => ":object tidak ditemukan.",
    "failed_to" => "Terjadi kesalahan saat :object",

    //prefixes
    "error_while" => "Terjadi kesalahan saat ",
    "failed" => "Gagal untuk ",
    
    //not found
    "user_not_active" => "Akun Anda diblokir. Hubungi CS kami untuk informasi lebih lanjut",
    "wallet_not_found" => "Wallet not found",

    //progress
    "create_user" => "membuat akun.",
    "update_user" => "memperbaharui akun.",
    "set_pin" => "set PIN.",
    "reset-pin" => "reset PIN.",
    "update_pin" => "mengubah PIN.",
    "check_pin" => "check PIN.",
    "update_phone" => "memperbaharui nomor handphone.",
    "create_session" => "memuat sesi.",
    "logout_user" => "logging out user.",
    "create_order" => "create order.",
    "update_order" => "update order.",

    "event_request" => "request",
    "event_joined" => "Anda sudah bergabung dalam event ini",
    "cannot_join_event" => "Hanya pengguna yang sudah upgrade akun yang dapat mengikuti event ini",
    "referral_code_not_found" => "Kode referral tidak ditemukan",

    "min_topup" => "Top Up minimal :object",
    "topup_exceed" => "Upgrade akun Anda untuk menambah saldo maksimum Anda",
    "topup_exceed_up" => "Anda tidak dapat melewati batas saldo maksimum",
    "still_active" => "Masih terdapat :object yang aktif",

    "user_credential_not_found" => "Anda belum terdaftar.",
    "user_phone_not_found" => "Akun dengan nomor :object tidak ditemukan.",
    "user_not_found" => "Akun pengguna tidak ditemukan.",
    "user_not_active" => "Akun Anda telah dinon-aktifkan, Mohon hubungi Customer Service kami.",
    "error_while_create_user" => "Terjadi kesalahan saat membuat akun.",
    "failed_create_user" => "Gagal untuk membuat akun.",
    "error_while_update_user" => "Terjadi kesalahan saat memperbaharui akun",
    "failed_update_user" => "Gagal untuk memperbaharui akun",
    "update_user_no_user" => "Gagal untuk memperbaharui akun. Anda belum login.",
    "error_while_update_phone" => "Terjadi kesalahan saat memperbaharui nomor handphone.",
    "set_pin_no_user" => "Gagal untuk set PIN. Anda belum login.",
    "check_pin_no_user" => "Gagal untuk check PIN. Akun tidak ditemukan.",
    "update_pin_no_user" => "Gagal untuk memperbaharui PIN. Anda belum login.",
    "pin_already_set" => "PIN telah diset sebelumnya",
    "error_while_set_pin" => "Terjadi kesalahan saat set PIN.",
    "incorrect_pin" => "PIN salah",
    "incorret_old_pin" => "Incorrect Old PIN",
    "same_new_pin" => "Old PIN and new PIN shouldn't be similar",
    "error_while_update_pin" => "Terjadi kesalahan saat memperbaharui PIN",
    "forgot_pin_field_required" => "Field name is required if NRIC is verified",
    "wrong_answer" => "Jawaban salah.",
    "otp_error" => "Invalid or expired PIN.",
    "create_session_no_user" => "Gagal untuk memuat sesi. Anda belum login.",
    "failed_create_session" => "Gagal untuk memuat sesi.",
    "error_while_logout_user" => "Terjadi kesalahan saat logout.",

    "update_bank_failed" => "Gagal memperbaharui informasi bank." ,
    "update_bank_no_user" => "Gagal untuk menambah/memperbaharui akun bank, Anda belum login.",
    "update_bank_acc_failed" => "Terjadi kesalahan saat memperbaharui informasi akun bank.",
    "set_bank_no_user" => "Gagal untuk memperbaharui akun bank utama, Anda belum login.",
    "set_bank_not_author" => "Anda tidak memiliki hak untuk memperbaharui akun bank utama.",
    "delete_bank_not_author" => "Anda tidak memiliki hak untuk menghapus akun bank.",
    "delete_bank_in_use" => "Gagal untuk menghapus akun bank, akun bank sedang digunakan.",
];