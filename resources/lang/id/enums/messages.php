<?php

return [
    'active' => 'Aktif',
    'inactive' => 'Inaktif',
    'pending' => 'Menunggu',
    'waiting_for_payment' => 'Menunggu Pembayaran',
    'on_process' => 'Diproses',
    'verified' => 'Terverifikasi',
    'accepted' => 'Diterima',
    'declined' => 'Ditolak',
    'cancelled' => 'Dibatalkan',
    'refunded' => 'Direfund',
    'phone' => 'Pulsa',
    'postpaid_phone' => 'Tagihan Telepon',
    'pln' => 'PLN',
    'game_voucher' => 'Voucher Game',
];