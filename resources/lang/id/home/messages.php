<?php

return [
    'head_1' => 'Selamat Datang di Mareco!<br>Pembayaran Mobile Masa Depan Anda',
    'head_1_desc' => 'Dapatkan pengalaman baru Anda bersama Mareco dan ubah Smartphone menjadi dompet Anda.',
    'head_2' => 'Fitur-Fitur pada Mareco',
    'head_2_features' => [
        0 => [
            'title' => 'Kirim',
            'desc' => 'Kirim saldo Anda ke pengguna lain dengan cepat, mudah dan aman.',
        ],
        1 => [
            'title' => 'Patungan',
            'desc' => 'Buat ruang untuk melakukan “Patungan” dengan teman Anda.',
        ],
        2 => [
            'title' => 'Terima',
            'desc' => 'Terima saldo dari akun pengguna lain hanya dengan sekali klik.',
        ],
        3 => [
            'title' => 'Pindai',
            'desc' => 'Bayar apapun tanpa mengahabiskan waktu untuk mengantri.',
        ],
    ],
    'head_3' => 'Aplikasi Mareco',
    'head_3_desc' => 'Mareco adalah sistem pembayaran yang menggunakan teknologi Kode QR. Mareco mengajak
        Anda untuk bergabung menuju Masa Depan Non Tunai karena Mareco membantu Anda untuk membayar
        dengan mudah, aman, dan cepat. Hanya dengan sekali klik, Smartphone Anda akan lebih
        berfungi sama seperti dompet Anda sekaligus.',
    'head_4' => 'Cara Kerja',
    'head_4_sub' => '4 Langkah untuk membayar',
    'head_4_payment' => [
        0 => [
            'title' => 'Klik Scan',
            'desc' => 'Untuk membayar tagihan, klik “Scan”, kamera Smartphone Anda akan aktif secara
                otomatis.',
        ],
        1 => [
            'title' => 'Scan QR Code Toko',
            'desc' => 'Arahkan kamera Smartphone Anda ke Kode QR toko.',
        ],
        2 => [
            'title' => 'Masukkan Nominal dan Lanjutkan',
            'desc' => 'Anda akan diminta untuk memasukkan total jumlah tagihan Anda.',
        ],
        3 => [
            'title' => 'Muncul Pemberitahuan Transaksi Sukses',
            'desc' => 'Pemberitahuan akan muncul untuk memberi tahu bahwa Transaksi Anda telah sukses.',
        ],
    ],
    'head_5' => 'Nikmati Promo Menarik dan Terbaru dari Kami',
    'head_6' => 'Merchant kami yang telah bergabung',
];