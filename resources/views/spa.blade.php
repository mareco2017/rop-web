<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="timezone" content="UTC">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    
    <!-- SEO -->

    <title>Rich On Pay</title>
    <!-- CSS -->

	<link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ mix('/css/material-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/styles.css?ver=1.2') }}">
    @stack('pageRelatedCss')
</head>

<body>
    <div id="app"></div>
</body>

<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>