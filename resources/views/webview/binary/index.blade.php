@extends('structure')

@section('title', 'Binaries')

@section('body')
<body class="webview-binaries">
	<div class="bg-white mb-2 border-bottom total">
		<div class="p-3">
			@if ($dekstop)
			<a href="{{ route('logout') }}" class="logout">Logout</a>
			@endif
			<p class="text-center total-poin f-bold">INFORMASI JARINGAN</p>
			<div class="row no-gutters">
				<div class="col align-self-center py-1">
					<div class="text-center">
						<p class="total-text mb-1">Kiri</p>
						<p class="total-text fs-1 mb-1"><i class="fas fa-user-friends text-red mr-1"></i>: <span class="f-bold">{{ $leftBinaryUser }}</span></p>
						<p class="total-poin mb-0">Total Poin : {{ $leftBinaryPoint }}</p>
					</div>
				</div>
				<div class="separator"></div>
				<div class="col align-self-center py-1">
					<div class="text-center">
						<p class="total-text mb-1">Kanan</p>
						<p class="total-text fs-1 mb-1"><i class="fas fa-user-friends text-red mr-1"></i>: <span class="f-bold">{{ $rightBinaryUser }}</span></p>
						<p class="total-poin mb-0">Total Poin : {{ $rightBinaryPoint }}</p>
					</div>
				</div>
			</div>
		</div>	
	</div>
	<div class="bg-white p-3 o-auto flex-1 position-relative">
		<div class="color-means">
			<div class="d-flex align-items-center mb-1">
				<div class="node free"></div><span>Free</span>
			</div>
			<div class="d-flex align-items-center mb-1">
				<div class="node silver"></div><span>Silver</span>
			</div>
			<div class="d-flex align-items-center mb-1">
				<div class="node gold"></div><span>Gold</span>
			</div>
			<div class="d-flex align-items-center mb-1">
				<div class="node platinum"></div><span>Platinum</span>
			</div>
		</div>
		<div id="tree-container"></div>
	</div>
</body>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" href="{{ asset('css/Treant.css?ver=1.1') }}" type="text/css"/>
@endpush

@push('pageRelatedJs')
<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
<script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script type="text/javascript">
	var binaries = {!! json_encode($binaries) !!};
	
	var config = {
		container: "#tree-container",
		connectors: {
			type: 'bCurve',
			style: {
				stroke: '#000',
				'stroke-width': 2,
				'stroke-opacity': '.7',
			}
		},
		siblingSeparation: 15,
		rootOrientation: 'NORTH',
		node: {
			collapsable: true,
		}
	};

	var nodes = [];

	binaries.forEach(function(binary, i) {
		if (binary.level == 0) {
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				id: binary.id,
				text: {
					name: binary.fullname,
					desc: binary.referral_code
				},
                HTMLclass: packageClass,
			});
		} else {
			var parent = nodes.find(function(element) {
				return element.id == binary.leader_binary_id;
			});
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				parent: parent,
				id: binary.id,
				text: {
					name: binary.fullname,
					desc: binary.referral_code
				},
                HTMLclass: packageClass,
			});
		}
	});

	var temp = [
		config
	];

	var simple_chart_config = temp.concat(nodes);
	var my_chart = new Treant(simple_chart_config);
</script>
@endpush

{{-- <!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <!-- stylesheets -->
    <link rel="stylesheet" href="{{ asset('css/Treant.css') }}" type="text/css"/>
</head>
<body>
    <!-- HTML -->
	<div class="container">
		<div>asdasd</div>
		<div id="tree-container"></div>
	</div>

    <!-- javascript -->
	<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
    <script src="{{ asset('js/raphael.js') }}"></script>
    <script src="{{ asset('js/Treant.js') }}"></script>
    <script type="text/javascript">
    	var binaries = {!! json_encode($binaries) !!};

		var config = {
		    container: "#tree-container",
            connectors: {
                type: 'bCurve',
                style: {
                    stroke: '#000',
                    'stroke-width': 2,
                    'stroke-opacity': '.7',
                }
            },
            siblingSeparation: 15,
            rootOrientation: 'NORTH',
            node: {
                collapsable: true,
            }
		};

		var nodes = [];

		binaries.forEach(function(binary, i) {
			if (binary.level == 0) {
				nodes.push({
					id: binary.id,
					text: {
						name: binary.fullname,
						desc: binary.referral_code
					} 
				});
			} else {
				var parent = nodes.find(function(element) {
				  	return element.id == binary.leader_binary_id;
				});

				nodes.push({
					parent: parent,
					id: binary.id,
					text: {
						name: binary.fullname,
						desc: binary.referral_code
					}
				});
			}
		});

		var temp = [
		    config
		];

		var simple_chart_config = temp.concat(nodes);
	    var my_chart = new Treant(simple_chart_config);
    </script>
</body>
</html> --}}