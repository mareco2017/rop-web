@extends('structure')
@section('title', $promo->title ?? 'Promo Detail')

@section('body')
<body class="bg-light-grey">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8">
                    <div class="bg-white box-shadow my-3">
                        <img class="img-fluid w-100" src="{{ asset($promo->cover_url) }}">
                        <div class="py-2 px-3 py-lg-3 px-lg-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <p class="fs-2 f-bold">{{ $promo->title }}</p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        @if ($promo->type != 9)
                                        <div class="col">
                                            <p class="text-grey mb-0">Valid Till</p>
                                            <p class="fs-1 f-bold mb-0">{{ $promo->end_date->format('j M Y') }}</p>
                                        </div>
                                        @endif
                                        @if ($promo->type != 9)
                                        <div class="col">
                                            <p class="text-grey mb-0">Coupon Left</p>
                                            <p class="fs-1 f-bold mb-0">{{ $promo->quantity }}</p>
                                        </div>
                                        @endif
                                    </div>
                                    @if ($promo->description)
                                    <div>
                                        <p class="text-grey mb-0">Description</p>
                                        <p class="mb-0">{!! $promo->description !!}</p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($promo->tnc)
                    <div class="bg-white box-shadow my-3">
                        <div class="py-2 px-3 py-lg-3 px-lg-4 pointer" data-toggle="collapse" data-target="#termsConditions">
                            <div class="row align-items-center mx-min-3">
                                <div class="col px-3">
                                    <p class="f-bold mb-0">Terms and Conditions</p>
                                </div>
                                <div class="px-3">
                                    <i class="fas fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <div id="termsConditions" class="collapse">
                            <div class="pb-2 px-3 pb-lg-3 px-lg-4">
                                <ol class="pl-3 mb-0">
                                    @foreach ($promo->tnc as $i => $tnc)
                                    <li class="{{ !$loop->last ? 'mb-1' : '' }}">{{ $tnc }}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
</body>
@endsection