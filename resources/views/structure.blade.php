<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | Rich On Pay</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
	<link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}"/>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/styles.css?ver=1.2') }}">
    @stack('pageRelatedCss')
    <script>
        FontAwesomeConfig = { searchPseudoElements: true };
    </script>
    <script src="{{ asset('js/fontawesome-all.min.js') }}"></script>
</head>
@yield('body')
<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script> -->
@stack('pageRelatedJs')