@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Payment Product')
@section('groupName', 'Payment Product')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Payment Product</h3>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.payment-product.create') }}">Create payment product</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="products-table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
					<th>Name</th>
					<th>Amount</th>
					<th>Price</th>
					<th>Fee</th>
					<th>Status</th>
					<th>Data</th>
					<th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#products-table').DataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.payment-product.ajax') !!}',
				type: 'POST'
			},
			columns: [
				{ data: 'id', name: 'id' },
				{ data: 'name', name: 'name' },
				{ data: 'amount', name: 'amount' },
				{ data: 'price', name: 'price' },
				{ data: 'fee', name: 'fee' },
				{ data: 'status', name: 'status' },
				{ data: 'data', name: 'data', render: function(data){
					return JSON.stringify(data, null, 2);
				}},
				{ data: 'action', name: 'action' },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');
	});
</script>
@endpush