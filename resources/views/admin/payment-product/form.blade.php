@extends('templates.admin.master')

@section('title', ($paymentProduct->id ? 'Edit' : 'Create').' Banner')
@section('groupName', 'Payment Product')

@section('content')
@if (!$paymentProduct->id)
{!! Form::open(['route' => 'admin.payment-product.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($paymentProduct, ['route' => ['admin.payment-product.update', $paymentProduct], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
<div class="card">
    <div class="card-header">
        <h4 class="card-title">{{ $paymentProduct->id ? 'Edit' : 'Create' }} Payment Product</h3>
    </div>
    <div class="card-body">
        <!-- <div class="row">
            <label for="title" class="col-2 col-form-label">Title</label>
            <div class="col-10">
                <div class="form-group">
                    {{ Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => 'Title']) }}
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('name') ? $errors->first('name') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('amount', 'Amount') }}
                    {{ Form::text('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount', 'required' => 'required')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('amount') ? $errors->first('amount') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('price', 'Price') }}
                    {{ Form::text('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Price', 'required' => 'required')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('price') ? $errors->first('price') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('fee', 'Fee') }}
                    {{ Form::text('fee', old('fee'), array('class' => 'form-control', 'placeholder' => 'Fee', 'required' => 'required')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('fee') ? $errors->first('fee') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('type_id', 'Type') }}
                    {{ Form::select('type_id', $types, old('type_id'), array('class' => 'form-control', 'placeholder' => 'Type')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('type_id') ? $errors->first('type_id') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('category_id', 'Category') }}
                    {{ Form::select('category_id', $categories, old('category_id'), array('class' => 'form-control', 'placeholder' => 'Category')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('category_id') ? $errors->first('category_id') : '' }}</p>
                </div>
                <div class="form-group">
                    {{ Form::label('status', 'Status') }}
                    {{ Form::select('status', App\Helpers\Enums\ActiveStatus::getArray(), old('status'), array('class' => 'form-control', 'placeholder' => 'Status', 'required' => 'required')) }}
                    <p class="text-red mb-0 mt-1">{{ $errors->has('status') ? $errors->first('status') : '' }}</p>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    {{ Form::label('dada', 'Data') }}
                    <ol id="dataContainer" class="mb-3 pl-3">
                        <?php
                            $datas = old('data') && old('data') > 0 ? old('data') : $paymentProduct->data;
                            $nextLoop = 0;
                        ?>
                        @if ($datas && $datas > 0)
                            @foreach ($datas as $name => $data)
                            <li class="pl-2 mb-1">
                                <div class="row mx-min-2">
                                    <div class="col px-2">
                                        @if (is_array($data))
                                        {{ Form::text('data['.$loop->index.'][name]', $data['name'], array('class' => 'form-control', 'placeholder' => 'Name')) }}
                                        @else
                                        {{ Form::text('data['.$loop->index.'][name]', $name, array('class' => 'form-control', 'placeholder' => 'Name')) }}
                                        @endif
                                        <p class="text-left text-red mb-0 mt-1">{{ $errors->has('data.'.$name.'.name') ? $errors->first('data.'.$name.'.name') : '' }}</p>
                                    </div>
                                    <div class="col px-2">
                                        @if (is_array($data))
                                        {{ Form::text('data['.$loop->index.'][value]', $data['value'], array('class' => 'form-control', 'placeholder' => 'Value')) }}
                                        @else
                                        {{ Form::text('data['.$loop->index.'][value]', $data, array('class' => 'form-control', 'placeholder' => 'Value')) }}
                                        @endif
                                        <p class="text-left text-red mb-0 mt-1">{{ $errors->has('data.'.$name.'.value') ? $errors->first('data.'.$name.'.value') : '' }}</p>
                                    </div>
                                    <div class="d-flex align-items-center px-2">
                                        <i class="fas fa-times text-red fs-2 pointer"></i>
                                    </div>
                                </div>
                            </li>
                            <?php
                                $nextLoop++;
                            ?>
                            @endforeach
                        @endif
                    </ol>
                    <p id="addData" class="mb-0 text-left pointer">&nbsp; + Add Data</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="">
            <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
        </div>
    </div>
</div>
{{ Form::close() }}

@endsection
@push('pageRelatedJs')
<script type="text/javascript">
$(document).ready(function() {
    var dataContainer = $('#dataContainer');
    var addData = $('#addData');
    var nextLoop = {!! json_encode($nextLoop) !!};

    dataContainer.on('click', '.fa-times', function() {
        $(this).closest('li').remove();
    });

    addData.on('click', function() {
        let html = `
            <li class="pl-2 mb-1">
                <div class="row mx-min-2">
                    <div class="col px-2">
                        {{ Form::text('data[${nextLoop}][name]', null, array('class' => 'form-control', 'placeholder' => 'Name', 'required')) }}
                    </div>
                    <div class="col px-2">
                        {{ Form::text('data[${nextLoop}][value]', null, array('class' => 'form-control', 'placeholder' => 'Value', 'required')) }}
                    </div>
                    <div class="d-flex align-items-center px-2">
                        <i class="fas fa-times text-red fs-2 pointer"></i>
                    </div>
                </div>
            </li>`;
        dataContainer.append(html);
        nextLoop++;
    });
});
</script>
@endpush