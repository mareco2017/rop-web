@extends('templates.admin.master')

@section('title', ($config->id ? 'Edit' : 'Create').' Config')
@section('groupName', 'Config')

@section('content')
@if (!$config->id)
{!! Form::open(['route' => 'admin.config.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($config, ['route' => ['admin.config.update', $config], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $config->id ? 'Edit' : 'Create' }} Config</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="key" class="col-2 col-form-label">Key</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('key', old('key'), array('class' => 'form-control', 'id' => 'key', 'placeholder' => 'Key')) }}
                        @if($errors->has('key'))
                        <p class="status-text">{{ $errors->first('key') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="value" class="col-2 col-form-label">Value</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::text('value', old('value'), array('class' => 'form-control', 'id' => 'value', 'placeholder' => 'Value')) }}
                        </div>
                        @if($errors->has('value'))
                        <p class="status-text">{{ $errors->first('value') }}</p>
                        @endif
                        <a class="status-text text-dark-grey pointer my-2 d-inline-flex align-items-center" id="switch"><i class="now-ui-icons loader_refresh fs-2 mr-1"></i><span>Switch to CK Editor</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/plugins/textindent') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    var useCkeditor = false;
    var wrapper = $('#wrapper');

    CKEDITOR.config.extraPlugins = 'textindent';
    CKEDITOR.config.indentation = 20;
    $('#switch').on('click', function() {
        useCkeditor = !useCkeditor;
        
        if (useCkeditor) {
            let val = wrapper.find('#value').val();

            wrapper.html('<textarea class="form-control" id="value" name="value" style="visibility: hidden; display: none;"></textarea>');
            wrapper.find('#value').val(val);

            CKEDITOR.replace('value', {
                height: 300,
                indentation: 20
            });
            CKEDITOR.config.extraPlugins = 'textindent';

            text.html('Switch to Normal Input');
        } else {
            let val = CKEDITOR.instances.value.getData();
            
            wrapper.html('<input class="form-control" id="value" placeholder="Value" name="value" type="text">');

            wrapper.find('#value').val(val);
            text.html('Switch to CK Editor');
        }
    });
});
</script>
@endpush
