@extends('templates.admin.master')

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/select2.min.css') }}">
@endpush
@section('title', ($payment->id ? 'Edit' : 'Create').' Payment')
@section('groupName', 'Payments')
@section('content')
@if (!$payment->id)
{!! Form::open(['route' => 'admin.payment.store', 'method' => 'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
@else
{!! Form::model($payment, ['route' => ['admin.payment.update', $payment], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $payment->id ? 'Edit' : 'Create' }} Unclaimed Payment</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label class="col-2 col-form-label">Pair</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('pair_ids[]', $pairList, old('pair_ids[]'), array('class' => 'form-control', 'id' => 'pairList', 'multiple')) }}
                        @if($errors->has('pair_ids'))
                        <p class="status-text">{{ $errors->first('pair_ids') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Sponsor</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('order_ids[]', $orderList, old('order_ids[]'), array('class' => 'form-control', 'id' => 'orderList', 'multiple')) }}
                        @if($errors->has('order_ids'))
                        <p class="status-text">{{ $errors->first('order_ids') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Bank Account</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('bank_account_id', $bankAccountList, old('bank_account_id'), array('class' => 'form-control', 'id' => 'orderList', 'placeholder' => 'Bank Account List')) }}
                        @if($errors->has('bank_account_id'))
                        <p class="status-text">{{ $errors->first('bank_account_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Amount</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('amount', old('amount'), ['class' => 'form-control', 'id' => 'amount', 'placeholder' => 'Amount']) }}
                        @if($errors->has('amount'))
                        <p class="status-text">{{ $errors->first('amount') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
@push('pageRelatedJs')
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script>
$(document).ready(function(){
    var selectPair = $('#pairList');
    var selectOrder = $('#orderList');

    selectPair.select2({
        closeOnSelect: false,
        placeholder: "Select (Multiable)"
    });

    selectOrder.select2({
        closeOnSelect: false,
        placeholder: "Select (Multiable)"
    });

    selectPair.on('change', function() {
        let options = $(this).find('option');
        let selected = $(this).find('option:selected');
    });

    selectOrder.on('change', function() {
        let options = $(this).find('option');
        let selected = $(this).find('option:selected');
    });
});
</script>
@endpush