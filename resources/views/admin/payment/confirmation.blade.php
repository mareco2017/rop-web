@extends('templates.mails.template')

@section('style')
<style>
table.container {
  max-width: 1200px;
}

.hi-text {
  display: none;
}

table.struk {
  width: 100%;
  background: #f1f1f1;
  background-image: url(https://rop.mareco.id/assets/images/bg-logo-transparent.png), url(https://rop.mareco.id/assets/images/email-top.png);
  background-position: center, top left;
  background-size: auto, 100% auto;
  background-repeat: no-repeat;
  padding: 38px 0 10px;
  border-spacing: 0;
}

.h2 {
  text-align: center;
  margin-top: 0;
}

h3 {
  margin: 0 0 17px 0;
}

.w-50 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding-left: 40px;
  padding-right: 4px;
}

.bonus {
  padding-left: 40px;
}

.w-50-1 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding-right: 40px;
  padding-left: 4px;
}

.d-1,
.d-2 {
  display: inline-block;    
  white-space: pre-wrap;
  word-break: break-word;
  vertical-align: top;
}

.d-1 {
  width: 30%;
}

.d-2 {
  width: 70%;
}

.h3 {
  margin: 0;
}

.dummy-text {
  text-align: center;
  margin-bottom: 0;
}

.bg-pink {
  background-color: #f9827c;
}
</style>
@endsection
@section('content')
<?php 
use App\Helpers\Enums\PackageType;
use App\Helpers\GlobalHelper;

?>
<table class="struk">
  <tr>
    <td colspan="2">
      <h2 class="h2">Statement Bonus {{ $payment->created_at->setTimezone('Asia/Jakarta')->format('F Y') }}</h2>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br><br></td>
  </tr>
  <tr>
    <td class="w-50">
      <h3>Statement No : {{ $payment->statement_no  }}</h3>
    </td>
    <td class="w-50-1">
      <h3>Periode : {{ Carbon\Carbon::today()->startOfWeek()->subDays(7)->setTimezone('Asia/Jakarta')->format('d-M-y')  }} s/d {{ $payment->created_at->setTimezone('Asia/Jakarta')->format('d-M-y') }}</h3>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Nama</div>
      <div class="d-2">: {{ $payment->bankAccount->ownable->first_name }} {{ $payment->bankAccount->ownable->last_name }}
      </div>
    </td>
    <td class="w-50-1">
      @if ($payment->bankAccount->ownable->sponsor == null)
      <div class="d-1">Sponsor</div><div class="d-2">: <i>Perusahaan</i> </div>
      @else
      <div class="d-1">Sponsor</div><div class="d-2">: {{ $payment->bankAccount->ownable->sponsor->first_name }} {{ $payment->bankAccount->ownable->sponsor->last_name }}</div>
      @endif
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Kode Referral</div><div class="d-2">: {{ $payment->bankAccount->ownable->referral_code }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1">Bonus Sponsor</div><div class="d-2">: {{ GlobalHelper::toCurrency($totalBonusSponsor) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Tipe Paket</div><div class="d-2">: {{ PackageType::getString($payment->bankAccount->ownable->packages) }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1">Bonus Pairing</div><div class="d-2">: {{ GlobalHelper::toCurrency($totalBonusPair) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Join Sejak </div><div class="d-2">: {{ $payment->bankAccount->ownable->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i') }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1">Bonus Cashback </div><div class="d-2">: 0</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Total Jaringan Kiri </div><div class="d-2">: {{ $payment->bankAccount->ownable->total_left_binary_user }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1">Total Jaringan Kanan </div><div class="d-2">: {{ $payment->bankAccount->ownable->total_right_binary_user }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Total Poin Kiri </div><div class="d-2">: {{ $payment->bankAccount->ownable->total_left_binary_point }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1">Total Poin Kanan </div><div class="d-2">: {{ $payment->bankAccount->ownable->total_right_binary_point }}</div>
    </td>
  </tr>
    <tr>
    <td class="w-50">
      <div class="d-1">Total Bonus Bruto</div><div class="d-2">: {{ GlobalHelper::toCurrency($totalBonusSponsor+$totalBonusPair) }} </div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Pajak ( PPh 21 ) </div><div class="d-2">: 3% </div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <br><br><br>
    </td>
  </tr>
  <tr>
    <td class="bonus" colspan="2">
      <h3>Bonus Setelah Pajak</h3>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Bank </div><div class="d-2">: {{ $payment->bankAccount->bank->abbr }} </div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">No Rekening </div><div class="d-2">: {{ $payment->bankAccount->account_no }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">Nama Rekening</div><div class="d-2">: {{ $payment->bankAccount->account_name }}</div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr class="bg-pink">
    <td class="w-50">
      <div class="d-1 f-bold">TOTAL BONUS</div><div class="d-2">: <span class="f-bold">{{ GlobalHelper::toCurrency( ($totalBonusSponsor+$totalBonusPair) - ($totalBonusSponsor+$totalBonusPair)*0.03 )  }}</span></div>
    </td>
    <td class="w-50-1">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="mb-3">
        <p class="dummy-text">Terima kasih telah menggunakan Rich On Pay</p>
        <p class="dummy-text">Informasi Hubungi Call Center : 082288827775</p>
      </div>
    </td>
  </tr>
</table>
{!! Form::model($payment, ['route' => ['admin.payment.send-email', $payment], 'method' => 'post', 'files' => true]) !!}
<div class="mb-3">
    <button type="submit" class="btn btn-fill btn-info"> Send Email to {{ $payment->bankAccount->ownable->first_name }} {{ $payment->bankAccount->ownable->last_name }}</button>
</div>
{!! Form::close() !!}
@endsection