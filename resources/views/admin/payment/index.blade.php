@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Payment')
@section('groupName', 'Payment')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Payment</h3>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.payment.create') }}">Create payment</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Statement No</th>
                    <th>Bank Account</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
    $(document).ready(function(){
        var table = $('#table').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{!! route('admin.payment.ajax') !!}',
                type: 'POST'
            },
            columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'statement_no', name: 'statement_no', className: 'text-center' },
                { data: 'bank_account_id', name: 'bank_account_id', className: 'text-center' },
                { data: 'amount', name: 'amount', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
            ]
        });
        $(table.table().container()).removeClass( 'form-inline');
    });
</script>
@endpush