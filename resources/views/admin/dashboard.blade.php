@extends('templates.admin.master')

@push('pageRelatedCss')
<link rel="stylesheet" href="{{ asset('css/Treant.css') }}" type="text/css"/>
@endpush

@section('title', 'Dashboard')
@section('groupName', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">Binary tree</h5>
                <h2 class="card-title">{{ $totalBinary }}</h2>
            </div>
            <div class="card-body">
                @if (count($binaries) > 0)
                <div class="dashboard tree-container" id="tree-container"></div>
                @else
                <div class="mb-5">
                    <p class="fs-7 f-light text-grey mb-0 text-center">No binary found</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script src="{{ asset('js/raphael.js') }}"></script>
<script src="{{ asset('js/Treant.js') }}"></script>
<script type="text/javascript">
    var binaries = {!! json_encode($binaries) !!};
	
	var config = {
		container: "#tree-container",
		connectors: {
			type: 'bCurve',
			style: {
				stroke: '#000',
				'stroke-width': 2,
				'stroke-opacity': '.7',
			}
		},
		siblingSeparation: 15,
		rootOrientation: 'NORTH',
		node: {
			collapsable: true,
		}
	};

    var nodes = [];

    	binaries.forEach(function(binary, i) {
		if (binary.level == 0) {
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				id: binary.id,
				text: {
					name: binary.fullname,
                    title: 'ID: ' + binary.user_id,
					desc: binary.referral_code 
				},
                HTMLclass: packageClass,
			});
		} else {
			var parent = nodes.find(function(element) {
				return element.id == binary.leader_binary_id;
			});
            let packageClass = '';
            if (binary.type == 0) {
                packageClass = 'free';
            } else if (binary.type == 1) {
                packageClass = 'silver';
            } else if (binary.type == 2) {
                packageClass = 'gold';
            } else if (binary.type == 3) {
                packageClass = 'platinum';
            }

			nodes.push({
				parent: parent,
				id: binary.id,
				text: {
					name: binary.fullname,
                    title: 'ID: ' + binary.user_id,
					desc: binary.referral_code
				},
                HTMLclass: packageClass,
			});
		}
	});

	var temp = [
		config
	];

	var simple_chart_config = temp.concat(nodes);
	var my_chart = new Treant(simple_chart_config);
    
    $(document).ready(function() {
        $('#tree-container').perfectScrollbar();
    });
</script>
@endpush