@extends('layouts.backend.dashboard')
@extends('layouts.backend.side_bar')

@section('title', '| Top Up')

@section('bc-item')
<li class="breadcrumb-item">
    <a href="{{ route('admin.dashboard.index') }}">
        <i class="fas fa-home"></i><span>Home</span>
    </a>
</li>
<li class="breadcrumb-item">
    <a href="{{ route('admin.top-up.index') }}">
        <i class="fas fa-chevron-circle-down"></i><span>Top-Up</span>
    </a>
</li>
<li class="breadcrumb-item active"><i class="fas fa-plus"></i><span>Confirmation Top-Up</span></li>
@endsection

@section('content')
@if($order->id)
{!! Form::model($order, ['url' => route('admin.top-up.update', ['order' => $order]),'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.top-up.store'),'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content d-flex flex-column">
        @include('includes.session_message')
        <div class="squared">
            <div class="squared-title d-flex align-items-center">
                <div class="actions ml-1 mr-3">
                    <a href="{{ route('admin.top-up.index') }}" title="Back" data-toggle="tooltip" data-placement="right" class="btn btn-circle btn-icon-only">
                        <span class="entypo-back"></span>
                    </a>
                </div>
                <div class="caption">
                    {{ $order->id ? 'Edit' : 'New' }} Topup
                </div>
                <div class="actions ml-auto">
                    
                </div>
            </div>
            <div class="form-group text-left">
                <p class="">Top Up To</p>
                <label class="radio" for="user">
                    {{ Form::radio('request_by_type', 'user', false, ['id' => 'user', 'class' => 'd-none']) }}
                    <div class="circle">
                        <div class="outer-circle"></div>
                        <div class="inner-circle"></div>
                        <div class="ripple-circle"></div>
                    </div>
                    <p>User</p>
                </label>
                <label class="radio" for="business">
                    {{ Form::radio('request_by_type', 'business', false, ['id' => 'business', 'class' => 'd-none']) }}
                    <div class="circle">
                        <div class="outer-circle"></div>
                        <div class="inner-circle"></div>
                        <div class="ripple-circle"></div>
                    </div>
                    <p>Business</p>
                </label>
            </div>
            <div class="form-group" id="userList">
                {{ Form::label('request_by_id_user', 'Users List') }}
                {{ Form::select('request_by_id_user', $userList, null, array('class' => 'form-control nullable', 'placeholder' => 'Please select user', 'required' => 'required')) }}
            </div>
            <div class="form-group" id="businessList">
                {{ Form::label('request_by_id_business', 'Business List') }}
                {{ Form::select('request_by_id_business', $businessList, null, array('class' => 'form-control nullable', 'placeholder' => 'Please select business', 'required' => 'required', 'disabled')) }}
            </div>
            <div class="form-group">
                {{ Form::label('amount', 'Amount') }}
                {{ Form::number('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Amount')) }}
            </div>
            <div class="form-group">
                {{ Form::label('dest_bank_account_id', 'Destination Bank Account') }}
                {{ Form::select('dest_bank_account_id', $destBankAccounts, null, array('class' => 'form-control nullable', 'placeholder' => 'Please select one...')) }}
            </div>
            <button type="submit" class="btn btn-outline-blue"><i class="far fa-save mr-2"></i>Submit</button>
        </div>
    </div>
</div>

{{ Form::close() }}

@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){
        var userList = $('#userList');
        var businessList = $('#businessList');
        var radio = $('input[type=radio]');
        
        radio.on('change', function() {
            if (this.value == 'business') {
                businessList.removeClass('hide').find('select').prop('disabled', false);
                userList.addClass('hide').find('select').prop('disabled', true);
            } else {
                businessList.addClass('hide').find('select').prop('disabled', true);
                userList.removeClass('hide').find('select').prop('disabled', false);
            }
        });
    });
</script>
@endpush