@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Withdraw')
@section('groupName', 'Withdraw')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Withdraw</h3>
        <!-- <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.bank.create') }}">Create bank</a>
            </div>
        </div> -->
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Reference Number</th>
                    <th>Status</th>
                    <th>Request By</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.withdraw.ajax') !!}',
				type: 'POST'
			},
			columns: [
				{ data: 'id', name: 'id' },
				{ data: 'reference_number', name: 'reference_number' },
				{ data: 'status', name: 'status' },
				{ data: 'created_by', name: 'created_by' },
				{ data: 'action', name: 'action', sortable: false },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');
	});
</script>
@endpush