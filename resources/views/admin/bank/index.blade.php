@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Bank')
@section('groupName', 'Bank')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Bank</h3>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.bank.create') }}">Create bank</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Abbr</th>
                    <th>Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.bank.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'cover', name: 'cover', className: 'text-center', orderable: false, searchable: false, render: function (data, type, full, meta) {
                    if (!data) return '';
                    var str = '<a href="'+ data +'" target="_blank"><img id="previewImage" src="'+ data +'"/></a>';
                    return str;
                    }
                },
                { data: 'name', name: 'name', className: 'text-center' },
                { data: 'abbr', name: 'abbr', className: 'text-center' },
                { data: 'code', name: 'code', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass( 'form-inline');
	});
</script>
@endpush