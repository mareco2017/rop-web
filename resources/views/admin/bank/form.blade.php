@extends('templates.admin.master')

@section('title', ($bank->id ? 'Edit' : 'Create').' Bank')
@section('groupName', 'Bank')

@section('content')
@if (!$bank->id)
{!! Form::open(['route' => 'admin.bank.store', 'method' => 'post', 'files' => true]) !!}
@else
{!! Form::model($bank, ['route' => ['admin.bank.update', $bank], 'method' => 'post', 'files' => true]) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $bank->id ? 'Edit' : 'Create' }} Bank</h3>
        </div>
        <div class="card-body">
            <div class="row mx-min-1">
                <div class="col-md-4 col-12 px-1">
                    <div class="form-group">
                        <label for="name">Name</label>
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name']) }}
                        @if($errors->has('name'))
                        <p class="status-text">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 col-6 px-1">
                    <div class="form-group mb-md-0">
                        <label for="abbr">Abbrevation</label>
                        {{ Form::text('abbr', old('abbr'), ['class' => 'form-control', 'id' => 'abbr', 'placeholder' => 'Abbrevation']) }}
                        @if($errors->has('abbr'))
                        <p class="status-text">{{ $errors->first('abbr') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 col-6 px-1">
                    <div class="form-group mb-md-0">
                        <label for="code">Code</label>
                        {{ Form::text('code', old('code'), ['class' => 'form-control inputFileVisible', 'id' => 'code', 'placeholder' => 'Code']) }}
                        @if($errors->has('code'))
                        <p class="status-text">{{ $errors->first('code') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div>
                    <label>Logo</label>
                </div>
                <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <img src="{{ $bank->id && $bank->cover_url ? $bank->cover_url : asset('/assets/images/img-placeholder.png') }}">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                    <div>
                        <span class="btn btn-primary btn-round btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                            {{ Form::file('logo') }}
                        </span>
                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                    </div>
                </div>
                @if($errors->has('logo'))
                <p class="status-text">{{ $errors->first('logo') }}</p>
                @endif
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-fill btn-info">Submit</button>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
@endpush