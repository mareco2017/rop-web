@extends('templates.admin.master')

@section('title', ($bankAccount->id ? 'Edit' : 'Create').' Bank Account')
@section('groupName', 'Bank Account')

@section('content')
@if (!$bankAccount->id)
{!! Form::open(['route' => 'admin.bank-account.store', 'method' => 'post']) !!}
@else
{!! Form::model($bankAccount, ['route' => ['admin.bank-account.update', $bankAccount], 'method' => 'post']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $bankAccount->id ? 'Edit' : 'Create' }} Bank Account</h3>
        </div>
        <div class="card-body">
            <div class="row mx-min-1">
                <div class="col-md-4 col-6 px-1">
                    <div class="form-group">
                        <label for="name">Account Name</label>
                        {{ Form::text('account_name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Account Name']) }}
                        @if($errors->has('account_name'))
                        <p class="status-text">{{ $errors->first('account_name') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 col-6 px-1">
                    <div class="form-group">
                        <label for="account_no">Account Number</label>
                        {{ Form::text('account_no', old('account_no'), ['class' => 'form-control', 'id' => 'account_no', 'placeholder' => 'Account Number']) }}
                        @if($errors->has('account_no'))
                        <p class="status-text">{{ $errors->first('account_no') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 col-12 px-1">
                    <div class="form-group">
                        <label for="bankList">Select Bank</label>
                        {{ Form::select('bank_id', $bankList, old('bank_id'), array('class' => 'form-control', 'id' => 'bankList', 'placeholder' => 'Bank List')) }}
                        @if($errors->has('bank_id'))
                        <p class="status-text">{{ $errors->first('bank_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row mx-min-1">
                <div class="col-md-4 col-6 px-1">
                    <div class="form-group">
                        <label for="adminId">Select Admin</label>
                        {{ Form::select('admin_id', $adminList, $bankAccount->id ? $bankAccount->ownable->id : old('admin_id'), array('class' => 'form-control', 'placeholder' => 'Admin List')) }}
                        @if($errors->has('admin_id'))
                        <p class="status-text">{{ $errors->first('admin_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Set Primary</label>
                <div class="form-check form-check-radio">
                    <label class="form-check-label">
                        {{ Form::radio('is_primary', 1, $bankAccount->id ? $bankAccount->is_primary : true, array('id' => 'primary', 'class' => 'form-check-input')) }}
                        <span class="form-check-sign"></span>
                        Primary
                    </label>
                </div>
                <div class="form-check form-check-radio">
                    <label class="form-check-label">
                        {{ Form::radio('is_primary', 0, $bankAccount->is_primary, array('id' => 'primary', 'class' => 'form-check-input')) }}
                        <span class="form-check-sign"></span>
                        Not Primary
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-fill btn-info">Submit</button>
        </div>
    </div>
{!! Form::close() !!}
@endsection