@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'Order')
@section('groupName', 'Order')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Order</h4>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-6 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class="form-group">
                    <label for="filterDate">Filter Date</label>
                    {{ Form::text('date', old('date'), array('class' => 'form-control datepickers', 'id' => 'filterDate', 'placeholder' => 'Date')) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Reference Number</th>
                    <th>Dest No</th>
                    <th>Status</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Request By</th>
                    <th>Date / Time</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script>
	$(document).ready(function() {
        var filterDate = $('input[name="date"]');
        var userId = "{{ Request::get('user_id') }}";
        var status = "{{ Request::get('status') }}";

        filterDate.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            clearBtn: true
        });

        var filterStatusEnum = null;
        var filterDateValue = null;
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.order.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                    d.date = filterDateValue;
                    d.user_id = userId;
                },
				type: 'POST'
			},
			columns: [
				{ data: 'id', name: 'id', className: 'text-center' },
                { data: 'reference_number', name: 'reference_number', className: 'text-center' },
				{ data: 'options', name: 'options', className: 'text-center' },
                { data: 'status', name: 'status', className: 'text-center' },
                { data: 'type', name: 'type', className: 'text-center' },
				{ data: 'amount', name: 'amount', className: 'text-center' },
                { data: 'created_by', name: 'created_by', className: 'text-center' },
				{ data: 'created_at', name: 'created_at', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});

		$(table.table().container()).removeClass( 'form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value;
            table.draw();
        });

        if (status) {
            $('#filterStatus').val(status);
            $('#filterStatus').change();
        }


        filterDate.on('change', function(e) {
            filterDateValue = e.currentTarget.value;
            table.draw();
        });
	});
</script>
@endpush