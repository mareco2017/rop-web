@extends('templates.admin.master')

@section('title', 'Detail')
@section('groupName', 'Users')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">{{ $user->fullname }} Detail</h3>
             @if($user->role_id == 7) <b> *This is Special User* </b> @endif
    </div>
    <div class="card-body">
		<div class="squared">
            <div class="row no-gutters form-group">
                <div class="col-12 col-lg-6">
                    <div class="row no-gutters">
                        <div class="setting-tab-icon d-flex justify-content-center px-0">
                            <i class="fas fa-cog text-blue fs-2"></i>
                        </div>
                        <div class="col pl-2">
                            <p class="text-blue fs-1 f-bold line-height-2">General Information</p>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">First Name</p>
                                        <span class="">{{ $user->first_name }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Last Name</p>
                                        <span class="">{{ $user->last_name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Gender</p>
                                        <div class="d-flex align-items-center">
                                            <i class="fas fs-3  {{ $user->gender == GenderType::MALE ? 'fa-mars text-blue' : 'fa-venus text-pink' }} mr-2"></i>
                                            <span class="">{{ GenderType::getString($user->gender) }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Date of Birth</p>
                                        <span class="">{{ $user->dob ? $user->dob : '-' }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Email</p>
                                        <span class="">{{ $user->email }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Saldo</p>
                                        <span class="">{{ GlobalHelper::toCurrency($user->getWallet(0)->balance) }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Phone Number</p>
                                        <span class="">{{ $user->phone_number }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Point</p>
                                        <span class="">{{ $user->getWallet(WalletType::POINT)->balance }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Leader ID</p>
                                        @if($leader)
                                        <span class="">{{ $leader->first_name }} {{ $leader->last_name }} - {{ $leader->referral_code }}</span>
                                        @else
                                        <span class="">Tidak ada leader</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Sponsor ID</p>
                                        @if($user->binary)
                                            @if($user->binary->SponsorUser)
                                            <span class="">{{ $user->binary->sponsorUser->first_name }} {{ $user->binary->sponsorUser->last_name}} - {{ $user->binary->sponsorUser->referral_code }}</span>
                                            @else
                                            <span class=""> Tidak ada sponsor</span>
                                            @endif
                                        @else
                                        <span class=""> Tidak ada sponsor</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Poin Kiri</p>
                                        <span class="">{{ $leftBinaryPoint }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Poin Kanan</p>
                                        <span class="">{{ $rightBinaryPoint }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="row no-gutters">
                        <div class="setting-tab-icon d-flex justify-content-center px-0">
                            <i class="fas fa-book text-blue fs-2"></i>
                        </div>
                        <div class="col pl-2">
                            <p class="text-blue fs-1 f-bold line-height-2">Documentation</p>
                            <div class="mb-3">
                                <p class="mb-0 text-grey">Identification Number</p>
                                @if ($user->getDocument(DocumentType::NRIC))
                                <span class="">{{ $user->getDocument(DocumentType::NRIC)->identification_number }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <p class="mb-0 text-grey">Identification Status</p>
                                @if ($user->getDocument(DocumentType::NRIC))
                                <span class="">{{ VerificationStatus::getString($user->getDocument(DocumentType::NRIC)->verifications[0]->status) }}</span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">NRIC Image</p>
                                        @if ($user->getDocument(DocumentType::NRIC))
                                        <a href="{{ $user->getDocument(DocumentType::NRIC)->attachment_url }}" data-lightbox="image-1">
                                            <img class="img-fluid mt-1 rounded border border-primary" src="{{ $user->getDocument(DocumentType::NRIC)->attachment_url }}">
                                        </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Selfie Image</p>
                                        @if ($user->getDocument(DocumentType::SELFIE))
                                        <a href="{{ $user->getDocument(DocumentType::SELFIE)->attachment_url }}" data-lightbox="image-1">
                                            <img class="img-fluid mt-1 rounded border border-primary" src="{{ $user->getDocument(DocumentType::SELFIE)->attachment_url }}">
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                $nric = $user->getDocument(DocumentType::NRIC);
                                $vNric = $nric ? $nric->verification() : null;
                                $histories = $vNric ? $vNric->histories : [];
                            @endphp
                            @if ($nric)
                                <div class="row">
                                    <div class="col-12 col-lg-12">
                                        <div class="mb-3">
                                            <p class="mb-0 text-grey">Histories</p>
                                            @foreach($histories as $h)
                                                @php
                                                    $creatable = $h->creatable;
                                                @endphp
                                                <span>
                                                    {{ $h->created_at }} - 
                                                    {{ VerificationStatus::getString($h->status) }} by <b>{{ $creatable ? $creatable->fullname : "Unknown" }}</b>
                                                </span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="row no-gutters">
                        <div class="setting-tab-icon d-flex justify-content-center px-0">
                            <i class="fas fa-laptop text-blue fs-2"></i>
                        </div>
                        <div class="col pl-2">
                            <p class="text-blue fs-1 f-bold line-height-2">System</p>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Device Type</p>
                                        <span class="">{{ $user->device_type }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Last Login</p>
                                        <span class="">{{ $user->last_login ? $user->last_login : " - "}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <p class="mb-0 text-grey">Created At</p>
                                <span class="">{{ $user->created_at }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($user->bankAccounts && count($user->bankAccounts) > 0)
                <div class="col-12 col-lg-6">
                    <div class="row no-gutters">
                        <div class="setting-tab-icon d-flex justify-content-center px-0">
                            <i class="fas fa-university text-blue fs-2"></i>
                        </div>
                        <div class="col pl-2">
                            <p class="text-blue fs-1 f-bold line-height-2">Bank</p>
                            @foreach ($user->bankAccounts as $i => $bankAccount)
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Account Name</p>
                                        <span class="">{{ $bankAccount->account_name }}</span>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="mb-3">
                                        <p class="mb-0 text-grey">Account Number</p>
                                        <span class="">{{ $bankAccount->account_no }}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <span>We have a responsibility to protect user data</span>
        </div>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
	});
</script>
@endpush