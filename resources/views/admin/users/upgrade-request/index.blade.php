@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title', 'User Upgrade Package')
@section('groupName', 'Users')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">User Upgrade Package</h3>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Package</label>
                    {{ Form::select('package', $packageList, '', ['class' => 'form-control', 'id' => 'filterPackage', 'placeholder' => 'Filter Package']) }}
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control', 'id' => 'filterStatus', 'placeholder' => 'Filter Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th rowspan="2">Id</th>
                    <th colspan="2" class="text-center">Package</th>
                    <th rowspan="2">Price</th>
                    <th rowspan="2">Requester</th>
                    <th rowspan="2">Date / Time</th>
                    <th rowspan="2">Reviewer</th>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">Action</th>
                </tr>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;
        var filterPackageEnum = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.users.upgrade-request.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                    d.package_enum = filterPackageEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'package', name: 'package', className: 'text-center' },
                { data: 'package_name', name: 'package_name', className: 'text-center', sortable: false },
                { data: 'price', name: 'price', className: 'text-center' },
                { data: 'requested_by.fullname', name: 'requested_by.fullname', className: 'text-center' },
                { data: 'created_at', name: 'created_at', className: 'text-center' },
                { data: 'reviewed_by.fullname', name: 'reviewed_by.fullname', className: 'text-center', 'render': function (data, type, full, meta) {
						if (!data) return '';
						return data;
					}
				},
                { data: 'status', name: 'status', className: 'text-center', sortable: false },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass('form-inline');

        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 

        $('#filterPackage').on('change', function(e) { 
            filterPackageEnum = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush