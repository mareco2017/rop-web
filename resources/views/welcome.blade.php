@extends('structure')

@section('title', 'Home')

@section('body')
<body>
    <header class="header navbar navbar-expand-xl bg-white">
        <div class="container">
            <a href="/" class="d-flex align-items-center my-auto navbar-brand-wrapper">
                <div class="navbar-brand logo-text p-0 mr-0"></div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fas fa-ellipsis-v fs-2 m-auto"></i></span>
            </button>
            <div class="navbar-collapse collapse justify-content-xl-end" id="navigation">
                <div class="tab-list d-flex flex-column flex-xl-row pt-3 pt-xl-0">
                    <a href="#aboutROP">
                        <p>Apa itu ROP?</p>
                    </a>
                    <a href="#productService">
                        <p>Produk & Layanan</p>
                    </a>
                    <a href="#testimonials">
                        <p>Testimonial</p>
                    </a>
                    <div class="btn-wrapper">
                        <a href="#join" class="btn btn-red btn-block text-center">CARA BERGABUNG</a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="intro" class="intro">
        <div class="container">
            <div class="intro-wrapper">
                <div class="d-flex flex-column-reverse flex-xl-row">
                    <div class="intro-text">
                        <p class="h2 mb-0">Jadikan smartphone Anda sebagai</p>
                        <p class="display-2 big-text my-4">Sumber pendapatan tanpa batas!</p>
                        <p class="desc mb-0">Jualan produk digital dan nikmati keuntungan bonus dan cashback hingga jutaan rupiah! Yuk gabung dibisnis masa kini, Download aplikasi kami sekarang juga!</p>
                        <div class="download-box">
                            <a href="https://itunes.apple.com/id/app/rich-on-pay/id1444844899?mt=8">
                                <img src="{{ asset('/assets/images/download-at-app-store.png') }}" alt="download-at-app-store">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.richonpay">
                                <img src="{{ asset('/assets/images/get-in-google-play.png') }}" alt="get-on-google-play">
                            </a>
                        </div>
                        @if (!Auth::user())
                        <div>
                            <button data-toggle="modal" data-target="#loginModal" class="btn btn-outline-white btn-label">Login Untuk Melihat Jaringan</button>
                        </div>
                        @else
                        <div>
                            <a href="{{ route('web.binary') }}" class="btn btn-outline-white btn-label">Lihat Jaringan</a>
                        </div>
                        @endif
                    </div>
                    <div class="iphone" id="iphones">
                        <img class="iphone-back" data-depth="0.1" src="{{ asset('/assets/images/home/iphone-back.png') }}" alt="iphone-image">
                        <img class="iphone-front" data-depth="0.4" src="{{ asset('/assets/images/home/iphone-front.png') }}" alt="iphone-image">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="aboutROP" class="about-rop">
        <div class="container">
            <div class="label">Tentang Rich On Pay</div>
            <p class="display-4 f-bold my-4">Apa itu ROP?</p>
            <p class="desc mb-0">Richonpay adalah Aplikasi digital payment dari Kota Batam untuk kemudahan transaksi anda. Richonpay dapat melakukan transaksi atau pembayaran seperti halnya ATM, Internet atau Mobile Banking, atau PPOB.<br><br>Dengan Richonpay, Handphone anda dapat melakukan pembelian atau pembayaran seperti isi ulang pulsa,isi paket data, Token listrik, Bayar tagihan telepon, Listrik, PDAM, BPJS Kesehatan, Beli Tiket Pesawat dan Kereta Api, cicilan kendaraan, dan masih banyak lagi. Sangat mudah dan praktis tanpa harus keluar rumah, lebih hemat waktu dan tenaga, Bisa untuk transaksi pribadi maupun orang lain.<br><br>Richonpay memberikan kemudahan dan keuntungan seperti adanya Cashback transaksi, komisi jaringan, dan juga Reward sehingga anda berpotensi meraih kesuksesan lebih cepat hanya dengan cara sederhana yaitu Memasyarakatkan Richonpay dan Me-richonpaykan masyarakat.</p>
        </div>
    </section>

    <section id="productService" class="product-service">
        <div class="container">
            <div class="wrapper">
                <div class="label">Produk & Layanan</div>
                <div class="products">
                    <div class="item">
                        <div class="item-logo">
                            <img src="{{ asset('/assets/images/home/products/pulsa.svg') }}" alt="pulsa">
                        </div>
                        <p class="item-title">Pulsa</p>
                    </div>
                    <div class="item">
                        <div class="item-logo">
                            <img src="{{ asset('/assets/images/home/products/listrik.svg') }}" alt="pulsa">
                        </div>
                        <p class="item-title">Listrik</p>
                    </div>
                    <div class="item">
                        <div class="item-logo">
                            <img src="{{ asset('/assets/images/home/products/games.svg') }}" alt="pulsa">
                        </div>
                        <p class="item-title">Voucher Game</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials" class="testimonials">
        <div class="container">
            <div class="text-center">
                <p class="h1 title">Testimonials</p>
            </div>
            <div class="carousel-wrapper">
                <div class="owl-carousel owl-theme">

                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/1.jpg') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Elizabeth</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>Saya pakai ROP dan sambil kerja jualan pulsa di kantor, seneng banget karena transaksinya cepet dan komisi nya gede bgt hehe</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/2.png') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Ade Gunawan</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>Keren nich aplikasinya.. semakin byk aplikasi anak bangsa yg memberikan manfaat ke masyarakat. Aplikasi ROP solusi kebutuhan di era digital..</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/3.jpeg') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Muhammad Nor Fazrin</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>Sangat membantu dalam proses transaksi, aku jadi gampang kalau mau bayar2 gak ribet bawa dompet lagi</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/4.jpeg') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Hendra James Simbolon</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>Begh...harusnya dari dulu indonesia miliki aplikasi seperti ROP ini..😍 bonusnya berlemak bah..!😘😘 Terimakasih ROP..! Sukses yah..!👍</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/5.jpeg') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Lili Susanty</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>baru x ini nemu aplikasi komplit begini. Bs ngerubah Pengeluaran justru jd penghasilan. Sukak dan ❤❤ bgt deh. harga pulsanya jg gila murah bgt, bisa sambilan jualan pulsa ke tmn2...</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="card-shadowed">
                            <div class="card-image">
                                <img src="{{ asset('/assets/images/home/testimonials/6.jpeg') }}" alt="">
                            </div>
                            <div class="card-name">
                                <p>Venti</p>
                            </div>
                            <div class="card-class">
                                <p>Platinum Member</p>
                            </div>
                            <div class="card-desc">
                                <p>RoP mempermudah aktivitas ku sehari hari, gak takut lagi untuk bertransaksi.. Kamu tunggu apalagi, nikmatnya menggunakan ROP di era digitalisasi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="join" class="join">
        <div class="container">
            <div class="wrapper">
                <div class="phone">
                    <img src="{{ asset('/assets/images/home/iphone-front.png') }}" alt="iphone-image">
                </div>
                <div class="steps">
                    <p class="title text-red h1 f-bold">Cara Bergabung Menjadi Member</p>
                    <div class="red-box">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="step">
                                    <div class="step-index">
                                        <p class="display-3 f-bold">1</p>
                                    </div>
                                    <div class="step-desc">
                                        <p class="h3">Download & Install Aplikasi</p>
                                        <p class="text-desc">Silahkan unduh (download) aplikasi Rich On Pay melalui playstore atau appstore sesuai dengan sistem operasi smartphone teman-teman.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="step">
                                    <div class="step-index">
                                        <p class="display-3 f-bold">2</p>
                                    </div>
                                    <div class="step-desc">
                                        <p class="h3">Daftar & Eksplorasi</p>
                                        <p class="text-desc">Segera daftarkan akun anda di aplikasi Rich On Pay dan Eksplorasi aplikasi ini secara menyeluruh, Anda akan mendapatkan pengalaman terbaik dari aplikasi ini.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="step">
                                    <div class="step-index">
                                        <p class="display-3 f-bold">3</p>
                                    </div>
                                    <div class="step-desc">
                                        <p class="h3">Daftar & Eksplorasi</p>
                                        <p class="text-desc">Segera daftarkan akun anda di aplikasi Rich On Pay dan Eksplorasi aplikasi ini secara menyeluruh, Anda akan mendapatkan pengalaman terbaik dari aplikasi ini.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="download" class="download">
        <div class="container">
            <div class="d-flex flex-column flex-xl-row">
                <div class="wrapper">
                    <p class="h1 f-bold">Download Aplikasi<br>Rich On Pay Sekarang!</p>
                    <p class="desc">Jualan produk digital dan nikmati keuntungan bonus dan cashback hingga jutaan rupiah!</p>
                    <div class="download-box">
                        <a href="https://itunes.apple.com/id/app/rich-on-pay/id1444844899?mt=8">
                            <img src="{{ asset('/assets/images/download-at-app-store.png') }}" alt="download-at-app-store">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.richonpay">
                            <img src="{{ asset('/assets/images/get-in-google-play.png') }}" alt="get-on-google-play">
                        </a>
                    </div>
                </div>
                <div class="phones">
                    <img src="{{ asset('/assets/images/home/phones.png') }}" alt="phones">
                </div>
            </div>
        </div>
    </section>
    
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginModalTitle">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'login', 'method' => 'post']) !!}
                        <div class="form-group">
                            <input class="form-control" type="text" name="username" placeholder="Email / Phone Number (+62xx)">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" name="password" placeholder="Password">
                        </div>
                        <div class="d-flex justify-content-end">
                            <button class="btn text-red">Sign In</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer justify-content-center pointer" data-dismiss="modal">
                    <p class="text-center mb-0 text-grey">Close</p>
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="logo-wrapper">
                <img class="logo" src="{{ asset('/assets/icons/logo-text.png') }}" alt="logo-rop">
            </div>
            <div class="info-wrapper">
                <div class="row">
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Customer Service</p>
                        <p class="item"><i class="fas fa-phone mr-3"></i>+6282288827775</p>
                        <p class="item"><i class="fas fa-envelope mr-3"></i>official.richonpay@gmail.com</p>
                    </div>
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Dukungan</p>
                        <a href="{{ route('webview.terms-conditions') }}">
                            <p class="item">Kebijakan Privasi</p>
                        </a>
                        <a href="{{ route('webview.privacy-policy') }}">
                            <p class="item">Syarat & Ketentuan</p>
                        </a>
                    </div>
                    <div class="col-lg-4 col-12 item-wrapper">
                        <p class="title">Social Media</p>
                        <a href="https://www.facebook.com/Rich-On-Pay-393678798127302/">
                            <p class="item-icon"><i class="fab fa-facebook-f"></i></p>
                        </a>
                        <a href="https://www.instagram.com/richonpay/?hl=en">
                            <p class="item-icon"><i class="fab fa-instagram"></i></p>
                        </a>
                        <p class="item-icon"><i class="fab fa-twitter"></i></p>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <p class="copyright">© 2019 PT.ROP Group Indonesia | RICH ON PAY - All Rights Reserved</p>
        </div>
    </footer>
</body>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('/css/owl.theme.default.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
<script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
<script>
$(document).ready(function(){
    var iphones = document.getElementById('iphones');
    var parallaxInstance = new Parallax(iphones);

    $('.carousel-wrapper .owl-carousel').owlCarousel({
        loop: false,
        margin: 60,
        items: 3,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        slideTransition: 'cubic-bezier(0.5,0.5,0,1)',
        responsive:{
            0:{
                items:1
            },
            800:{
                items:2
            },
            1200:{
                items:3
            }
        }
    });

    $('.red-box .owl-carousel').owlCarousel({
        loop: false,
        items: 1,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        slideTransition: 'cubic-bezier(0.5,0.5,0,1)',
    });
    
    $(".tab-list a").on('click', function (e){
        e.preventDefault();
        
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top + 1
        }, 300);
    });
});
</script>
@endpush