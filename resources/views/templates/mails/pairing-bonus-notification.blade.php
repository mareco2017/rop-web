@extends('templates.mails.template')

@section('style')
<style>
table.container {
  max-width: 1200px;
}

.hi-text {
  display: none;
}

table.struk {
  width: 100%;
  background: #f1f1f1;
  border: solid 2px #dedede;
  padding: 10px 40px;
}

.h2 {
  text-align: center;
  margin-top: 0;
}

.w-50 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding: 0 8px 0 0;
}

.w-50 + .w-50 {
  padding: 0 0 0 8px;
}

.d-1,
.d-2 {
  display: inline-block;    
  white-space: pre-wrap;
  word-break: break-all;
  vertical-align: top;
}

.d-1 {
  width: 30%;
}

.d-2 {
  width: 70%;
}

.h3 {
  margin: 0;
}

.dummy-text {
  text-align: center;
  margin-bottom: 0;
}
</style>
@endsection
@section('content')
<?php 
use App\Helpers\Enums\PackageType;
use App\Helpers\GlobalHelper;

?>
<table class="struk">
  <tr>
    <td colspan="2">
      <h2 class="h2">STATEMENT PAIRING BONUS</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">NAMA</div><div class="d-2">: {{ $notification->options['leader_user']['first_name'] }} {{ $notification->options['leader_user']['last_name'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">JARINGAN KIRI</div><div class="d-2">: {{ $notification->options['left_binary_user']['first_name'] }} {{ $notification->options['left_binary_user']['last_name'] }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">KODE REFERRAL</div><div class="d-2">: {{ $notification->options['leader_user']['referral_code'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">JARINGAN KANAN</div><div class="d-2">: {{ $notification->options['right_binary_user']['first_name'] }} {{ $notification->options['right_binary_user']['last_name'] }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TIPE PAKET</div><div class="d-2">: {{ PackageType::getString($notification->options['leader_user']['packages']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">BONUS</div><div class="d-2">: {{ GlobalHelper::toCurrency($notification->options['amount']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">REF</div>
      <div class="d-2">: {{ $notification->options['reference_number'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <h2 class="text-center">MAKING MONEY IS ART, WORKING IS ART AND GOOD BUSINESS IS THE BEST ART</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">ADMIN</div><div class="d-2">: {{ GlobalHelper::toCurrency(0) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TOTAL BONUS</div><div class="d-2">: {{ GlobalHelper::toCurrency($notification->options['amount']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="mb-3">
        <p class="dummy-text">Thank you for using Rich On Pay</p>
        <p class="dummy-text">Informasi Hubungi Call Center : 0778 - 4xxxxx</p>
      </div>
    </td>
  </tr>
</table>
@endsection