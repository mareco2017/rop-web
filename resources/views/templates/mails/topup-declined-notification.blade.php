@extends('templates.mails.template')

@section('style')
<style>
  .price {
    font-size: 32px;
    font-weight: bold;
  }
</style>
@endsection
@section('content')
<p class="mb-1">Your Top Up have been declined</p>
<p class="text-grey">Thank you for using Rich On Pay, Enjoy cashless with us</p>
<p class="label-grey-0">Reason</p>
<p class="mb-0">{{ $notification->options['message'] }}</p>
@endsection