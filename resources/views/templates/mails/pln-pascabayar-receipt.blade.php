@extends('templates.mails.template')

@section('style')
<style>
table.container {
  max-width: 1200px;
}

.hi-text {
  display: none;
}

table.struk {
  width: 100%;
  background: #f1f1f1;
  border: solid 2px #dedede;
  padding: 10px 40px;
}

.h2 {
  text-align: center;
  margin-top: 0;
}

.w-50 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding: 0 8px 0 0;
}

.w-50 + .w-50 {
  padding: 0 0 0 8px;
}

.d-1,
.d-2 {
  display: inline-block;    
  white-space: pre-wrap;
  word-break: break-all;
  vertical-align: top;
}

.d-1 {
  width: 30%;
}

.d-2 {
  width: 70%;
}

.h3 {
  margin: 0;
}

.dummy-text {
  text-align: center;
  margin-bottom: 0;
}
</style>
@endsection
@section('content')
<?php 
use App\Helpers\GlobalHelper;

?>
<table class="struk">
  <tr>
    <td colspan="2">
      <h2 class="h2">STRUK PEMBAYARAN TAGIHAN LISTRIK</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">ID PEL</div><div class="d-2">: {{ $data['customer_id'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">BL/TH</div><div class="d-2">: {{ $data['period'] }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">NAMA</div><div class="d-2">: {{ $data['customer_name'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">STAND METER</div><div class="d-2">: {{ $data['stand_meter'] }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TARIF/DAYA</div><div class="d-2">: {{ $data['power_rate'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">RP TAG PLN</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['amount']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">REF</div>
      <div class="d-2">: {{ $data['ref_no'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <h2 class="text-center">PLN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">ADMIN BANK</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['admin_charge']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TOTAL BAYAR</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['total_amount']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="mb-3">
        <p class="dummy-text">{{ $data['info_text1'] }}</p>
        <p class="dummy-text">{{ $data['info_text2'] }}</p>
        <p class="dummy-text">{{ $data['info_text3'] }}</p>
        <p class="dummy-text">{{ $data['info_text4'] }}</p>
      </div>
    </td>
  </tr>
</table>
@endsection