@extends('templates.mails.template')

@section('style')
<style>
table.container {
  max-width: 1200px;
}

.hi-text {
  display: none;
}

table.struk {
  width: 100%;
  background: #f1f1f1;
  border: solid 2px #dedede;
  padding: 10px 40px;
}

.h2 {
  text-align: center;
  margin-top: 0;
}

.w-50 {
  width: 50%;
  max-width: 50%;
  min-width: 50%;
  white-space: nowrap;
  vertical-align: top;
  padding: 0 8px 0 0;
}

.w-50 + .w-50 {
  padding: 0 0 0 8px;
}

.d-1,
.d-2 {
  display: inline-block;    
  white-space: pre-wrap;
  word-break: break-all;
  vertical-align: top;
}

.d-1 {
  width: 30%;
}

.d-2 {
  width: 70%;
}

.h3 {
  margin: 0;
}
</style>
@endsection
@section('content')
<table class="struk">
  <tr>
    <td colspan="2">
      <h2 class="h2">STRUK PEMBELIAN LISTRIK PRABAYAR</h2>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">NO METER</div><div class="d-2">: {{ $data['meter_no'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">ADMIN BANK</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['admin_charge']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">ID PEL</div><div class="d-2">: {{ $data['customer_id'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">MATERAI</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['stamp_duty']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">NAMA</div><div class="d-2">: {{ $data['customer_name'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">PPN</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['ppn']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">TARIF/DANA</div><div class="d-2">: {{ $data['power_rate'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">PPJ</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['ppj']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">REF</div><div class="d-2">: {{ $data['ref_no'] }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">ANGSURAN</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['installment']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1">RP BAYAR</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['amount']) }}</div>
    </td>
    <td class="w-50">
      <div class="d-1">RP STROOM/TOKEN</div><div class="d-2">: {{ GlobalHelper::toCurrency($data['pp']) }}</div>
    </td>
  </tr>
  <tr>
    <td class="w-50">
      <div class="d-1"></div><div class="d-2"></div>
    </td>
    <td class="w-50">
      <div class="d-1">JML KWH</div><div class="d-2">: {{ $data['kwh'] }}</div>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <h3 class="h3"><strong>STROOM/TOKEN : {{ $data['sn'] }}</strong></h3>
    </td>
  </tr>
  <tr>
    <td colspan="2"><br></td>
  </tr>
  <tr>
    <td colspan="2">
      <p class="text-center">{{ $data['info_text'] }} {{ $data['service_unit_phone'] }} </p>
    </td>
  </tr>
</table>
@endsection