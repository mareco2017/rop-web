<div class="container">
	<p>Dear {{ $user->fullname }},</p>
	<br/>
	<p>Your verification PIN is <b>{{ $otp->pin }}</b>. The PIN only works for 5 minutes.</p>
	<p>Do not share this PIN to anyone.</p>
	<br/>
	<p>Thanks,</p>
	<p>ROP Customer Support</p>
</div>