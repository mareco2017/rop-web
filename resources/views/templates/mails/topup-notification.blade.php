@extends('templates.mails.template')

@section('style')
<style>
  .price {
    font-size: 32px;
    font-weight: bold;
  }
</style>
@endsection
@section('content')
<p class="mb-1">Your Top Up have been successfully made</p>
<p class="text-grey">Thank you for using Rich On Pay, Enjoy cashless with us</p>
<br>
<p class="label-grey-1">Top Up Amount</p>
<p class="price">{{ GlobalHelper::toCurrency($notification->options['amount']) }}</p>
@endsection