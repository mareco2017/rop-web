<?php
$currentRoute = Route::getCurrentRoute();
$routeCollection = Route::getRoutes();
$routes = [
    (object) [
        'title' => 'Dashboard',
        'icon' => 'now-ui-icons design_app',
        'href' => route('admin.dashboard'),
        'collection' => $routeCollection->getRoutesByName()['admin.dashboard']
    ],
    (object) [
        'title' => 'Admins',
        'icon' => 'now-ui-icons clothes_tie-bow',
        'child' => [
            (object) [
                'title' => 'admin List',
                'icon' => 'AL',
                'href' => route('admin.admins.admin.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.admins.admin.index']
            ],
        ]
    ],
    (object) [
        'title' => 'Users',
        'icon' => 'now-ui-icons users_circle-08',
        'child' => [
            (object) [
                'title' => 'User List',
                'icon' => 'UL',
                'href' => route('admin.users.user.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.user.index']
            ],
            (object) [
                'title' => 'Upgrade Package',
                'icon' => 'UP',
                'href' => route('admin.users.upgrade-request.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.upgrade-request.index']
            ],
            (object) [
                'title' => 'Verify Identity',
                'icon' => 'VI',
                'href' => route('admin.users.verify-identity.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.verify-identity.index']
            ],
            (object) [
                'title' => 'Pair',
                'icon' => 'P',
                'href' => route('admin.users.pair.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.pair.index']
            ],
        ]
    ],
    (object) [
        'title' => 'Promos',
        'icon' => 'now-ui-icons media-1_album',
        'child' => [
            (object) [
                'title' => 'Banner',
                'icon' => 'B',
                'href' => route('admin.promos.banner.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.promos.banner.index']
            ],
        ]
    ],
    (object) [
        'title' => 'System Config',
        'icon' => 'now-ui-icons ui-2_settings-90',
        'href' => route('admin.config.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.config.index']
    ],
    (object) [
        'title' => 'Bank',
        'icon' => 'now-ui-icons business_bank',
        'href' => route('admin.bank.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.bank.index']
    ],
    (object) [
        'title' => 'Bank Account',
        'icon' => 'now-ui-icons business_badge',
        'href' => route('admin.bank-account.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.bank-account.index']
    ],
    (object) [
        'title' => 'Top Up',
        'icon' => 'now-ui-icons arrows-1_cloud-upload-94',
        'href' => route('admin.top-up.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.top-up.index']
    ],
    (object) [
        'title' => 'Withdraw',
        'icon' => 'now-ui-icons arrows-1_cloud-download-93',
        'href' => route('admin.withdraw.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.withdraw.index']
    ],
    (object) [
        'title' => 'Payment',
        'icon' => 'now-ui-icons business_money-coins',
        'href' => route('admin.payment.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.payment.index']
    ],
    (object) [
        'title' => 'Payment Product',
        'icon' => 'now-ui-icons business_money-coins',
        'href' => route('admin.payment-product.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.payment-product.index']
    ],
]
?>
<div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="{{ asset('/assets/images/img-placeholder.png') }}" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <span>
                    {{ Auth::user()->fullname }}
                    <b class="caret"></b>
                </span>
            </a>
            <div class="clearfix"></div>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <!-- <li>
                        <a href="#">
                            <span class="sidebar-mini-icon">MP</span>
                            <span class="sidebar-normal">My Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="sidebar-mini-icon">EP</span>
                            <span class="sidebar-normal">Edit Profile</span>
                        </a>
                    </li> -->
                    <li>
                        {!! Form::open(['route' => 'admin.logout', 'method' => 'POST', 'name' => 'logoutForm']) !!}
                        {!! Form::close() !!}
                        <a href="" onclick="document.forms['logoutForm'].submit(); return false;">
                            <span class="sidebar-mini-icon">LO</span>
                            <span class="sidebar-normal">Log Out</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        @foreach($routes as $key => $route)
            <?php
                if (isset($route->collection) && $route->collection) {
                    $active = $route->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                } else if (isset($route->child) && $route->child) {
                    $hasAccess = false;
                    $active = false;
                    $open = false;
                    foreach ($route->child as $childRoute) {
                        if (isset($childRoute->collection) && $childRoute->collection) {
                            if ($childRoute->collection->getPrefix() == $currentRoute->getPrefix()) {
                                $active = 'active';
                                $open = true;
                            }
                        }
                    }
                }
            ?>
            @if(isset($route->child))
                <li class="{{ $active }}">
                    <a data-toggle="collapse" href="#{{ 'dropdown'.$key }}" aria-expanded="{{ $open ? 'true' : 'false' }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse {{ !$open ?: 'show' }}" id="{{ 'dropdown'.$key }}">
                        <ul class="nav mt-0">
                            @foreach($route->child as $child)
                            <?php
                                if (isset($child->collection) && $child->collection) {
                                    $childActive = $child->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                                }
                            ?>
                            <li class="{{ $childActive }}">
                                <a href="{{ $child->href }}">
                                    <span class="sidebar-mini-icon">{{ $child->icon }}</span>
                                    <span class="sidebar-normal">{{ $child->title }}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @else
                <li class="{{ $active }}">
                    <a href="{{ $route->href }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}</p>
                    </a>
                </li>
            @endif
        @endforeach
        <!-- <li class="active">
            <a href="/admin">
                <i class="now-ui-icons design_app"></i>
                <p>Dashboard</p>
            </a>
        </li> -->

        <!-- <li>
            <a data-toggle="collapse" href="#pagesExamples">
                <i class="now-ui-icons design_image"></i>
                <p>Example 2
                    <b class="caret"></b>
                </p>
            </a>

            <div class="collapse" id="pagesExamples">
                <ul class="nav">
                    <li>
                        <a href="../examples/pages/pricing.html">
                            <span class="sidebar-mini-icon">C1</span>
                            <span class="sidebar-normal">Collapse 1</span>
                        </a>
                    </li>
                    <li>
                        <a href="../examples/pages/timeline.html">
                            <span class="sidebar-mini-icon">C2</span>
                            <span class="sidebar-normal">Collapse 2</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li> -->
    </ul>
</div>