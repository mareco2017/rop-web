<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\SMS;
use App\Helpers\Managers\HistoryManager;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('sms', function ($app) {
            return new SMS();
        });

        $this->app->singleton('history_logger', function ($app) {
            return new HistoryManager();
        });
    }
}
