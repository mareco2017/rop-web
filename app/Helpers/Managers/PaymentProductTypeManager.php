<?php 
 
namespace App\Helpers\Managers; 
 
use DB; 
use Exception; 
use App\Models\User; 
use App\Models\PaymentProductType; 
use AttachmentManager;
 
class PaymentProductTypeManager 
{ 
    protected $user; 
 
    public function __construct($user = null) 
    { 
        $this->user = $user; 
    }

    public function update(PaymentProductType $paymentProductType, $data)
    {
        $paymentProductType->product_code = $data['product_code'];
        $paymentProductType->status = $data['status'];
        $paymentProductType->method = isset($data['method']) ? $data['method'] : null;
        $paymentProductType->options = isset($data['options']) && $data['options'] > 0 ? $data['options'] : null;

        if (!$paymentProductType->save()) {
            throw new Exception('Failed to update type!', 500);
        }

        $this->updateCover($paymentProductType, $data);

        return $paymentProductType;
    }

    public function updateCover(PaymentProductType $paymentProductType, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $paymentProductType->cover;

            $attachmentManager = new AttachmentManager('s3');
            $attachment = $attachmentManager->setAttachable($paymentProductType)->create($data['cover']);
            $paymentProductType->cover()->associate($attachment);

            if (!$paymentProductType->save()) {
                throw new Exception('Failed to update type cover!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(PaymentProductType $paymentProductType)
    {
        $attachmentManager = new AttachmentManager('s3');
        if ($paymentProductType->picture) {
            $attachment = $attachmentManager->delete($paymentProductType->picture);
        }
        $paymentProductType->delete();

        return $paymentProductType;
    }
}