<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
// use Carbon\Carbon;
use App\Models\Admin;
use App\Models\Role;
// use App\Models\Config as AppConfig;
// use App\Helpers\Enums\WalletType;
// use App\Helpers\Enums\VerificationStatus;
// use App\Helpers\Managers\EventManager;
// use Illuminate\Support\Facades\Hash;
use Propaganistas\LaravelPhone\PhoneNumber;
// use WalletManager;

class AdminManager
{
    protected $user;
    protected $countryCode;

    public function __construct($user = null)
    {
        $this->user = $user;
        $this->countryCode = 'ID';
    }

    // public function createBusinessOwner($data)
    // {
    //     $roleId = AppConfig::where('key', 'business_register_role')->value('value');
    //     $role = Role::find($roleId);
    //     if (!$role) throw new Exception('Business Owner Role Not Found', 500);    
    //     $admin = new Admin;
    //     $admin = $this->create($admin, $role, $data);

    //     return $admin;
    // }

    public function create(Admin $admin, Role $role, $data)
    {
        $admin->first_name = $data['first_name'];
        $admin->last_name = $data['last_name'];
        
        $admin->phone_number = PhoneNumber::make($data['phone_number'], $this->countryCode);
        if (isset($data['password'])) {
            $admin->password = bcrypt($data['password']);
        }
        $admin->email = isset($data['email']) ? $data['email'] : null;
        $admin->role()->associate($role);
        
        // if (array_key_exists('referral_code', $data) && $data['referral_code'] != null) {
        //     $eventManager = new EventManager(); 
        //     $admin->options = [
        //         'referral_user_id' => $eventManager->findReferral($data['referral_code']),
        //         'verify_referral' => VerificationStatus::PENDING
        //     ];
        // }

        if (!$admin->save()) {
            throw new Exception('Failed create user!', 500);
        }

        // $createWallet = $this->initializeWallets($admin);

        return $admin;
    }

    public function delete($admin)
    {
        $admin->delete();
        return $admin;
    }

    // public function changePassword($data)
    // {
    //     if (!Hash::check($data['old_password'], $this->user->password)) throw new Exception('Old Password Not Match!', 500);
        
    //     $this->user->password = bcrypt($data['new_password']);
        
    //     if (!$this->user->save()) {
    //         throw new Exception('Failed to save new password!', 500);
    //     }
    // }

    // public function changeGeneralInformation($data)
    // {
    //     $user = $this->user;
        
    //     $user->first_name = $data['first_name'];
    //     $user->last_name = $data['last_name'];
    //     $user->email = $data['email'];
    //     // $user->phone_number = $data['phone_number'];
        
    //     if (!$user->save()) {
    //         throw new Exception('Failed to save new password!', 500);
    //     }
    // }

    // public function findAdminByPhoneNumber($phoneNumber)
    // {
    //     if (is_numeric($phoneNumber)) {
    //         $credential = $this->convertPhoneNumber($phoneNumber);
    //         $credentialType = 'phone_number';
    //     }

    //     if (is_null($credentialType)) throw new Exception('Admin with that credential not found!', 404);

    //     $admin = Admin::where($credentialType, $credential)->first();
    //     if (!$admin) throw new Exception('Admin with phone number '. $credential .' not found!', 404);

    //     return $admin;
    // }

    // public function convertPhoneNumber($phoneNumber)
    // {
    //     return (string) PhoneNumber::make($phoneNumber, $this->countryCode);
    // }

    // public function initializeWallets($ownable)
    // {
    //     $walletManager = new WalletManager($ownable);
    //     $wallets = [];
        
    //     foreach (WalletType::getList() as $walletType) {
    //         $wallets[] = $walletManager->create($ownable, $walletType);
    //     }

    //     return $ownable;
    // }

}
