<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\Order;
use App\Models\User;
use App\Models\Business;
use App\Models\Promo;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\Config as AppConfig;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\WalletType;
use App\Facades\HistoryLogger;
use WalletManager;

class OrderManager
{
    protected $user;
    protected $date;
    protected $reference_number;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setRefereable($user)
    {
        $this->referable = $referable;
        return $this;
    }

    public function setDate(Carbon $date)
    {
        $this->date = $date;
        return $this;
    }

    public function setReferenceNumber($reference_number)
    {
        $this->reference_number = $reference_number;
        return $this;
    }

    public function update(Order $order, $data, $ownable = null, $referable = null)
    {
        if(isset($ownable) && isset($referable)){
            $order->ownable = associate($ownable);
            $order->referable = associate($referable);
        }
        $order->type = $data['type'];
        $order->status = $data['status'];
        $order->transaction_id = $data['transaction_id'];

        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . __('error/messages.update_order'), 500);
        }
        return $order;
    }

    // API =========================================================================================
    public function ongoingOwned($status, $transaction_type = null)
    {
        if (!$this->user) throw new Exception(__('error/messages.no_user'), 500);
        $types = [OrderType::TOPUP, OrderType::WITHDRAW, OrderType::FREEZE, OrderType::TOPUP_CREDIT];
        $query = $this->user->orders()->whereIn('type', $types)->whereIn('status', $status)->with('orderDetails', 'transactions');
        $orders = $query->orderBy('created_at', 'desc')->get();     

        foreach ($orders as $key => $order) {
            $order->total_before_discount = $order->totalBeforeDiscount;
        }
        if ($orders->count() == 0){
            $orders = [];
        }
        return $orders;
    }

    public function owned($ownerWalletIds, $limit, $offset = 0, $transaction_type = null, $startDate = null, $endDate = null)
    {
        $walletId = null;
        if (!$this->user) throw new Exception(__('error/messages.no_user'), 500);
        $excludeType = [TransactionType::FREEZE, TransactionType::UNFREEZE, TransactionType::BONUS];
        if ($transaction_type == "0") { // receive
            $query = Transaction::whereIn('debit_wallet_id', $ownerWalletIds)->WhereNotIn('type', $excludeType);
        } else if ($transaction_type == "1") {
            $query = Transaction::whereIn('credit_wallet_id', $ownerWalletIds)->WhereNotIn('type', $excludeType);
        } else {
            $excludeTypeStr = '('.implode(',', $excludeType).')';
            $ownerWalletIdsStr = '('.implode(',', $ownerWalletIds).')';
            $rawStr = "`type` not in ". $excludeTypeStr ." and (`credit_wallet_id` in ". $ownerWalletIdsStr . " or `debit_wallet_id` in ". $ownerWalletIdsStr .")";
            $query = Transaction::whereRaw($rawStr);
        }

        // Query start and end date converted to UTC [Carbon]
        if ($startDate && $endDate) {
            $query->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()]);
        } else if ($startDate && !$endDate) {
            $query->whereDate('created_at', '>=', $startDate->toDateTimeString());
        } else if (!$startDate && $endDate) {
            $query->whereDate('created_at', '<=', $endDate->toDateTimeString());
        }

        $transactions = $query->with('order', 'order.orderDetails', 'order.promo')
            ->orderBy('created_at', 'desc')->orderBy('id', 'desc')->skip($offset)->take($limit)->get();
        if(count($transactions) == 0)
        {
            $transactions = [];
            return $transactions; 
        }
        foreach ($transactions as $key => $transaction) {
            $this->alterTransaction($transaction, $ownerWalletIds);
        }
        return $transactions;
    }

    public function transactionDetail($orderId)
    {
        $excludeType = [TransactionType::FREEZE, TransactionType::UNFREEZE, TransactionType::BONUS];
        $transaction = Transaction::where('order_id', $orderId)->WhereNotIn('type', $excludeType)->with('order', 'order.orderDetails', 'order.promo')->first();
        $this->alterTransaction($transaction, $this->user->wallets->pluck('id')->toArray());
        return $transaction;
    }

    public function create($referable, $type, $data)
    {
        if (!$this->user) throw new Exception(__('error/messages.no_user'), 500);
        $status = OrderStatus::PENDING;
        
        $referenceNumber = $this->generateReferenceNumber($type);

        $order = new Order;
        $order->expired_at = isset($data['expired_at']) ? $data['expired_at'] : null;
        $order->promo_id = isset($data['promo_id']) ? $data['promo_id'] : null;
        $order->type = $type;
        $order->referable()->associate($referable);
        if (isset($this->reference_number)) {
            $referenceNumber = $this->reference_number;   
        }
        $order->reference_number = $referenceNumber;
        $order->ownable()->associate($this->user);
        if (in_array($type, [OrderType::TOPUP, OrderType::TOPUP_CREDIT])) {
            $status = OrderStatus::WAITING_FOR_PAYMENT;
            if ($type == OrderType::TOPUP) $order->expired_at = Carbon::now()->addMinutes(30);
            if ($type == OrderType::TOPUP_CREDIT) $order->expired_at = Carbon::now()->addMinutes(30);
        } else if (in_array($type, [OrderType::WITHDRAW, OrderType::WITHDRAW_CREDIT])) {
            // $status = OrderStatus::PENDING;
        } else if (in_array($type, [OrderType::PLN_PREPAID])) {
            $status = OrderStatus::WAITING_FOR_PAYMENT;
            $order->expired_at = Carbon::now()->addHours(2);
        } else if (in_array($type, [OrderType::PAY, OrderType::TRANSFER, OrderType::SPLIT, OrderType::CASHBACK])) {
            $status = OrderStatus::ONGOING;
        } else if (in_array($type, [OrderType::FREEZE, OrderType::UNFREEZE])) {
            $status = OrderStatus::ONGOING;
        } else if (in_array($type, [OrderType::UPGRADE_PACKAGE])) {
            $status = OrderStatus::COMPLETED;
            $order->expired_at = Carbon::now();
        } else if (in_array($type, [OrderType::SPONSOR_BONUS])) {
            $status = OrderStatus::COMPLETED;
            $order->expired_at = Carbon::now();
        } else if (in_array($type, [OrderType::PAIRING_BONUS])) {
            $status = OrderStatus::COMPLETED;
        }
        
        $order->options = $data['options'] ?? null;
        // if (isset($data['options'])) {
        //     $order->options = $data['options'];
        // }

        if ($this->date) {
            $order->created_at = $this->date;
            $order->updated_at = $this->date;
            $order->timestamps = false;
        }

        $adminFee = 0;
        if ($type == OrderType::PLN_POSTPAID) {
            $adminFee = AppConfig::where('key', 'pln_prepaid_admin')->value('value');
        } else if ($type == OrderType::PHONE_PREPAID) {
            if ($this->user->role_id == 5) {
                $checkLimit = $this->user->orders()->whereMonth('created_at', date('n'))->where('status', OrderStatus::COMPLETED)->where('type', '=', OrderType::PHONE_PREPAID)->get()->sum('total');
            }
            if ($this->user->packages == PackageType::FREE) {
                $adminFee = 1500;
            } elseif ($this->user->packages == PackageType::SILVER) {
                if ($checkLimit > 500000) $adminFee = 1300;
            } else if ($this->user->packages == PackageType::GOLD) {
                if ($checkLimit > 1000000) $adminFee = 1200;
            } else {
                if ($checkLimit > 1500000) $adminFee = 1000;
            }
        } 

        $order->status = $status;
        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . 'Failed to create order!', 500);
        }

        if (isset($data['products']) && is_array($data['products'])) {
            foreach ($data['products'] as $product) {
                $detail = [
                    'productable_type' => $product->type ?? null,
                    'productable_id' => $product->id ?? null,
                    'qty' => $product->qty,
                    'price' => $product->price,
                    'total' => ($product->qty * $product->price)+$adminFee,
                    'discount' => $product->discount ?? 0
                ];
                if (isset($product->options)) {
                    $detail['options'] = $product->options;
                }
                if ($this->date) {
                    $detail['created_at'] = $this->date;
                    $detail['updated_at'] = $this->date;
                    $detail['timestamps'] = false;
                }
                $order->orderDetails()->create($detail);
            }
        }
        if (in_array($type, [OrderType::WITHDRAW, OrderType::WITHDRAW_CREDIT])) {
            $withdrawManager = new WithdrawManager($this->user);
            if ($this->date) {
                $withdrawManager->setDate($this->date);
            }
            $withdrawManager->setOrder($order);
            $frozeBalance = $withdrawManager->frozeBalance();
        } else if (in_array($type, [OrderType::PAY, OrderType::TRANSFER, OrderType::CASHBACK])) {
            $order = $this->updateStatus($order, OrderStatus::COMPLETED);
        }
        
        $history = HistoryLogger::create($order, $this->user, $order->status);
        return $order;
    }

    public function topupPaid(Order $order)
    {
        if (!$this->user) throw new Exception('Failed to paid '. OrderType::getString(OrderType::TOPUP) .', no user binded!', 500);
        if (!$order->ownedBy($this->user)) throw new Exception('Not authorized to paid '. OrderType::getString(OrderType::TOPUP) .'!', 400);
        if (!$order->type == OrderType::TOPUP) throw new Exception('Not a '. OrderType::getString(OrderType::TOPUP) .' order!', 400);
        if (!in_array($order->status, [OrderStatus::WAITING_FOR_PAYMENT])) throw new Exception('Order status is not '. OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT) .'!', 400);
        $update = $this->updateStatus($order, OrderStatus::ONGOING);
        return $update;
    }

    public function setExpired(Order $order)
    {
        if ($order->status !== OrderStatus::WAITING_FOR_PAYMENT) return false;
        
        $update = $this->updateStatus($order, OrderStatus::EXPIRED);
        return $update;
    }

    public function refund(Order $order, Transaction $transaction, $options = [])
    {
        $refundable = false;
        $refundTimeLimit = 30;
        if ($transaction->created_at->diffInMinutes(Carbon::now()) <= $refundTimeLimit) $refundable = true;
        if (!in_array($order->status, [OrderStatus::COMPLETED])) throw new Exception("Unable to refund order!", 400);
        if (!$refundable) throw new Exception("Unable to refund order, beyond refund time limit.", 400);
        if (!in_array($order->type, [OrderType::PAY])) {
            throw new Exception("Unable to refund order!", 400);
        }

        $paymentManager = new PaymentManager;
        $refund = $paymentManager->refund($order, $transaction, $options);
        
        return $refund;
    }

    public function forceRefund(Order $order, $options = []) {
        if (!in_array($order->status, [OrderStatus::COMPLETED])) throw new Exception("Unable to refund order!", 400);

        $refundAmount = $order->total;
        $ownable = $order->ownable;

        if (!$ownable || !($ownable instanceof User)) {
            throw new Exception("Something went wrong, please try again later.", 500);
        }

        $wallet = $ownable->getWallet(WalletType::PRIMARY);

        $this->refundOrderDetails($order);

        $wallet->add($refundAmount);

        $update = $this->updateStatus($order, OrderStatus::REFUNDED, $options);
        return $update;
    }

    protected function refundOrderDetails(Order $order) {
        foreach ($order->orderDetails as $d) {
            $options = (object) [
                'backup_price' => $d->price,
                'backup_total' => $d->total,
                'backup_discount' => $d->discount,
            ];

            $d->price = 0;
            $d->total = 0;
            $d->discount = 0;
            $d->options = (object) array_merge((array) $d->options, (array) $options);

            if (!$d->save()) {
                throw new Exception('Failed to update order detail!', 500);
            }
        }

        return $order;
    }

    protected function updateStatus(Order $order, $status, $options = [])
    {
        $order->status = $status;
        $currentOptions = [];
        if (is_array((array)$order->options)) {
            (array) $currentOptions = $order->options;
        }
        $mergedOptions = array_merge((array)$currentOptions, $options);
        $order->options = empty($mergedOptions) ? null : $mergedOptions;
        if ($this->date) {
            $order->updated_at = $this->date;
            $order->timestamps = false;
        }
        if (!$order->save()) {
            throw new Exception('Failed to update order status!', 500);
        }
        $history = HistoryLogger::create($order, $this->user, $order->status);
        return $order;
    }

    protected function updateOptions(Order $order, $options = [])
    {
        $currentOptions = [];
        if (is_array($order->options)) {
            $currentOptions = $order->options;
        }
        $mergedOptions = array_merge($currentOptions, $options);
        $order->options = empty($mergedOptions) ? null : $mergedOptions;
        if ($this->date) {
            $order->updated_at = $this->date;
            $order->timestamps = false;
        }
        if (!$order->save()) {
            throw new Exception('Failed to update order options!', 500);
        }
        
        return $order;
    }

    protected function generateReferenceNumber($type)
    {
        $code = OrderType::getCode($type);
        $reference = $code.rand(100000,999999);
        $referenceNumber = $reference.chr(rand(65,90));

        // $minLength = 3;
        // $latest = Order::where('reference_number', 'like', $reference.'%')->max('reference_number');
        // if (!$latest) {
        //     $referenceNumber = $reference.str_pad('1', $minLength, "0", STR_PAD_LEFT).chr(rand(65,90));
        // } else {
        //     // dd($latest);
        //     $number = (int) substr($latest, strlen($reference));
        //     $number++;
        //     if (strlen((string) $number) > $minLength) {
        //         $minLength = strlen((string) $number);
        //     }
        //     $referenceNumber = $reference.str_pad($number, $minLength, "0", STR_PAD_LEFT).chr(rand(65,90));
        // }
        
        return $referenceNumber;
    }

    public function alterTransaction($transaction, $ownerWalletIds)
    {
        if (in_array($transaction->debit_wallet_id, $ownerWalletIds)){
            $transaction->transaction_type = 0;
            $walletId = $transaction->debit_wallet_id;
            $transaction->reference = Wallet::find($transaction->credit_wallet_id)->ownable->fullname;
            $transaction->reference_phone_number = Wallet::find($transaction->credit_wallet_id)->ownable->phone_number;
        } else {
            $transaction->transaction_type = 1;
            $walletId = $transaction->credit_wallet_id;
            $transaction->reference = Wallet::find($transaction->debit_wallet_id)->ownable->fullname;
            $transaction->reference_phone_number = Wallet::find($transaction->debit_wallet_id)->ownable->phone_number;
        }
        $transaction->order->setHidden(['referable']);
        $transaction->order->total_before_discount = $transaction->order->totalBeforeDiscount;
        $transaction->wallet_type = Wallet::find($walletId)->type;
        $transaction->type_string = TransactionType::getString($transaction->type);

        // model referable
        if ($transaction->order->referable instanceof User) {
            $transaction->order->user = $transaction->order->referable;
            if ($transaction->order->user->id == $this->user->id) {
                $transaction->order->user = $transaction->order->ownable;
            }
        } else if ($transaction->order->referable instanceof Business) {
            $transaction->order->vendor = $transaction->order->referable;
            $transaction->order->vendor->primary_cat = $transaction->order->referable->primaryCategory->first();
        } else if ($transaction->order->referable instanceof Admin) {
            $transaction->order->admin = $transaction->order->referable;
            $transaction->reference = "Mareco";
        }
        if ($transaction->type == TransactionType::SPLIT_PAY) {
            $channelDetail = new ChannelDetail;
            $channelDetail = $channelDetail->where('order_id', $transaction->order->id)->with('channel')->first();
            $transaction->channel = $channelDetail->channel->load('channelDetails', 'channelDetails.ownable');
            $transaction->wallet_type = 0; //pasti primary
        } else if ($transaction->type == TransactionType::PLN || $transaction->type == TransactionType::PULSA || $transaction->type == TransactionType::GAME) {
            $productId = $transaction->order->orderDetails[0]->productable_id;
            if ($productId) {
                $product = PaymentProduct::find($productId);
                if (isset($product->type->cover_id)) {
                    $product->cover_url = $product->type->cover_url;
                } else {
                    $product->cover_url = null;
                }
                if (isset($product->category->picture_id)) {
                    $product->picture_url = $product->category->picture_url;
                    $product->category_name = $product->category->category;
                } else {
                    $product->picture_url = null;
                    $product->category_name = null;
                }
                $product->setHidden(['type', 'category']);
                if ($product){
                    $transaction->order->orderDetails[0]->product = $product;
                }
                $transaction->payment_product_amount = ($product->amount == 0) ? $transaction->total_amount : $product->price;
                if ($transaction->order->type == OrderType::PLN_POSTPAID){
                    $transaction->payment_product_amount = $transaction->order->options['amount'];
                } 
                $transaction->payment_product_fee = 0;
                if ($product->price == 0 && array_key_exists('admin_charge', $transaction->order->options)){
                    $transaction->payment_product_fee = $transaction->order->options['admin_charge'];
                }
            }
        } else if ($transaction->type == TransactionType::TOPUP) { 
            $relatedTransaction = Transaction::where('related_transaction_id', $transaction->id)->first();
            if (isset($relatedTransaction)) {
                $transaction->total_amount = $transaction->total_amount + $relatedTransaction->total_amount;
            }
        }
    }

    public function unreviewedTransaction()
    {
        $recentOrder = null;
        $recentOrder = Order::where('ownable_id', $this->user->id)->recent()->complete()->needReview()->orderBy('created_at','desc')->select('id', 'ownable_id', 'ownable_type', 'referable_id', 'referable_type')->first();
        if (isset($recentOrder)) {
            if($recentOrder->referable == $recentOrder->ownable) {
                $recentOrder = null;
            } else {
                $recentOrder['vendor'] = Business::select('id', 'name', 'phone_number')->find($recentOrder->referable_id)->setHidden(['balances']);
                $recentOrder->setHidden(['referable', 'ownable']);
            }
        }
        return $recentOrder;
    }

    public function finishOrder(Business $business, Order $order, $orderData = [])
    {
        $order->referable()->associate($business);
        $order->type = $orderData['type'];
        $order->status = $orderData['status'];
        $order->options = (object) $orderData['options'];

        if (!$order->save()) {
            throw new Exception('Failed to update order!', 500);
        }
        return $order;
    }

    public function customOrder($user, $orderData = [])
    {
        $relatedOrder = Order::find($orderData['order_id']);
        //debit user
        if ($orderData['ownable_user_category'] == UserCategory::ADMIN) {
            $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
            $ownableUser = Admin::find($id);
        } else if ($orderData['ownable_user_category'] == UserCategory::BUSINESS) {
            $ownableUser = Business::find($orderData['ownable_business_id']);
        } else if ($orderData['ownable_user_category'] == UserCategory::USER) {
            $ownableUser = User::find($orderData['ownable_user_id']);
        } 
        if (!$ownableUser) throw new Exception('Something went wrong, please contact customer support!', 400);
        //credit user
        if ($orderData['referable_user_category'] == UserCategory::ADMIN) {
            $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
            $referableUser = Admin::find($id);
        } else if ($orderData['referable_user_category'] == UserCategory::BUSINESS) {
            $referableUser = Business::find($orderData['referable_business_id']);
        } else if ($orderData['referable_user_category'] == UserCategory::USER) {
            $referableUser = User::find($orderData['referable_user_id']);
        } 
        if (!$referableUser) throw new Exception('Something went wrong, please contact customer support!', 400);

        $this->setUser($ownableUser);
        $status = $orderData['order_status'];
        $type = $orderData['order_type'];
        $referenceNumber = $this->generateReferenceNumber($type);

        $order = new Order;
        $order->type = $type;
        $order->ownable()->associate($this->user);
        $order->referable()->associate($referableUser);
        if (isset($this->reference_number)) {
            $referenceNumber = $this->reference_number;   
        }
        $order->reference_number = $referenceNumber;
        $order->options = [
            'note' => array_key_exists('note', $orderData) ? $orderData['note'] : null, 
            'related_order_id' => (isset($relatedOrder)) ? $relatedOrder->id : null
        ];

        $order->status = $status;
        if (!$order->save()) {
            throw new Exception(__('error/messages.failed') . 'Failed to create order!', 500);
        }
        $discount = 0;
        if (array_key_exists('discount', $orderData) && $orderData['discount'] != null){
            $discount = $orderData['discount'];
        }
        $detail = [
            'productable_type' => null,
            'productable_id' => null,
            'qty' => 1,
            'price' => $orderData['total'],
            'total' => $orderData['total'] * 1,
            'discount' => $discount
        ];
        $order->orderDetails()->create($detail);
        $history = HistoryLogger::create($order, $user, $order->status);

        $transactionManager = new TransactionManager($ownableUser);
        if ($order) {
            $transactionManager->setOrder($order);
        }
        if (in_array($type, OrderType::getOwnableDebitList())){
            $debitUser = $ownableUser;
            $creditUser = $referableUser;
        } else if (in_array($type, OrderType::getOwnableCreditList())){
            $creditUser = $ownableUser;
            $debitUser = $referableUser;
        } else if ($type == OrderType::REVERSAL && in_array($relatedOrder->type, OrderType::getOwnableDebitList())){
            $creditUser = $ownableUser;
            $debitUser = $referableUser;
        } else if ($type == OrderType::REVERSAL && in_array($relatedOrder->type, OrderType::getOwnableCreditList())){
            $debitUser = $ownableUser;
            $creditUser = $referableUser;
        }

        $debitWallet = $order->referable ? $order->referable->getWallet($orderData['ownable_wallet_type']) : null; 
        $creditWallet = $order->ownable ? $order->ownable->getWallet($orderData['referable_wallet_type']) : null; 
        $transactionManager->create($debitWallet, $creditWallet, $order->total, OrderType::getTransactionType($type));

        return $order;
    }
}
