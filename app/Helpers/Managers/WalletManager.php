<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\User;
use App\Models\Wallet;
use App\Helpers\Enums\WalletType;

class WalletManager
{
    protected $user;
    protected $admin;

    public function __construct($user = null)
    {
        if ($user instanceof User) $this->user = $user;
        if ($user instanceof Admin) $this->admin = $user;
    }

    public function create($ownable, $type, $balance = 0)
    {
        if (!$ownable) throw new Exception("Failed to create ". WalletType::getString($type) .", no ownable binded!", 500);

        if (!$ownable instanceof User && !$ownable instanceof Admin && !$ownable instanceof Business) {
            throw new Exception("Failed to create ". WalletType::getString($type) .", invalid ownable instance!", 500);
        }

        $wallet = $ownable->wallets()->firstOrCreate([
            'type' => $type
        ]);

        if ($balance > 0) {
            $wallet->balance += $balance;
        }

        if (!$wallet->save()) {
            throw new Exception('Something went wrong while creating wallet!', 500);
        }

        return $wallet;
    }

    public function froze($ownable, $amount)
    {
        $primary = $ownable->getWallet(WalletType::PRIMARY);
        if (!$primary) throw new Exception("Failed to froze, ". WalletType::getString($type) ." wallet not found!", 400);
        if ($primary->balance < $amount) throw new Exception("Failed to froze, insufficient balance!", 400);

        $frozen = $ownable->getWallet(WalletType::FROZEN);
        $this->add($primary, $frozen, $amount);

        return $ownable;
    }

    public function add(Wallet $sourceWallet, Wallet $toWallet, $amount)
    {
        $sourceWallet->deduct($amount);
        $toWallet->add($amount);
        return true;
    }

    public function deduct(Wallet $sourceWallet, Wallet $toWallet, $amount)
    {
        $toWallet->deduct($amount);
        $sourceWallet->add($amount);
        return true;
    }

    public function initializeWallets($ownable)
    {
        $wallets = [];
        
        foreach (WalletType::getList() as $walletType) {
            $wallets[] = $this->create($ownable, $walletType);
        }

        return $ownable;
    }
}
