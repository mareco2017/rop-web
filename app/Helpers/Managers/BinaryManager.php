<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\User;
use App\Models\Binary;
use App\Helpers\Enums\BinaryType;

class BinaryManager
{
    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($leaderId)
    {
        if (!$this->user) throw new Exception("No user binded!", 500);

        $binary = new Binary;

        if ($leaderId) {
            $leaderUser = User::find($leaderId);
            if (!$leaderUser) throw new Exception("Leader user not found!", 500);

            $leaderBinary = $leaderUser->binary;
            if ($leaderBinary->isFull()) throw new Exception("Leader binary full!", 500);
            
            $leftOrRight = !$leaderBinary->left_binary_id ? BinaryType::LEFT : BinaryType::RIGHT;

            $binary->leaderBinary()->associate($leaderBinary);
            if ($this->user->sponsor) {
                $binary->sponsorUser()->associate($this->user->sponsor);
            }
        } else {
            if (Binary::getRoot()->first()) throw new Exception("Root Binary exist, Cannot create another root binary!", 500);
        }
        $binary->user()->associate($this->user);
        $binary->type = $this->user->packages;
        
        if (!$binary->save()) throw new Exception('Failed to save binary', 500);
        if ($leaderId) $this->updateLeaderBinary($leaderBinary, $binary, $leftOrRight);

        return $binary;
    }

    public function updateLeaderBinary($leaderBinary, $childBinary, $leftOrRight)
    {
        if ($leftOrRight == BinaryType::LEFT) {
            $leaderBinary->leftBinary()->associate($childBinary);
        } else if ($leftOrRight == BinaryType::RIGHT) {
            $leaderBinary->rightBinary()->associate($childBinary);
        }

        if (!$leaderBinary->save()) throw new Exception('Failed to update leader binary', 500);
    }
}