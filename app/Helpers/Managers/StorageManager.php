<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Storage;

class StorageManager
{
    protected $directory;
    protected $format;
    protected $maxSame;

    public function __construct($directory = '/public/', $format ='.txt', $maxSame = 10)
    {
        $this->directory = $directory;
        $this->format = $format;
        $this->maxSame = $maxSame;
    }

    public function get($path = null, $fileName = null)
    {
        return Storage::exists($path ?? $this->fullpath($fileName)) ? Storage::get($path ?? $this->fullpath($fileName)) : null;
    }

    public function getAll($directory = null)
    {
        return Storage::files($directory ?? $this->directory);
    }

    public function update($content, $fileName, $overwrite = false)
    {
        $exist = $this->get(null, $fileName);
        
        if (!$exist || $overwrite) return Storage::put($this->fullpath($fileName), $content);

        for ($i=1; $i < $this->maxSame; $i++) {
            $exist = Storage::exists($this->directory.$fileName.'-'.$i.$this->format);
            if (!$exist) return Storage::put($this->directory.$fileName.'-'.$i.$this->format, $content);
        }
        
        throw new Exception("Error Saving, Filename already exists", 500);
    }

    public function updateFile(Array $data, $fileName)
    {
        foreach ($data as $key => $value) {
            $updatedFileName = $fileName.'-'.substr($key, -2);
            $update = $this->update($value, $updatedFileName, true);
        }

        return $update;
    }

    public function delete($path = null)
    {
        $exist = $this->get($path);
        if ($exist) Storage::delete($path ?? $this->fullpath());

        throw new Exception("Error Delete, File does not exist", 500);
    }

    public function fullpath($fileName)
    {
        return $this->directory.$fileName.$this->format;
    }

    public function getTermsConditions($lang = 'id')
    {
        return $this->get('/public/terms-conditions/terms-conditions-'.$lang.'.txt');
    }

    public function getPrivacyPolicy($lang = 'id')
    {
        return $this->get('/public/privacy-policy/privacy-policy-'.$lang.'.txt');
    }
}
