<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Carbon\Carbon;
use App\Models\Attachment;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class AttachmentManager
{
    protected $user;
    protected $disk;
    protected $attachable;
    protected $type;

    public function __construct($disk = 'local', $user = null)
    {
        $this->user = $user;
        $this->disk = $disk;
    }

    public function setDisk($disk)
    {
        $this->disk = $disk;
        return $this;
    }

    public function setAttachable($attachable)
    {
        if ($attachable->attachments()) {
            $this->attachable = $attachable;
        }
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function create(UploadedFile $file, $folder = 'images', $description = null, $customType = null)
    {
        $attachment = new Attachment;
        $newName = time().'_'.str_random(15);
        $filePath = "{$folder}/{$newName}.{$file->extension()}";

        $uploaded = Storage::disk($this->disk)->put($filePath, file_get_contents($file), 'public');
        $options = (object) [
            'original_name' => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
            'name' => $newName,
            'extension' => pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION),
            'mime' => $file->getMimeType(),
            'size' => $file->getClientSize(),
            'description' => $description,
            'type' => $customType ? $customType : (isset($this->type) ? $this->type : null),
            'disk' => $this->disk
        ];

        $attachment->key = $filePath;
        $attachment->options = $options;
        if ($this->attachable) {
            $attachment->attachable()->associate($this->attachable);
        }

        if (!$attachment->save()) {
            throw new Exception('Failed to create new attachment!', 500);
        }

        return $attachment;
    }

    public function delete(Attachment $attachment, $force = false)
    {
        $oldFile = $attachment->key;

        if ($force) {
            if (!$attachment->forceDelete()) {
                throw new Exception('Failed to delete attachment!', 500);
            }
            
            if ($oldFile && !$this->remove($oldFile)) {
                $exists = Storage::disk($this->disk)->exists($oldFile);
                if ($exists) {
                    throw new Exception('Failed to delete attachment, error while removing old files!', 500);
                }
            }
        } else {
            if (!$attachment->delete()) {
                throw new Exception('Failed to delete attachment!', 500);
            }            
        }
        
        return true;
    }

    public function remove($files)
    {
        return Storage::disk($this->disk)->delete($files);
    }
}
