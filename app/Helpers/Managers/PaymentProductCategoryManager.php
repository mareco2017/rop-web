<?php 
 
namespace App\Helpers\Managers; 
 
use DB; 
use Exception; 
use App\Models\User; 
use App\Models\PaymentProductCategory; 
use AttachmentManager;
 
class PaymentProductCategoryManager 
{ 
    protected $user; 
 
    public function __construct($user = null) 
    { 
        $this->user = $user; 
    }

    public function update(PaymentProductCategory $paymentProductCategory, $data)
    {
        $paymentProductCategory->category = $data['category'];
        $paymentProductCategory->data = isset($data['data']) && $data['data'] > 0 ? $data['data'] : null;

        if (!$paymentProductCategory->save()) {
            throw new Exception('Failed to update category!', 500);
        }

        $this->updateCover($paymentProductCategory, $data);

        return $paymentProductCategory;
    }

    public function updateCover(PaymentProductCategory $paymentProductCategory, $data)
    {
        if (array_key_exists('picture', $data) && isset($data['picture'])) {
            $oldCover = $paymentProductCategory->picture;

            $attachmentManager = new AttachmentManager('s3');
            $attachment = $attachmentManager->setAttachable($paymentProductCategory)->create($data['picture']);
            $paymentProductCategory->picture()->associate($attachment);

            if (!$paymentProductCategory->save()) {
                throw new Exception('Failed to update category picture!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(PaymentProductCategory $paymentProductCategory)
    {
        $attachmentManager = new AttachmentManager('s3');
        if ($paymentProductCategory->picture) {
            $attachment = $attachmentManager->delete($paymentProductCategory->picture);
        }
        $paymentProductCategory->delete();
    }
}