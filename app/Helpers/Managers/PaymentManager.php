<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use App\Models\Payment;
use App\Models\Pair;
use App\Models\Order;
use Carbon\Carbon;

class PaymentManager
{
    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function update(Payment $payment, $data)
    {
        $pairId = [];
        $orderId = [];
        if (isset($data['pair_ids'])) {
            $pairs = Order::whereIn('id', $data['pair_ids'])->get();
            foreach ($pairs as $key => $pair) {
                $pairId[] .= $pair->id;
                // if (!$pair->save()) {
                //     throw new Exception('Failed to update pair!', 500);
                // }
            }
        }
        if (isset($data['order_ids'])) {
            $orders = Order::whereIn('id', $data['order_ids'])->get();
            foreach ($orders as $key => $order) {
                $orderId[] .= $order->id;
                // if (!$order->save()) {
                //     throw new Exception('Failed to update order!', 500);
                // }
            }
        }

        if (isset($data['pair_ids'])) {
            $pairToArray['pair_id'] = $pairId;
        } else {
            $pairToArray['pair_id'] = null;
        }
        if (isset($data['order_ids'])) {
            $orderToArray['order_id'] = $orderId; 
        } else {
            $orderToArray['order_id'] = null;
        }
        $reference = 'INV'.rand(100000,999999);
        $arrays = array_merge($pairToArray, $orderToArray);
        $payment->statement_no = $reference;
        $payment->options = json_encode($arrays);
        $payment->bank_account_id = $data['bank_account_id'];
        $payment->amount = (int)$data['amount'];
        $payment->reviewed_by_id = $this->user->id;
        $payment->save();

        return $payment;
    }
}
