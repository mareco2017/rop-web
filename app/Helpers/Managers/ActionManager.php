<?php

namespace App\Helpers\Managers;

use Exception;
use Carbon\Carbon;
use App\Models\Action;
use App\Helpers\Enums\ActionStatus;

class ActionManager
{
    protected $user;

    public function __construct($user)
    {
    	$this->user = $user;
    }

    public function create($objectable = null, $name, $description = "", $options = null)
    {
    	$action = new Action;
        $action->name = $name;
        $action->description = $description;
        $action->options = $options;
    	$action->status = ActionStatus::PENDING;
    	$action->userable()->associate($this->user);
    	if ($objectable) {
    		$action->objectable()->associate($objectable);
    	}

    	if (!$action->save()) {
            throw new Exception('Failed to create action!', 500);
    	}

    	return $action;
    }

    public function success(Action $action, $description = "")
    {
        $action->status = ActionStatus::SUCCESS;
        $action->description = $action->description . $description;
        
        if (!$action->save()) {
            throw new Exception('Failed to set action to success!', 500);
        }

        return $action;
    }

    public function error(Action $action, $description = "")
    {
        $action->status = ActionStatus::ERROR;
        $action->description = $action->description . $description;
        
        if (!$action->save()) {
            throw new Exception('Failed to set action to error!', 500);
        }

        return $action;
    }
}
