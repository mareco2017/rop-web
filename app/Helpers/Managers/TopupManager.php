<?php

namespace App\Helpers\Managers;

use App\Facades\HistoryLogger;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\WalletType;
use App\Helpers\Managers\OrderManager;
use App\Models\Admin;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;

class TopupManager extends OrderManager
{
    protected $user;
    protected $order;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function setOrder(Order $order)
    {
        if (!in_array($order->type, [OrderType::TOPUP, OrderType::TOPUP_CREDIT])) throw new Exception('Order type is either '. OrderType::getString(OrderType::TOPUP) .' or '. OrderType::getString(OrderType::TOPUP_CREDIT) .'!', 400);
        $this->order = $order;
        return $this;
    }

    public function paid()
    {
        if (!$this->user) throw new Exception('Failed to paid '. OrderType::getString($this->order->type) .', no user binded!', 500);
        if (!$this->order->ownedBy($this->user)) throw new Exception('Not authorized to paid '. OrderType::getString($this->order->type) .'!', 400);
        if (!in_array($this->order->status, [OrderStatus::WAITING_FOR_PAYMENT])) throw new Exception('Order status is not '. OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT) .'!', 400);
        
        $update = $this->updateStatus($this->order, OrderStatus::ONGOING);
        return $update;
    }

    public function cancel()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);

        $this->updateStatus($this->order, OrderStatus::CANCELLED);
        
        return $this->order;
    }

    public function approve()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);
        if (!$this->order->isActive()) throw new Exception("Order is not active anymore!", 500);
        
        $debitWallet = $this->order->ownable ? $this->order->ownable->getWallet(WalletType::PRIMARY) : null;
        $creditWallet = $this->order->referable ? $this->order->referable->getWallet(WalletType::PRIMARY) : null;

        $transactionManager = new TransactionManager($this->user);
        $transactionManager->setOrder($this->order);
        if ($this->date) {
            $transactionManager->setDate($this->date);
        }

        $transaction = $transactionManager->create($debitWallet, $creditWallet, $this->order->total, TransactionType::TOPUP);
        $bonusTransaction = $transactionManager->setRelatedTransaction($transaction)->create($debitWallet, $creditWallet, $this->order->total_discount, TransactionType::BONUS);

        $options = [];
        if ($this->user) {
            $options['approvable_type'] = $this->user->getMorphClass();
            $options['approvable_id'] = $this->user->id;
            $options['approvable_date'] = Carbon::now();
        }

        $this->updateStatus($this->order, OrderStatus::COMPLETED, $options);

        return $this->order;
    }
     public function decline()
    {
        if (!$this->order) throw new Exception("No order binded!", 500);
        if (!$this->order->isActive()) throw new Exception("Order is not active anymore!", 500);
        $this->updateStatus($this->order, OrderStatus::DECLINED);
        
        return $this->order;
    }
}
