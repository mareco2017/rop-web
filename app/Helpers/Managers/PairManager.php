<?php

namespace App\Helpers\Managers;

use App\Helpers\Enums\NotificationSubType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\PackageType;
use App\Helpers\Managers\NotificationManager;
use App\Helpers\Managers\OrderManager;
use App\Helpers\UniqueHelper;
use App\Models\Admin;
use App\Models\Config as AppConfig;
use App\Models\Pair;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PairManager
{
    public function claim(Request $request, Pair $pair)
    {
        if ($pair->is_leader_claimed) throw new Exception('Pair already claimed!', 500);

        $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
        $referable = Admin::find($id);
        if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);
        $user = User::find($pair->leader_user_id);
        $amount = PackageType::getPairingBonus($pair->leader_package_type);
        $orderManager = new OrderManager($user);
        $orderData = [
            'options' => [
                'dest_bank_account_id' => (int)$request->bank_account_id
            ],
            'products' => [
                (object) [
                    'qty' => 1,
                    'price' => $amount,
                    'discount' => 0 // Unique
                ]
            ]
        ];

        $order = $orderManager->create($referable, OrderType::PAIRING_BONUS, $orderData);
        $pair->is_leader_claimed = 1;
        $pair->claimed_at = Carbon::now();
        $pair->order_id = $order->id;
        if ($request->bank_account_id) $pair->bank_account_id = $request->bank_account_id;

        if (!$pair->save()) {
            throw new Exception('Failed to claim pair!', 500);
        }

        // $notificationType = NotificationType::PAIRING_BONUS;
        // $notificationSubType = NotificationSubType::BANK_TRANSFER;
        // $notificationManager = new NotificationManager($user);
        // $bankAccount = BankAccount::find($approveOrder->options->approvable_id)->with('bank')->first();
        // $bank = $bankAccount->bank->abbr;
        // $notificationData = [
        //     'category' => NotificationType::getCategory($notificationType),
        //     'bank_destination' => 0,
        //     'reference_number' => $order->reference_number,
        //     'order_id' => $order->id,
        //     'amount' => $order->total_before_discount,
        //     'leader_user' => $user,
        //     'left_binary_user' => $pair->leftBinaryUser,
        //     'right_binary_user' => $pair->rightBinaryUser

        // ];
        // $notification = $notificationManager->setType($notificationType)->update($order->ownable, $notificationData, $notificationSubType);

        return $pair;
    }

    public function update(Pair $pair, $data)
    {
        $leader = User::find($data['leader_user_id']);

        $pair->leaderUser()->associate($leader);
        $pair->leftBinaryUser()->associate($data['left_binary_user_id']);
        $pair->rightBinaryUser()->associate($data['right_binary_user_id']);
        $pair->leader_package_type = $leader->packages;
        if (!$pair->save()) {
            throw new Exception('Failed to create pair!', 500);
        }
    }

    public function delete(Pair $pair)
    {
        $pair->delete();
        return $pair;
    }
}
