<?php

namespace App\Helpers\Managers;

use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\UpgradeRequestStatus;
use App\Helpers\Enums\WalletType;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Managers\TransactionManager;
use App\Helpers\UniqueHelper;
use App\Models\Admin;
use App\Models\Config as AppConfig;
use App\Models\Order;
use App\Models\UpgradeRequest;
use App\Models\User;
use Exception;

class UpgradeRequestManager
{
    protected $admin;

    public function __construct($admin = null)
    {
        $this->admin = $admin;
    }

    public function approve(UpgradeRequest $upgradeRequest)
    {
        $latestUserPackage = $upgradeRequest->requestedBy->packages;
        if (!$upgradeRequest->isActive()) throw new Exception('Upgrade Package already reviewed!', 500);
        if (!$this->admin) throw new Exception('Reviewer Admin Not Found!', 500);

        $upgradeRequest->status = UpgradeRequestStatus::ACCEPTED;
        $upgradeRequest->reviewedBy()->associate($this->admin);
        $this->updatePackage($upgradeRequest->requestedBy, $upgradeRequest->package);
        if (!$upgradeRequest->save()) {
            throw new Exception('Failed to save!', 500);
        }
        
        $this->createOrder($upgradeRequest->requestedBy);
        if ($upgradeRequest->requestedBy->sponsor) {
            if ($latestUserPackage == PackageType::FREE) $this->sponsorBonus($upgradeRequest->requestedBy);
        }

        return $upgradeRequest;
    }

    public function decline(UpgradeRequest $upgradeRequest)
    {
        if (!$upgradeRequest->isActive()) throw new Exception('Upgrade Package already reviewed!', 500);
        if (!$this->admin) throw new Exception('Reviewer Admin Not Found!', 500);

        $upgradeRequest->status = UpgradeRequestStatus::DECLINED;
        $upgradeRequest->reviewedBy()->associate($this->admin);

        if (!$upgradeRequest->save()) {
            throw new Exception('Failed to save!', 500);
        }
        return $upgradeRequest;
    }

    public function updatePackage(User $user, $package)
    {
        $user->packages = $package;
        $binary = $user->binary;

        if (!$user->save()) {
            throw new Exception('Failed to update user package!', 500);
        }

        if ($binary) {
            $binary->type = $user->packages;

            if (!$binary->save()) {
                throw new Exception('Failed to update user package!', 500);
            }
        }


        return $user;
    }

    public function delete(UpgradeRequest $upgradeRequest)
    {
        $upgradeRequest->delete();
        return $upgradeRequest;
    }

    public function sponsorBonus($upgradeRequest)
    {
        $downlinePackage = $upgradeRequest->packages;
        $bonus = PackageType::getSponsorBonus($downlinePackage);
        $sponsorId = $upgradeRequest->sponsor;
        $leaderSecondaryWallet = $sponsorId->wallets[WalletType::SECONDARY];
        $leaderSecondaryWallet->balance += $bonus;
        $leaderSecondaryWallet->save();

        $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
        $referable = Admin::find($id);
        if (!$referable) throw new Exception('Something went wrong, please contact customer support!', 400);
        //Create Order
        $orderManager = new OrderManager($upgradeRequest->sponsor);
        $orderData = [
            'options' => [
                'requested_by' => $upgradeRequest,
                'sponsor_by' => $upgradeRequest->sponsor,
                'leader_id' => $upgradeRequest->leader
            ],
            'products' => [
                (object) [
                    'qty' => 1,
                    'price' => $bonus,
                    'discount' => 0 // Unique
                ]
            ]
        ];
        $order = $orderManager->create($referable, OrderType::SPONSOR_BONUS, $orderData);
        //Create Transaction
        // $debitWallet = $order->ownable ? $order->ownable->getWallet(WalletType::SECONDARY) : null;
        // $creditWallet = $order->referable ? $order->referable->getWallet(WalletType::SECONDARY) : null;
        // $transactionManager = new TransactionManager($upgradeRequest->sponsor);
        // $transactionManager->setOrder($order);
        // $transaction = $transactionManager->create($debitWallet, $creditWallet, PackageType::getSponsorBonus($upgradeRequest->packages), TransactionType::SPONSOR_BONUS);

    }
    
    public function createOrder($user)
    {
        $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
        $referable = Admin::find($id);
        if (!$referable) throw new Exception('Something went wrong, please contact customer support!', 400);
        //Create Order
        $upgradePrice = $user->upgradeRequests->last();
        if (!$upgradePrice) throw new Exception('Something went wrong, please contact customer support!', 400);
        
        $orderManager = new OrderManager($user);
        $orderData = [
            'options' => [
                'requested_by' => $user,
                'sponsor_by' => $user->sponsor,
                'leader_id' => $user->leader
            ],
            'products' => [
                (object) [
                    'qty' => 1,
                    'price' => substr($upgradePrice->price, 0, 4)."000",
                    'discount' => substr($upgradePrice->price, -3)
                ]
            ]
        ];
        $order = $orderManager->create($referable, OrderType::UPGRADE_PACKAGE, $orderData);
        //Create Transaction
        // $debitWallet = $order->ownable ? $order->ownable->getWallet(WalletType::SECONDARY) : null;
        // $creditWallet = $order->referable ? $order->referable->getWallet(WalletType::SECONDARY) : null;
        // $transactionManager = new TransactionManager($user);
        // $transactionManager->setOrder($order);
        // $transaction = $transactionManager->create($debitWallet, $creditWallet, PackageType::getPrice($user->packages), TransactionType::UPGRADE_PACKAGE);
    }

}
