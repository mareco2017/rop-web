<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Mutation;
use App\Models\Wallet;
use App\Facades\HistoryLogger;

class MutationManager
{
    protected $user;
    protected $order;
    protected $transaction;
    protected $date;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function setDate(Carbon $date)
    {
        $this->date = $date;
        return $this;
    }

    public function create(Wallet $debitWallet, Wallet $creditWallet, Wallet $ownerWallet)
    {
        if (!$this->transaction) throw new Exception("No transaction binded!", 500);
        $mutation = new Mutation;
        $mutation->debitWallet()->associate($debitWallet);
        $mutation->creditWallet()->associate($creditWallet);
        $mutation->ownerWallet()->associate($ownerWallet);

        $debitOwnable = $debitWallet->ownable;
        $creditOwnable = $creditWallet->ownable;

        if (!$debitOwnable || !$creditOwnable) {
            throw new Exception("Failed to create mutation, debit or credit ownable nof found!", 400);
        }
        
        $mutation->debit_current_balance = $debitOwnable->payWallets()->sum('balance');
        $mutation->credit_current_balance = $creditOwnable->payWallets()->sum('balance');
        if ($this->transaction) {
            $mutation->transaction()->associate($this->transaction);
        }

        if (!$mutation->save()) {
            throw new Exception('Failed to create mutation!', 500);
        }

        if ($ownerWallet->is($debitWallet)) {
            $ownerWallet->add($this->transaction->total_amount);
        } else if ($ownerWallet->is($creditWallet)) {
            $ownerWallet->deduct($this->transaction->total_amount);
        }

        return $mutation;
    }
}
