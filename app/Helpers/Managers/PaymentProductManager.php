<?php

namespace App\Helpers\Managers;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\Config as AppConfig;
use App\Models\User;
use App\Models\Promo;
use App\Models\PaymentProduct;
use App\Models\PaymentProductType;
use App\Models\PaymentProductCategory;
use App\Models\Order;
use App\Models\Transaction;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Managers\TransactionManager;
use App\Helpers\Enums\PaymentProductType as Type;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\WalletType;
use App\Helpers\Enums\PaymentProductGroupType;
use App\Facades\HistoryLogger;
use WalletManager;
use Importer;

class PaymentProductManager
{
    protected $client;
    protected $user;
    protected $referenceUser;
    protected $paymentProduct;
    protected $order;

    public function __construct(PaymentProduct $paymentProduct = null)
    {
        $this->client = new \GuzzleHttp\Client();
        // $this->client = new \GuzzleHttp\Client(['verify' => '/etc/nginx/ssl/ca-cert.pem']);
        // $this->client = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false), ));
        $this->bimasaktiUrl = config('paymentproduct.fastpay.url');
        $this->paymentProduct = $paymentProduct;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setReferenceUser(User $user)
    {
        $this->referenceUser = $user;
        return $this;
    }

    public function setPaymentProduct(PaymentProduct $paymentProduct)
    {
        $this->paymentProduct = $paymentProduct;
        return $this;
    }

    public function setPaymentInquiry(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    public function xmlToArray($request)
    {
        $temps = array();

        $xmlNode = simplexml_load_string($request);
        $arrayData = $this->parseXml($xmlNode);
        // $temps = json_encode($arrayData);

        return $arrayData;
    }

    public function parseXml($xml, $options = array()) {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace

        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                if ($options['keySearch']) $attributeName =
                        str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                        . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                        . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                $childArray = $this->parseXml($childXml, $options);
                list($childTagName, $childProperties) = each($childArray);

                if ($options['keySearch']) $childTagName =
                        str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                if (!isset($tagsArray[$childTagName])) {
                    $tagsArray[$childTagName] =
                            in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
                ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        return array(
            $xml->getName() => $propertiesArray
        );
    }

    public static function generateXml($method)
    {
        $header = '<?xml version="1.0"?>';
        $header .= '<methodCall>';
        $header .= '<methodName>' . $method .'</methodName> ';
        $body = '';
        $footer = '</methodCall>';
        $xml = $header . $body . $footer;
        return $xml;
    }

    public static function addChild($xml, $datas = [])
    {
        $params = $xml->addChild('params');
        foreach ($datas as $key => $data) {
            $param = $params->addChild('param');
            $value = $param->addChild('value');
            $child = $value->addChild('string', $data);
        }
        return $xml;
    }

    public function convertData($orderType, $data)
    {
        \Log::info('######convertData');
        $data = $data['methodResponse']['params']['param']['value']['array']['data']['value'];
        switch ($orderType) {
            case OrderType::PHONE_PREPAID:
                $result = [
                    'product_code' => empty($data[0]['string']) ? "" : $data[0]['string'],
                    'time' => empty($data[1]['string']) ? "" : $data[1]['string'],
                    'phone_number' => empty($data[2]['string']) ? "" : $data[2]['string'],
                    'sn' => empty($data[5]['string']) ? "" : $data[5]['string'],
                    'ref1' => empty($data[6]['string']) ? "" : $data[6]['string'],
                    'ref2' => empty($data[7]['string']) ? "" : $data[7]['string'],
                    'status' => empty($data[8]['string']) ? "" : $data[8]['string'],
                    'description' => empty($data[9]['string']) ? "" : $data[9]['string']
                ];
                break;
            case OrderType::GAME:
                $raw = empty($data[5]['string']) ? "" : $data[5]['string'];
                $sn = substr($raw, strpos($raw, 'Password=')+9, strlen($raw) - strpos('Password=', $raw));
                $result = [
                    'product_code' => empty($data[0]['string']) ? "" : $data[0]['string'],
                    'time' => empty($data[1]['string']) ? "" : $data[1]['string'],
                    'phone_number' => null,
                    'sn' => $sn,
                    'ref1' => empty($data[6]['string']) ? "" : $data[6]['string'],
                    'ref2' => empty($data[7]['string']) ? "" : $data[7]['string'],
                    'status' => empty($data[8]['string']) ? "" : $data[8]['string'],
                    'description' => empty($data[9]['string']) ? "" : $data[9]['string']
                ];
                break;
            case OrderType::PLN_PREPAID:
                $status = empty($data[12]['string']) ? "" : $data[12]['string'];
                // $description = empty($data[13]['string']) ? "" : $data[13]['string'];

                // if ($status != 00) throw new Exception($description, 400);
                $data = [
                    'id2' => empty($data[3]['string']) ? "" : $data[3]['string'],
                    'amount' => empty($data[5]['string']) ? "" : $data[5]['string'],
                    'fee' => empty($data[6]['string']) ? "" : $data[6]['string'],
                    'ref1' => empty($data[9]['string']) ? "" : $data[9]['string'],
                    'ref2' => empty($data[10]['string']) ? "" : $data[10]['string'],
                    'status' => empty($data[12]['string']) ? "" : $data[12]['string'],
                    'description' => empty($data[13]['string']) ? "" : $data[13]['string'],
                    'meter_no' => empty($data[15]['string']) ? "" : $data[15]['string'],
                    'reference_no' => empty($data[19]['string']) ? "" : $data[19]['string'],
                    'customer_name' => empty($data[21]['string']) ? "" : $data[21]['string'],
                    'subscriber_segmentation' => empty($data[22]['string']) ? "" : $data[22]['string'],
                    'power_consuming_category' => empty($data[23]['string']) ? "" : $data[23]['string'],
                    'minor_unit_of_admin_charge' => empty($data[24]['string']) ? "" : $data[24]['string'],
                    'admin_charge' => empty($data[25]['string']) ? "" : $data[25]['string'],
                    'service_unit' => empty($data[28]['string']) ? "" : $data[28]['string'],
                    'service_unit_phone' => empty($data[29]['string']) ? "" : $data[29]['string'],
                    'token_pln' => empty($data[34]['string']) ? "" : $data[34]['string'],
                    'minor_unit_stamp_duty' => empty($data[35]['string']) ? "" : $data[35]['string'],
                    'stamp_duty' => empty($data[36]['string']) ? "" : $data[36]['string'],
                    'minor_unit_ppn' => empty($data[37]['string']) ? "" : $data[37]['string'],
                    'ppn' => empty($data[38]['string']) ? "" : $data[38]['string'],
                    'minor_unit_ppj' => empty($data[39]['string']) ? "" : $data[39]['string'],
                    'ppj' => empty($data[40]['string']) ? "" : $data[40]['string'],
                    'minor_unit_payables_installment' => empty($data[41]['string']) ? "" : $data[41]['string'],
                    'payables_installment' => empty($data[42]['string']) ? "" : $data[42]['string'],
                    'minor_unit_pp' => empty($data[43]['string']) ? "" : $data[43]['string'],
                    'pp' => empty($data[44]['string']) ? "" : $data[44]['string'],
                    'minor_unit_kwh' => empty($data[45]['string']) ? "" : $data[45]['string'],
                    'kwh' => empty($data[46]['string']) ? "" : $data[46]['string'],
                    'info_text' => empty($data[47]['string']) ? "" : $data[47]['string']
                ];

                $result = [
                    'customer_id' => $data['id2'],
                    'customer_name' => $data['customer_name'],
                    'meter_no' => $data['meter_no'],
                    'power_rate' => $data['subscriber_segmentation'].'/'.$this->convertToDouble($data['power_consuming_category']).' VA',
                    'ref_no_custom' => $data['ref1'], //no referensi custom
                    'ref2' => $data['ref2'], //utk request BMH
                    'ref_no' => $data['reference_no'], //ditampilkan di struk di pdf NOREF2
                    'status' => $status,
                    'description' => $data['description'],
                    'admin_charge' => $this->convertToDouble($data['admin_charge'], $data['minor_unit_of_admin_charge']),
                    'stamp_duty' => $this->convertToDouble($data['stamp_duty'], $data['minor_unit_stamp_duty']),
                    'ppn' => $this->convertToDouble($data['ppn'], $data['minor_unit_ppn']),
                    'ppj' => $this->convertToDouble($data['ppj'], $data['minor_unit_ppj']),
                    'installment' => $this->convertToDouble($data['payables_installment'], $data['minor_unit_payables_installment']),
                    'pp' => $this->convertToDouble($data['pp'], $data['minor_unit_pp']),
                    'kwh' => $this->convertToDouble($data['kwh'], $data['minor_unit_kwh']),
                    'total_amount' => $this->convertToDouble($data['amount']),
                    'sn' => !empty($data['token_pln']) ? $this->convertCode($data['token_pln'], 4) : null,
                    'info_text' => $data['info_text'],
                    'service_unit_phone' => $data['service_unit_phone']
                ];
                break;
            case OrderType::PLN_POSTPAID:
                $status = empty($data[12]['string']) ? "" : $data[12]['string'];
                // $description = empty($data[13]['string']) ? "" : $data[13]['string'];

                // if ($status != 00) throw new Exception($description, 400);

                $data = [
                    // 'product_code' => empty($data[0]['string']) ? "" : $data[0]['string'],
                    // 'time' => empty($data[1]['string']) ? "" : $data[1]['string'],
                    'id1' => $this->getNestedArrayData($data, 2),
                    // 'id2' => empty($data[3]['string']) ? "" : $data[3]['string'],
                    // 'id3' => empty($data[4]['string']) ? "" : $data[4]['string'],
                    'amount' => empty($data[5]['string']) ? "" : $data[5]['string'],
                    'fee' => empty($data[6]['string']) ? "" : $data[6]['string'],
                    'ref1' => empty($data[9]['string']) ? "" : $data[9]['string'],
                    'ref2' => empty($data[10]['string']) ? "" : $data[10]['string'],
                    'ref3' => empty($data[11]['string']) ? "" : $data[11]['string'],
                    'status' => empty($data[12]['string']) ? "" : $data[12]['string'],
                    'description' => empty($data[13]['string']) ? "" : $data[13]['string'],
                    'bill_status' => empty($data[16]['string']) ? "" : $data[16]['string'],
                    'payment_status' => empty($data[17]['string']) ? "" : $data[17]['string'],
                    'total_os_bill' => empty($data[18]['string']) ? "" : $data[18]['string'],
                    'sw_reference_no' => empty($data[19]['string']) ? "" : $data[19]['string'],
                    'subcriber_name' => empty($data[20]['string']) ? "" : $data[20]['string'],
                    'service_unit' => empty($data[21]['string']) ? "" : $data[21]['string'],
                    'service_unit_phone' => empty($data[22]['string']) ? "" : $data[22]['string'],
                    'subscriber_segmentation' => empty($data[23]['string']) ? "" : $data[23]['string'],
                    'power_consuming_category' => empty($data[24]['string']) ? "" : $data[24]['string'],
                    'total_admin_charge' => empty($data[25]['string']) ? "" : $data[25]['string'],

                    'bl_th1' => empty($data[26]['string']) ? "" : $data[26]['string'],
                    'penalty_fee1' => empty($data[32]['string']) ? "" : $data[32]['string'],
                    'slalwbp1' => empty($data[33]['string']) ? "" : $data[33]['string'],
                    'sahlwbp1' => empty($data[34]['string']) ? "" : $data[34]['string'],

                    'bl_th2' => empty($data[39]['string']) ? "" : $data[39]['string'],
                    'penalty_fee2' => empty($data[45]['string']) ? "" : $data[45]['string'],
                    'sahlwbp2' => empty($data[47]['string']) ? "" : $data[47]['string'],

                    'bl_th3' => empty($data[52]['string']) ? "" : $data[52]['string'],
                    'penalty_fee3' => empty($data[58]['string']) ? "" : $data[58]['string'],
                    'sahlwbp3' => empty($data[60]['string']) ? "" : $data[60]['string'],

                    'bl_th4' => empty($data[65]['string']) ? "" : $data[65]['string'],
                    'penalty_fee4' => empty($data[71]['string']) ? "" : $data[71]['string'],
                    'sahlwbp4' => empty($data[73]['string']) ? "" : $data[73]['string'],

                    'total_rp_tag' => empty($data[81]['string']) ? "" : $data[81]['string'],
                    'info_text' => empty($data[82]['string']) ? "" : $data[82]['string']
                ];
                $billCount =  $this->convertToDouble($data['bill_status']);
                $bl_ths = [$data['bl_th1'], $data['bl_th2'], $data['bl_th3'], $data['bl_th4']];
                $bl_th = "";
                for ($i = 0; $i < $billCount; $i++) {
                    if ($i > 0 && $i < 4){
                        $bl_th .= ", ";
                    }
                    $bl_th .= $this->getBlth($this->getRawItem($bl_ths, $i));
                }
                $amount = $this->convertToDouble($data['amount']);
                $admin = $this->convertToDouble($data['fee']);
                $stand_meter = $data['slalwbp1'];
                if ($billCount == 1) {
                    $stand_meter .= "-" . $data['sahlwbp1'];
                } else if ($billCount == 2) {
                    $stand_meter .= "-" . $data['sahlwbp2'];
                } else if ($billCount == 3) {
                    $stand_meter .= "-" . $data['sahlwbp3'];
                } else if ($billCount == 4) {
                    $stand_meter .= "-" . $data['sahlwbp4'];
                } else if ($billCount >= 5) {
                    $stand_meter .= "-" . $data['sahlwbp4'];
                }

                // $penalty = $this->convertToDouble($data['penalty_fee1']) + $this->convertToDouble($data['penalty_fee2']) + $this->convertToDouble($data['penalty_fee3']) + $this->convertToDouble($data['penalty_fee4']);
                $penalty = 0;
                $amount_before_penalty = $amount;
                $total_amount = $amount + (intval($amount) > 0 ? $admin : 0);

                if (intval($data['total_os_bill']) > 4) {
                    $os= ($data['total_os_bill'])-4;
                    $wording="Anda masih memiliki sisa tunggakan " . $os . " bulan";
                }else{
                    $wording="Terima Kasih";
                }

                $result = [
                    'customer_id' => $data['id1'],
                    'ref_no_custom' => $data['ref1'], //ref no custom
                    'ref2' => $data['ref2'], //utk request ke BMH
                    'stand_meter' => $stand_meter,
                    'customer_name' => $data['subcriber_name'],
                    'power_rate' => $data['subscriber_segmentation'].'/'.$this->convertToDouble($data['power_consuming_category']).' VA',
                    'bill_count' => $billCount,
                    'ref_no' => $data['sw_reference_no'], //ditampilkan di struk
                    'description' => $data['description'],
                    'status' => $status,
                    'period' => $bl_th,
                    'amount' => $amount_before_penalty,
                    'penalty' => $penalty,
                    'admin_charge' => $admin,
                    'total_amount' => $total_amount,
                    'sn' => '',
                    'info_text1' => $wording,
                    'info_text2' => $data['info_text'],
                    'info_text3' => "Informasi Hubungi Call Center : " . $data['service_unit_phone'],
                    'info_text4' => "Atau Hub. PLN Terdekat : " . $data['service_unit']
                ];
                break;
            case OrderType::PDAM:
                $data = [
                    'product_code' => $this->getNestedArrayData($data, 0),
                    'time' => $this->getNestedArrayData($data, 1),
                    'id1' => $this->getNestedArrayData($data, 2),
                    'id2' => $this->getNestedArrayData($data, 3),
                    'id3' => $this->getNestedArrayData($data, 4),
                    'amount' => $this->getNestedArrayData($data, 5),
                    'fee' => $this->getNestedArrayData($data, 6),
                    'id' => $this->getNestedArrayData($data, 7),
                    'pin' => $this->getNestedArrayData($data, 8),
                    'ref1' => $this->getNestedArrayData($data, 9),
                    'ref2' => $this->getNestedArrayData($data, 10),
                    'ref3' => $this->getNestedArrayData($data, 11),
                    'status' => $this->getNestedArrayData($data, 12),
                    'description' => $this->getNestedArrayData($data, 13),
                    'switcher_id' => $this->getNestedArrayData($data, 14),
                    'biller_code' => $this->getNestedArrayData($data, 15),
                    'customer_id1' => $this->getNestedArrayData($data, 16),
                    'customer_id2' => $this->getNestedArrayData($data, 17),
                    'customer_id3' => $this->getNestedArrayData($data, 18),
                    'bill_qty' => $this->getNestedArrayData($data, 19),
                    'no_ref1' => $this->getNestedArrayData($data, 20),
                    'no_ref2' => $this->getNestedArrayData($data, 21),
                    'customer_name' => $this->getNestedArrayData($data, 22),
                    'customer_address' => $this->getNestedArrayData($data, 23),
                    'customer_detail_information' => $this->getNestedArrayData($data, 24),
                    'biller_admin_charge' => $this->getNestedArrayData($data, 25),
                    'total_bill_amount' => $this->getNestedArrayData($data, 26),
                    'pdam_name' => $this->getNestedArrayData($data, 27),

                    'month_period1' => $this->getNestedArrayData($data, 28),
                    'year_period1' => $this->getNestedArrayData($data, 29),
                    'first_meter_read1' => $this->getNestedArrayData($data, 30),
                    'last_meter_read1' => $this->getNestedArrayData($data, 31),
                    'penalty1' => $this->getNestedArrayData($data, 32),
                    'bill_amount1' => $this->getNestedArrayData($data, 33),
                    'misc_amount1' => $this->getNestedArrayData($data, 34),

                    'month_period2' => $this->getNestedArrayData($data, 35),
                    'year_period2' => $this->getNestedArrayData($data, 36),
                    'first_meter_read2' => $this->getNestedArrayData($data, 37),
                    'last_meter_read2' => $this->getNestedArrayData($data, 38),
                    'penalty2' => $this->getNestedArrayData($data, 39),
                    'bill_amount2' => $this->getNestedArrayData($data, 40),
                    'misc_amount2' => $this->getNestedArrayData($data, 41),

                    'month_period3' => $this->getNestedArrayData($data, 42),
                    'year_period3' => $this->getNestedArrayData($data, 43),
                    'first_meter_read3' => $this->getNestedArrayData($data, 44),
                    'last_meter_read3' => $this->getNestedArrayData($data, 45),
                    'penalty3' => $this->getNestedArrayData($data, 46),
                    'bill_amount3' => $this->getNestedArrayData($data, 47),
                    'misc_amount3' => $this->getNestedArrayData($data, 48),

                    'month_period4' => $this->getNestedArrayData($data, 49),
                    'year_period4' => $this->getNestedArrayData($data, 50),
                    'first_meter_read4' => $this->getNestedArrayData($data, 51),
                    'last_meter_read4' => $this->getNestedArrayData($data, 52),
                    'penalty4' => $this->getNestedArrayData($data, 53),
                    'bill_amount4' => $this->getNestedArrayData($data, 54),
                    'misc_amount4' => $this->getNestedArrayData($data, 55),

                    'month_period5' => $this->getNestedArrayData($data, 56),
                    'year_period5' => $this->getNestedArrayData($data, 57),
                    'first_meter_read5' => $this->getNestedArrayData($data, 58),
                    'last_meter_read5' => $this->getNestedArrayData($data, 59),
                    'penalty5' => $this->getNestedArrayData($data, 60),
                    'bill_amount5' => $this->getNestedArrayData($data, 61),
                    'misc_amount5' => $this->getNestedArrayData($data, 62),

                    'month_period6' => $this->getNestedArrayData($data, 63),
                    'year_period6' => $this->getNestedArrayData($data, 64),
                    'first_meter_read6' => $this->getNestedArrayData($data, 65),
                    'last_meter_read6' => $this->getNestedArrayData($data, 66),
                    'penalty6' => $this->getNestedArrayData($data, 67),
                    'bill_amount6' => $this->getNestedArrayData($data, 68),
                    'misc_amount6' => $this->getNestedArrayData($data, 69)
                ];
                if (!empty($data['customer_id1']) && $data['customer_id1'] == "0") {
                    $customer_id = $data['customer_id1'];
                } else {
                    $customer_id = $data['customer_id2'];
                }
                $months = [];
                $bills = [];
                $total = 0;
                $penalty = 0;
                $billCount =  $this->convertToDouble($data['bill_qty']);
                for ($i=0; $i < $billCount; $i++) {
                    $monthIndex = 'month_period' . ($i+1);
                    $yearIndex = 'year_period' . ($i+1);
                    $billIndex = 'bill_amount' . ($i+1);
                    $penaltyIndex = 'penalty' . ($i+1);

                    $months = array_merge($months, [$this->getMonthStr($data[$monthIndex]) ." ". $data[$yearIndex]]);
                    $bills = array_merge($bills, [$data[$billIndex]]);
                    $penalties = array_merge($bills, [$data[$penaltyIndex]]);
                    $total += $data[$billIndex];
                    $penalty += $this->convertToDouble($data[$penaltyIndex]); // kalau 0, tidak ditampilkan dlm struk
                }
                $miscAmount = $this->convertToDouble($data['misc_amount1']) + $this->convertToDouble($data['misc_amount3']); // kalau 0, tidak ditampilkan dlm struk
                $stampDuty = $this->convertToDouble($data['month_period4']); // kalau 0, tidak ditampilkan dlm struk
                $result = [
                    'time' => substr($data['time'], 0, 4)."-".substr($data['time'], 4, 2)."-".substr($data['time'], 6, 2). " " . substr($data['time'], 8, 2).":".substr($data['time'], 10, 2).":".substr($data['time'], 12, 2), //mengikuti format bms
                    'ref2' => $data['ref2'], // no_resi yg ditampilkan di struk
                    'pdam_name' => $data['pdam_name'], // nama PDAM
                    'customer_id' => $customer_id, // no id pelanggan ataupun no sambungan air
                    'customer_name' => $data['customer_name'],
                    'customer_address' => empty($data['customer_address']) ? $data['customer_address'] : "-",
                    'months' => $months,
                    'bills' => $bills,
                    'amount' => (float) $total,
                    'penalty' => (float) $penalty,
                    'fee' => (float) $data['fee'],
                    'stamp_duty' => (float) $stampDuty,
                    'total_amount' => (float) $total + $penalty + $data['fee'] + $stampDuty,
                    'status' => $data['status'],
                    'description' => $data['description'],
                    'sn' => ''
                ];
                break;
            case OrderType::PHONE_POSTPAID:
                $data = [
                    'product_code' => $this->getNestedArrayData($data, 0),
                    'time' => $this->getNestedArrayData($data, 1),
                    'id1' => $this->getNestedArrayData($data, 2),
                    'id2' => $this->getNestedArrayData($data, 3),
                    'id3' => $this->getNestedArrayData($data, 4),
                    'amount' => $this->getNestedArrayData($data, 5),
                    'fee' => $this->getNestedArrayData($data, 6),
                    'id' => $this->getNestedArrayData($data, 7),
                    'pin' => $this->getNestedArrayData($data, 8),
                    'ref1' => $this->getNestedArrayData($data, 9),
                    'ref2' => $this->getNestedArrayData($data, 10),
                    'ref3' => $this->getNestedArrayData($data, 11),
                    'status' => $this->getNestedArrayData($data, 12),
                    'description' => $this->getNestedArrayData($data, 13),
                    'switcher_id' => $this->getNestedArrayData($data, 14),
                    'biller_code' => $this->getNestedArrayData($data, 15),
                    'customer_id' => $this->getNestedArrayData($data, 16),
                    'bill_qty' => $this->getNestedArrayData($data, 17),
                    'no_ref1' => $this->getNestedArrayData($data, 18),
                    'no_ref2' => $this->getNestedArrayData($data, 19),
                    'customer_name' => $this->getNestedArrayData($data, 20),
                    'customer_address' => $this->getNestedArrayData($data, 21),
                    'biller_admin_charge' => $this->getNestedArrayData($data, 22),
                    'total_bill_amount' => $this->getNestedArrayData($data, 23),
                    'provider_name' => $this->getNestedArrayData($data, 24),

                    'month_period1' => $this->getNestedArrayData($data, 25),
                    'year_period1' => $this->getNestedArrayData($data, 26),
                    'penalty1' => $this->getNestedArrayData($data, 27),
                    'bill_amount1' => $this->getNestedArrayData($data, 28),
                    'misc_amount1' => $this->getNestedArrayData($data, 29),

                    'month_period2' => $this->getNestedArrayData($data, 30),
                    'year_period2' => $this->getNestedArrayData($data, 31),
                    'penalty2' => $this->getNestedArrayData($data, 32),
                    'bill_amount2' => $this->getNestedArrayData($data, 33),
                    'misc_amount2' => $this->getNestedArrayData($data, 34),

                    'month_period3' => $this->getNestedArrayData($data, 35),
                    'year_period3' => $this->getNestedArrayData($data, 36),
                    'penalty3' => $this->getNestedArrayData($data, 37),
                    'bill_amount3' => $this->getNestedArrayData($data, 38),
                    'misc_amount3' => $this->getNestedArrayData($data, 39)
                ];

                $result = [
                    'time' => substr($data['time'], 0, 4)."-".substr($data['time'], 4, 2)."-".substr($data['time'], 6, 2). " " . substr($data['time'], 8, 2).":".substr($data['time'], 10, 2).":".substr($data['time'], 12, 2), //mengikuti format bms
                    'ref2' => $data['ref2'], // no_resi yg ditampilkan di struk dr BMH
                    'no_ref' => $data['no_ref1'], // no_resi yg ditampilkan di struk (jika ada)
                    'provider_name' => $data['provider_name'], // nama Provider
                    'customer_id' => $data['customer_id'], // no hp
                    'phone_number' => $data['customer_id'], // no hp
                    'customer_name' => $data['customer_name'],
                    'customer_address' => empty($data['customer_address']) ? $data['customer_address'] : "-",
                    'amount' => (float) $data['amount'],
                    'fee' => (float) $data['fee'],
                    'total_amount' => (float) ($this->convertToDouble($data['amount']) + $this->convertToDouble($data['fee'])),
                    'status' => $data['status'],
                    'description' => $data['description'],
                    'sn' => ''
                ];
                break;
            default:
                break;
        }
        return $result;
    }

    public function getNestedArrayData($data, $key = null)
    {
        if ($key == null && $key != 0) {
            foreach ($data as $key => $value) {
                $data[$key] = empty($data[$key]['string']) ? "" : $data[$key]['string'];
            }
            return $data;
        } else {
            return empty($data[$key]['string']) ? "" : $data[$key]['string'];
        }
    }

    public function getRawItem($object, $key)
    {
        if(array_key_exists($key, $object))
            return $object[$key];
        return "";
    }

    public function getBlth($blth)
    {
        $ret= $this->getMonthStr((int) substr  ($blth,4,2)) . " " . substr($blth,2,2);
        return $ret;
    }

    public function getMonthStr($bln)
    {
        if ($bln < 1) $bln = 1;
        $bulan = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Ags","Sep","Okt","Nov","Des");
        return $bulan[($bln-1)];
    }

    public function convertToDouble($amount, $minorAmount = 0)
    {
        $major = !empty($amount) ? $amount : 0;
        $minor = !empty($minorAmount) ? $minorAmount : 0;
        $result = sprintf("%".($minor+1)."0s", $major);
        $result = (float) (substr($result, 0, (strlen($result)-$minor)).".".substr($result,(strlen($result)-$minor)));
        return $result;
    }

    public function convertCode($data, $length)
    {
        $result = "";
        $resultLenght = ceil(strlen($data) / $length) - 1 + strlen($data);
        while (strlen($result) <= $resultLenght) {
            $connector = (strlen($result) + $length + 1 >= $resultLenght) ? " " : "-";
            $result .= substr($data, strlen($result)/($length+1) * $length, $length) . $connector;
        }
        return trim($result);
    }
    // API =========================================================================================

    public function getPaymentProductCategories($id = null, $limit, $offset, $keyword)
    {
        $type = PaymentProductType::find($id);
        switch ($type->product_code) {
            case Type::PLN:
                $result = [];
                break;
            case Type::PHONE:
                $result = $this->getPhoneProduct($id);
                break;
            case Type::POSTPAID_PHONE:
                $result = $this->getPhoneProduct($id);
                break;
            case Type::GAME:
                $result = $this->getGameProduct($id);
                break;
            case Type::PDAM:
                $result = $this->getPdamProduct($id, $keyword);
                break;
            default:
                $result = [];
                break;
        }
        return $result;
    }

    public function getPaymentProduct($id = null)
    {
        $result = PaymentProduct::where('category_id', $id)->get();
        return ['paymentProducts' => $result];
    }

    protected function getGameProduct($id)
    {
        $category_id = PaymentProduct::where('type_id', $id)->pluck('category_id');
        $categories = PaymentProductCategory::whereIn('id', $category_id)->select('id', 'data', 'category', 'picture_id')->get();
        return ['categories' => $categories];
    }

    protected function getPhoneProduct($id)
    {
        $paymentProduct = PaymentProduct::where('type_id', $id);
        $category_id = $paymentProduct->pluck('category_id');
        $paymentProducts = $paymentProduct->get();

        $categories = PaymentProductCategory::whereIn('id', $category_id)->select('id', 'category', 'data', 'picture_id')->get();

        return ['paymentProducts' => $paymentProducts, 'categories' => $categories];
    }

    protected function getPdamProduct($id, $keyword)
    {
        $paymentProduct = PaymentProduct::where('type_id', $id);

        $category_id = $paymentProduct->pluck('category_id');
        $categories = PaymentProductCategory::whereIn('id', $category_id);
        if (isset($keyword)) {
            $categories->where('category', 'like', '%'.$keyword.'%');
        }
        $categories = $categories->get();
        foreach ($categories as $key => $category) {
            $category->payment_products = $paymentProduct->where('category_id', $category->id)->get();
        }
        return ['categories' => $categories];
    }

    public function getPlnProduct()
    {
        $id = AppConfig::where('key', 'pp_type_pln_id')->value('value');
        $paymentProduct = PaymentProduct::where('type_id', $id)->where('amount', '>', 0);
        $paymentProducts = $paymentProduct->get();

        return $paymentProducts;
    }

    public function inquiry($orderType = null, $request = [])
    {
        if (!$this->user) throw new Exception('Failed to create inquiry, no user binded!', 500);
        if (!$this->paymentProduct) throw new Exception('Failed to create inquiry, no payment product binded!', 500);
        if (!isset($orderType)) throw new Exception('Payment type not found!', 400);

        $paymentProductCode = $this->paymentProduct->data['code'];
        if ($orderType == OrderType::PLN_PREPAID) {
            if (strlen($request['customer_id']) == 11) {
                $customer_id1 = $request['customer_id']; //no meteran
                $customer_id2 = "";
                $customer_id3 = "";
            } else if (strlen($request['customer_id']) == 12) {
                $customer_id1 = "";
                $customer_id2 = $request['customer_id']; //no id pelanggan
                $customer_id3 = "";
            } else throw new Exception("Your ID is not valid", 400);
        } else if ($orderType == OrderType::PLN_POSTPAID && strlen($request['customer_id']) == 12) {
            $customer_id1 = $request['customer_id']; //no id pelanggan
            $customer_id2 = "";
            $customer_id3 = "";
        } else if ($orderType == OrderType::PHONE_POSTPAID) {
            $customer_id1 = "";
            $customer_id2 = $request['phone_number']; //no telp
            $customer_id3 = "";
        } else if ($orderType == OrderType::PDAM) {
            $customer_id1 = $request['customer_id']; //no pelanggan
            $customer_id2 = ""; //no sambungan
            $customer_id3 = "";
        } else if ($orderType == OrderType::BPJS) {
            $customer_id1 = $request['area_code'];
        // } else if ($orderType == OrderType::PHONE_POSTPAID) {
        //     $customer_id1 = $request['area_code']; //kode area
        //     $customer_id2 = $request['phone_number']; //no telp
        //     $customer_id3 = "";
        } else throw new Exception("Your ". ($orderType == OrderType::PHONE_POSTPAID ? "phone number" : "ID") ." is not valid", 400);

        $data = [
            'customer_id1' => $customer_id1,
            'customer_id2' => $customer_id2,
            'customer_id3' => $customer_id3,
            'method' => 'fastpay.inq',
            'payment_product_code' => $paymentProductCode,
            'amount' => 0
        ];
        switch ($orderType) {
            case OrderType::PLN_PREPAID:
                $res = $this->inquiryPln($orderType, $data);
                break;
            case OrderType::PLN_POSTPAID:
                $res = $this->inquiryPln($orderType, $data);
                break;
            case OrderType::PHONE_POSTPAID:
                $res = $this->inquiryPln($orderType, $data);
                break;
            case OrderType::PDAM:
                $res = $this->inquiryPln($orderType, $data);
                break;
            default:
                throw new Exception('Payment type not found!', 400);
                $res = false;
                break;
        }
        return $res;
    }

    public function payment($orderType = null, $data = [])
    {
        if (!$this->user) throw new Exception('Failed to create payment, no user binded!', 500);
        if (!$this->paymentProduct) throw new Exception('Failed to create payment, no payment product binded!', 500);
        if (!isset($orderType)) throw new Exception('Payment type not found!', 400);
        // if ($this->user->id != 17) throw new Exception("Temporaly Disabled", 400);
        // if ($orderType == OrderType::PLN_PREPAID && $this->user->id != 13) throw new Exception('Token PLN sedang dalam gangguan', 400);
        switch ($orderType) {
            case OrderType::PLN_PREPAID:
                $res = $this->paymentPln($orderType, $data);
                break;
            case OrderType::PLN_POSTPAID:
                $res = $this->paymentPln($orderType, $data);
                break;
            case OrderType::PHONE_PREPAID:
                $res = $this->paymentPhonePrepaid($orderType, $data);
                break;
            case OrderType::GAME:
                $res = $this->paymentPhonePrepaid($orderType, $data);
                break;
            case OrderType::PDAM:
                $res = $this->paymentPln($orderType, $data);
                break;
            case OrderType::PHONE_POSTPAID:
                $res = $this->paymentPln($orderType, $data);
                break;
            default:
                throw new Exception('Payment type not found!', 400);
                $res = false;
                break;
        }
        return $res;
    }

    public function inquiryPln($orderType, $data = [])
    {
        $amount = $data['amount'];
        $xml_string = $this->generateXml($data['method']);
        $xml = simplexml_load_string($xml_string);
        $data = [
            $data['payment_product_code'],
            $data['customer_id1'],
            $data['customer_id2'],
            $data['customer_id3'],
            config('paymentproduct.fastpay.id'),
            config('paymentproduct.fastpay.pin'),
            'Ref1'
        ];
        $xml = $this->addChild($xml, $data);
        $response = $this->client->request('POST', $this->bimasaktiUrl, [
            'headers' => ['accept', 'application/xml'],
            'http_errors' => false,
            'body' => $xml->asXml()
        ]);

        $content = $response->getBody()->getContents();

        $dataResponse = $this->xmlToArray($content);
        $dataResponse = $this->convertData($orderType, $this->xmlToArray($content));
        if (in_array($response->getStatusCode(), [200, 201])) {
            if ($dataResponse['status'] == '00' || $dataResponse['status'] == '0') {
                $order = $this->createInquiry($this->paymentProduct, $dataResponse, $orderType);
            } else {
                $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.inq')) ? $dataResponse['status'] : '911';
                throw new Exception(__('ppob/messages.inq.'.$msgArray), 400);
            }
        } else {
            $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.inq')) ? $dataResponse['status'] : '911';
            throw new Exception(__('ppob/messages.inq.'.$msgArray), 400);
            // if ($content->message) {
            //     throw new Exception($content->message, 500);
            // }
            // throw new Exception('Failed to create inquiry.', 500);
        }
        if ($orderType == OrderType::PLN_PREPAID) {
            $paymentProducts = $this->getPlnProduct();
            $result = ['order' => $order, 'paymentProducts' => $paymentProducts];
        } else {
            $result = ['order' => $order];
        }
        return $result;
    }

    public function paymentPln($orderType, $data = [])
    {
        // check user balance
        \Log::info('paymentPln()');

        $walletType = $data['wallet_type'];
        $wallet = $this->user->wallets()->where('type', $walletType)->first();
        if (!$this->order) throw new Exception('Failed to create payment, no payment inquiry binded!', 500);
        if ($this->order->status == OrderStatus::COMPLETED) throw new Exception('Payment inquiry has completed!', 500);
        $price = $data['price'];
        \Log::info($price);
        if ($wallet->balance < $data['price']) throw new Exception('Balance not enough!', 400);
        $xml_string = $this->generateXml($data['method']);
        $xml = simplexml_load_string($xml_string);
        if ($orderType == OrderType::PLN_POSTPAID) {
            $dataPrice = $price-3000;
        } else {
            $dataPrice = $price;
        }
        $data = [
            $data['payment_product_code'],
            $data['customer_id1'],
            $data['customer_id2'],
            $data['customer_id3'],
            (int)$dataPrice, //Tanpa biaya admin
            config('paymentproduct.fastpay.id'),
            config('paymentproduct.fastpay.pin'),
            'ref1',
            $data['ref2'],
            ''
        ];
        \Log::info($data);

        $xml = $this->addChild($xml, $data);

        $response = $this->client->request('POST', $this->bimasaktiUrl, [
            'headers' => ['accept', 'application/xml'],
            'http_errors' => false,
            'body' => $xml->asXml()
        ]);

        $content = $response->getBody()->getContents();
        $dataResponse = $this->xmlToArray($content);
        $dataResponse = $this->convertData($orderType, $this->xmlToArray($content));
        \Log::info($dataResponse['status']);
        if (in_array($response->getStatusCode(), [200, 201])) {
            if ($dataResponse['status'] == '00' || $dataResponse['status'] == '0') {
                $payment = $this->createPayment($this->paymentProduct, $orderType, $dataResponse, $walletType, $price);
            } else {
                $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.pay')) ? $dataResponse['status'] : '911';
                throw new Exception(__('ppob/messages.pay.'.$msgArray), 400);
            }
        } else {
            $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.pay')) ? $dataResponse['status'] : '911';
            throw new Exception(__('ppob/messages.pay.'.$msgArray), 400);
        }
        if ($payment) {
            $order = $this->completeInquiry($this->order, $price, $dataResponse);
        }
        return $payment;
    }

    public function paymentPhonePrepaid($orderType, $data = []) //game sama
    {
        $walletType = $data['wallet_type'];
        $wallet = $this->user->wallets()->where('type', $walletType)->first();

        $price = $data['price'];
        if ($wallet->balance < $price) throw new Exception('Balance not enough!', 400);

        $xml_string = $this->generateXml($data['method']);
        $xml = simplexml_load_string($xml_string);
        $data = [
            $data['payment_product_code'],
            $data['phone_number'] ?? null,
            config('paymentproduct.fastpay.id'),
            config('paymentproduct.fastpay.pin'),
            ''];
        \Log::info($data);
        $xml = $this->addChild($xml, $data);

        $response = $this->client->request('POST', $this->bimasaktiUrl, [
            'headers' => ['accept', 'application/xml'],
            'http_errors' => false,
            'body' => $xml->asXml()
        ]);
        $content = $response->getBody()->getContents();
        $dataResponse = $this->xmlToArray($content);
        $dataResponse = $this->convertData($orderType, $this->xmlToArray($content));
        \Log::info($dataResponse);
        $data = $this->xmlToArray($content);
        $data = $this->convertData($orderType, $this->xmlToArray($content));
        if (in_array($response->getStatusCode(), [200, 201])) {
            if ($dataResponse['status'] == '00' || $dataResponse['status'] == '0') {
                $payment = $this->createPayment($this->paymentProduct, $orderType, $dataResponse, $walletType, $price);
            } else {
                $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.phone')) ? $dataResponse['status'] : '911';
                throw new Exception(__('ppob/messages.phone.'.$msgArray), 400);
            }
        } else {
            $msgArray = array_key_exists($dataResponse['status'], __('ppob/messages.phone')) ? $dataResponse['status'] : '911';
            throw new Exception(__('ppob/messages.phone.'.$msgArray), 400);
        }
        if ($payment) {
            $order = $this->completeInquiry($this->order, $price, $dataResponse);
        }

        return $payment;
    }

    public function createInquiry(PaymentProduct $paymentProduct, $data, $orderType, $amount = 0)
    {
        $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
        $referable = Admin::find($id);
        $plnFee = AppConfig::where('key', 'pln_prepaid_admin')->value('value');
        if ($paymentProduct->amount != 0) {
            $price = $paymentProduct->price;
        } else {
            \Log::info('paymentProductManager->createInquiry()');
            if($paymentProduct->data['code'] == 'PLNPRAH') {
              $data['pln_prepaid_admin'] = (int)$plnFee;
            } else {
              $data['pln_prepaid_admin'] = 0;
            }
            \Log::info($data);
            \Log::info($paymentProduct);
            if (isset($data['total_amount'])) {
                $price = $data['total_amount'];
            } else {
                $price = $amount;
            }
        }
        $orderManager = new OrderManager($this->user);
        $orderData = [
            'options' => $data,
            'products' => [
                (object) [
                    'id' => $paymentProduct->id,
                    'type' => 'payment_products',
                    'qty' => 1,
                    'price' => $price,
                    'discount' => 0
                ]
            ]
        ];
        $order = $orderManager->create($referable, $orderType, $orderData);
        if (!$order->save()) {
            throw new Exception('Failed to create inquiry.', 500);
        }
        return $order;
    }

    public function createPayment(PaymentProduct $paymentProduct, $type = null, $data = [], $walletType, $price)
    {
        $transactionManager = new TransactionManager($this->user);
        if (!$this->order) {
            $this->order = $this->createInquiry($paymentProduct, $data, $type, $price);
            $order = $this->order;
        } else if ($this->order) {
            $order = $this->order;
        }
        if (in_array($type, OrderType::getPlnList())) {
            $transactionType = TransactionType::PLN;
        } else if (in_array($type, OrderType::getPulsaList())){
            $transactionType = TransactionType::PULSA;
        } else if (in_array($type, OrderType::getGameList())){
            $transactionType = TransactionType::GAME;
        } else if (in_array($type, [OrderType::PDAM])){
            $transactionType = TransactionType::PDAM;
        }
        $debitWallet = $order->referable ? $order->referable->getWallet(WalletType::PRIMARY) : null;
        $creditWallet = $order->ownable ? $order->ownable->getWallet($walletType) : null;
        $transactionManager->setOrder($order);
        $transaction = $transactionManager->create($debitWallet, $creditWallet, $price, $transactionType);
        return $transaction;
    }

    public function completeInquiry(Order $order, $price, $data)
    {
        $checkLimit = $this->user->orders()->whereMonth('created_at', date('n'))->where('status', OrderStatus::COMPLETED)->where('type', '=', OrderType::PHONE_PREPAID)->get()->sum('total');
        $plnFee = AppConfig::where('key', 'pln_prepaid_admin')->value('value');
        $adminFee = 0;
        \Log::info('completeInquiry()');
        $order->status = OrderStatus::COMPLETED;
        $order->options = $data;
        if (!$order->save()) {
            throw new Exception('Failed to complete inquiry.', 500);
        }

        if ($order->type == OrderType::PLN_PREPAID) {
            $total = (int)$price+(int)$plnFee;
        } else if ($order->type == OrderType::PHONE_PREPAID) {
            if ($this->user->packages == PackageType::FREE) {
                $adminFee = 1500;
            } elseif ($this->user->packages == PackageType::SILVER) {
                if ($checkLimit > 500000) $adminFee = 1300;
            } else if ($this->user->packages == PackageType::GOLD) {
                if ($checkLimit > 1000000) $adminFee = 1200;
            } else {
                if ($checkLimit > 1500000) $adminFee = 1000;
            }
            $total = (int)$price+(int)$adminFee;
        } else {
            $total = $price;
        }
        
       
        if ($order->total == 0) {
            foreach ($order->orderDetails as $key => $orderDetail) {
                $orderDetail->total = $total;
                $orderDetail->price = $price;
                if(!$orderDetail->save()){
                    throw new Exception("Failed to complete inquiry", 500);
                }
            }
        }
        return $order;
    }

    // Admin Site
    public function update(PaymentProduct $paymentProduct, $data)
    {
        $paymentProduct->name = isset($data['name']) ? $data['name'] : null;
        $paymentProduct->amount = $data['amount'];
        $paymentProduct->price = $data['price'];
        $paymentProduct->fee = $data['fee'];
        if (isset($data['data'])) {
            $dataArr = [];

            foreach ($data['data'] as $key => $item) {
                $dataArr[$item['name']] = $item['value'];
            }
            $paymentProduct->data = $dataArr;
        } else {
            $paymentProduct->data = null;
        }
        $paymentProduct->status = $data['status'];
        $paymentProduct->type()->associate($data['type_id']);
        $paymentProduct->category()->associate($data['category_id']);

        if (!$paymentProduct->save()) {
            throw new Exception('Failed to save Product.', 500);
        }
        return $paymentProduct;
    }

    public function importFromExcel($rawFile)
    {
        $path = $rawFile->getRealPath();
        $excel = Importer::make('Excel');
        $excel->load($path);
        $collection = $excel->getCollection();

        if(!empty($collection) && $collection->count()) {
            $array = $collection->toArray();
            foreach ($array as $i => $value) {
                if ($i !== 0 && ($value[0] !== '' && $value[5] !== '' && $value[6] !== '')) {
                    $paymentProduct = PaymentProduct::updateOrCreate([
                        $array[0][2] => $value[2],
                        $array[0][5] => $value[5],
                        $array[0][6] => $value[6],
                    ], [
                        $array[0][0] => $value[0],
                        $array[0][1] => ["code" => $value[1]],
                        $array[0][3] => $value[3],
                        $array[0][4] => $value[4],
                        $array[0][7] => $value[7],
                    ]);
                }
            }
        }
        return $paymentProduct;
    }
}
