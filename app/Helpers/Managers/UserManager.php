<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\User;
use App\Models\Admin;
use App\Models\Role;
use App\Models\SessionToken;
use App\Helpers\Enums\DocumentType;
use App\Helpers\Enums\VerificationType;
use App\Helpers\Enums\VerificationStatus;
use App\Helpers\Managers\DocumentManager;
use App\Helpers\Managers\BinaryManager;
use App\Helpers\Managers\OTPManager;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Support\Facades\Hash;

class UserManager
{
    protected $user;
    protected $guard;
    protected $countryCode;

    public function __construct($user = null, $guard = null)
    {
        $this->user = $user;
        $this->guard = $guard;
        $this->countryCode = 'ID';
    }

    public function getUser()
    {
        return $this->user;
    }

    public function create(User $user, Role $role, $data)
    {
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        
        $user->phone_number = PhoneNumber::make($data['phone_number'], $this->countryCode);
        if (isset($data['password'])) {
            $user->password = bcrypt($data['password']);
        }
        $user->email = isset($data['email']) ? $data['email'] : null;
        $user->role()->associate($role);
        
        // if (array_key_exists('referral_code', $data) && $data['referral_code'] != null) {
        //     $eventManager = new EventManager(); 
        //     $user->options = [
        //         'referral_user_id' => $eventManager->findReferral($data['referral_code']),
        //         'verify_referral' => VerificationStatus::PENDING
        //     ];
        // }

        if (!$user->save()) {
            throw new Exception('Failed create user!', 500);
        }

        // $createWallet = $this->initializeWallets($admin);

        return $admin;
    }

    public function delete(User $user)
    {
        $user->delete();
        return $user;
    }

    public function approveIdentity($data, Admin $admin)
    {
        $document = $this->verifyUserData();

        $documentManager = new DocumentManager($admin);
        $nricVerified = $documentManager->verify($document['nric'], $data);
        $selfieVerified = $documentManager->verify($document['selfie'], $data);

        $this->storeUpgradeData($data);
        $this->createBinary(isset($data['leader_id']) ? $data['leader_id'] : null);
        return $this->user;
    }

    public function declineIdentity(Admin $admin)
    {
        $document = $this->verifyUserData();

        $documentManager = new DocumentManager($admin);
        $nricVerified = $documentManager->decline($document['nric']);
        $selfieVerified = $documentManager->decline($document['selfie']);

        return $this->user;
    }

    public function verifyUserData()
    {
        $nric = $this->user->getDocument(DocumentType::NRIC);
        $selfie = $this->user->getDocument(DocumentType::SELFIE);
        if (!$nric) throw new Exception("User has not uploaded NRIC or not found!", 400);
        if (!$selfie) throw new Exception("User has not uploaded Selfie or not found!", 400);
        if ($this->user->verification_status['nric'] != VerificationStatus::PENDING) throw new Exception("User already reviewed!", 400);

        return ['nric' => $nric, 'selfie' => $selfie];
    }

    public function storeUpgradeData($data)
    {
        $user = $this->user;
        $user->first_name = $data['first_name'];
        $user->last_name = isset($data['last_name']) ? $data['last_name'] : '';
        $user->gender = $data['gender'];
        $user->dob = $data['dob'];
        $user->sponsor_id = $data['sponsor_id'];
        $user->leader_id = $data['leader_id'];
        
        if (!$user->save()) throw new Exception(__('error/messages.failed_update_user'), 500);
    }

    public function createBinary($leaderId = null)
    {
        $binaryManager = new BinaryManager($this->user);
        $updatedBinary = $binaryManager->create($leaderId);
    }

    public function checkPassword($password)
    {
        if (!$this->user) throw new Exception(_('error/
            messages.check_password_no_user'), 500);

        if (!Hash::check($password, $this->user->password)) {
            throw new Exception(_('error/messages.incorrect_password'), 400);
        }
        return $this->user;
    }

    public function checkPin($pin) {
        if (!$this->user) throw new Exception(__('error/messages.check_pin_no_user'), 500);

        if (!Hash::check($pin, $this->user->pin)) {
            throw new Exception(__('error/messages.incorrect_pin'), 400);
        }

        return $this->user;
    }

    protected function findUser($credential = null)
    {
        if (is_numeric($credential)) {
            $credential = (string) PhoneNumber::make($credential, $this->countryCode);
            $credentialType = 'phone_number';
            $verificationType = VerificationType::PHONE_NUMBER;
        } else if (filter_var($credential, FILTER_VALIDATE_EMAIL)) {
            $credentialType = 'email';
            $verificationType = VerificationType::EMAIL;
        } else {
            throw new Exception(__('error/messages.user_not_found'), 404);
        }

        if (is_null($credentialType)) throw new Exception(__('error/messages.user_not_found'), 404);

        $user = User::where($credentialType, $credential)->first();
        if (!$user) throw new Exception(__('error/messages.user_credential_not_found'), 404);
        $response = [
            'user' => $user,
            'credentialType' => $credentialType,
            'verificationType' => $verificationType
        ];
        return (object)$response;
    }

    public function findUserByPhoneNumber($phoneNumber, $forRegister = false)
    {
        if (is_numeric($phoneNumber)) {
            $credential = (string) PhoneNumber::make($phoneNumber, $this->countryCode);
            $credentialType = 'phone_number';
        }

        if (is_null($credentialType)) throw new Exception(__('error/messages.user_credential_not_found'), 404);

        $user = User::where($credentialType, $credential)->first();
        if ($forRegister) return $user;
        if (!$user) throw new Exception(__('error/messages.user_phone_not_found', ['object' => $credential]), 404);

        return $user;
    }

    public function login($credential, $pin)
    {
        $userData = $this->findUser($credential);
        $user = $userData->user;
        $credentialType = $userData->credentialType;
        $verificationType = $userData->verificationType;
        if ($credentialType == 'phone_number') {
            $credential = (string) PhoneNumber::make($credential, $this->countryCode);
        }

        $otpManager = new OTPManager($user);
        $otp = $otpManager->verifyOTP($credentialType, $credential, $pin);

        // Verify user according verification type
        $verificationManager = new VerificationManager($user);
        $verification = $verificationManager->verify($verificationType, $otp);

        // $typeString = VerificationType::getString($type);
        // if (!$verification) throw new Exception("User {$typeString}  already verified!", 500);

        $token = $this->guard->claims([$credentialType => $credential])->login($user);
        $this->user = $this->guard->user();
        // User logged in, delete otp record

        return $token;
    }

    public function forceLogin(User $user)
    {
        $credentialType = 'phone_number';
        $credential = (string) PhoneNumber::make($user->phone_number, $this->countryCode);
        $token = $this->guard->claims([$credentialType => $credential])->login($user);
        $this->user = $this->guard->user();
        // User logged in, delete otp record

        return $token;
    }

    public function loggedIn($data, $type)
    {
        if (!$this->user) throw new Exception(__('error/messages.create_session_no_user'), 500);
        if (!$type) throw new Exception(__('error/messages.failed_create_session') . '. Type required!', 500);
        $session = new SessionToken;
        $session->token = isset($data['token']) ? $data['token'] : '';
        $session->type = $type;
        $session->device_type = isset($data['device_type']) ? $data['device_type'] : '';
        $session->device_token = isset($data['device_token']) ? $data['device_token'] : '';
        $session->ip_address = isset($data['ip_address']) ? $data['ip_address'] : '';
        $session->expired_at = isset($data['expired_at']) ? $data['expired_at'] : null;
        $session->ownable()->associate($this->user);
        if (!$session->save()) {
            throw new Exception(__('error/messages.failed_create_session'), 500);
        }
        $this->logoutUser($this->user, $session);
        return $session;
    }

    public function logoutUser($ownable, SessionToken $currentSession = null, $except = [])
    {
        if ($ownable instanceof User) {
            // is user instance
        } else if ($ownable instanceof Admin) {
            // is admin instance
        } else {
            throw new Exception(__('error/messages.error_while_logout_user'), 500);
        }

        if ($currentSession->id) {
            $except[] = $currentSession->id;
        }
        

        $query = $ownable->sessionTokens()->active();

        if ($currentSession && $currentSession->type) {
            $query->where('type', $currentSession->type);
        }

        $sessions = $query->whereNotIn('id', $except)->get();
        $sessions->each(function($session) {
            $this->invalidateSession($session);
        });

        return $ownable;
    }

    protected function invalidateSession(SessionToken $session)
    {
        $sessionTokenManager = new SessionTokenManager;
        $invalidate = $sessionTokenManager->invalidate($session);

        return $invalidate;
    }

    public function setPin($pin) {
        if (!$this->user) throw new Exception(__('error/messages.set_pin_no_user'), 500);
        if ($this->user->pin != null) throw new Exception(__('error/messages.pin_already_set'), 500);

        $this->user->pin = bcrypt($pin);
        
        if (!$this->user->save()) {
            throw new Exception(__('error/messages.error_while_set_pin'), 500);
        }
        
        return $this->user;
    }
}
