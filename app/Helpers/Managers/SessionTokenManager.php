<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\Config as AppConfig;
use App\Models\SessionToken;
use JWTAuth;

class SessionTokenManager
{
    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($data, $type)
    {
        if (!$this->user) throw new Exception('Failed to create session, no user binded!', 500);
        if (!$type) throw new Exception('Failed to create session, type required!', 500);

        $session = new SessionToken;
        $session->token = isset($data['token']) ? $data['token'] : '';
        $session->type = $type;
        $session->device_type = isset($data['device_type']) ? $data['device_type'] : '';
        $session->device_token = isset($data['device_token']) ? $data['device_token'] : '';
        $session->ip_address = isset($data['ip_address']) ? $data['ip_address'] : '';
        $session->options = isset($data['options']) ? $data['options'] : '';
        $session->expired_at = isset($data['expired_at']) ? $data['expired_at'] : null;
        $session->ownable()->associate($this->user);

        if (!$session->save()) {
            throw new Exception('Failed create session!', 500);
        }
        return $session;
    }

    public function compareDeviceToken($token = null, $deviceToken)
    {
        if (!$token) return false;
        $sessionToken = SessionToken::where('token', $token)->first();
        if (!$sessionToken) return false;
        if ($sessionToken->device_token != $deviceToken) {
            $sessionToken->device_token = $deviceToken;

            if (!$sessionToken->save()) {
                throw new Exception('Failed save session token!', 500);
            }
        }

        return $sessionToken;
    }

    public function invalidate(SessionToken $session)
    {
        $invalidatedSession = $this->delete($session->token);
        return $session;
    }

    public function delete($token = null, $force = false)
    {
        $session = SessionToken::where('token', $token)->first();
        if ($session) {
            if (in_array($session->type, ['web'])) {
                try {
                    Session::getHandler()->destroy($session->token);
                } catch (Exception $e) {
                    // do something
                }
            } else if (in_array($session->type, ['api', 'admin-api'])) {
                // $session->expired_at = Carbon::now();
                // if (!$session->save()) {
                //     throw new Exception('Failed update session!', 500);
                // }
                try {
                    JWTAuth::setToken($session->token);
                    JWTAuth::invalidate(true, $force);
                } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    // do whatever you want to do if token is expired
                } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    // do whatever you want to do if token is invalid
                } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                    // do whatever you want to do if token is not present
                }
            }
            $session->delete();
        }
        return $session;   
    }
}
