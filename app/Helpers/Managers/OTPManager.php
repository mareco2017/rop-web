<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Propaganistas\LaravelPhone\PhoneNumber;
use App\Models\User;
use App\Models\OneTimePin;
use App\Models\Config as AppConfig;
use App\Helpers\Enums\VerificationStatus;
use App\Helpers\Enums\VerificationType;
use App\Mail\EmailVerification;
use App\Facades\SMS;

class OTPManager
{
    protected $user;
    protected $guard;
    protected $countryCode;

    public function __construct($user = null, $guard = null)
    {
        $this->user = $user;
        $this->guard = $guard;
        $this->countryCode = 'ID';
    }

    public function getUser()
    {
        return $this->user;
    }

    public function sendOTP($credential, $usage = null)
    {
        $now = Carbon::now();
        $expiredMinutes = 20;
        $pinLength = 4;

        $pin = rand(pow(10, $pinLength -1), pow(10, $pinLength) -1);
        $otp = new OneTimePin;
        if (is_numeric($credential)) {
            $credential = (string) PhoneNumber::make($credential, $this->countryCode);
            OneTimePin::where('phone_number', $credential)->where('usage', $usage)->forceDelete();
            $otp->phone_number = $credential;
        } else if (filter_var($credential, FILTER_VALIDATE_EMAIL)) {
            OneTimePin::where('email', $credential)->where('usage', $usage)->forceDelete();
            $otp->email = $credential;
        } else {
            throw new Exception(__('error/messages.went_wrong'), 404);
        }

        $otp->pin = $pin;
        $otp->usage = $usage;
        $otp->expired_at = $now->addMinutes($expiredMinutes);
        if (!$otp->save()) {
            throw new Exception(__('error/messages.went_wrong'), 500);
        }

        if ($otp->phone_number) {
            $message = "Rich On Pay - {$otp->pin} adalah kode verifikasi anda. Kode berlaku hanya {$expiredMinutes} menit. Mohon tidak menyebarkan kode tersebut kepada siapa pun.";
            $sms = SMS::send($otp->phone_number, $message);
        } else if ($otp->email) {
            Mail::to($otp->email)->send(new EmailVerification($this->user, $otp));
        }

        return $otp;
    }

    public function forceSendOTP(User $user, $credential, $credentialType, $verificationType, $usage = null)
    {
        $now = Carbon::now();
        $expiredMinutes = 5;
        $pinLength = 4;

        $pin = rand(pow(10, $pinLength -1), pow(10, $pinLength) -1);
        $otp = new OneTimePin;
        if ($verificationType == VerificationType::PHONE_NUMBER) {
            $otp->phone_number = $credential;
            OneTimePin::where('phone_number', $credential)->where('usage', $usage)->forceDelete();
        } else if ($verificationType == VerificationType::EMAIL) {
            $otp->email = $credential;
            OneTimePin::where('email', $credential)->where('usage', $usage)->forceDelete();
        }
        $otp->pin = $pin;
        $otp->usage = $usage;
        $otp->expired_at = $now->addMinutes($expiredMinutes);
        if (!$otp->save()) {
            throw new Exception(__('error/messages.went_wrong'), 500);
        }

        if ($verificationType == VerificationType::PHONE_NUMBER) {
            $message = "Rich on Pay - {$otp->pin} adalah kode verifikasi anda. Kode berlaku hanya {$expiredMinutes} menit. Mohon tidak menyebarkan kode tersebut kepada siapa pun.";
            $sms = SMS::send($otp->phone_number, $message);
        } else if ($verificationType == VerificationType::EMAIL) {
            Mail::to($user)->send(new EmailVerification($user, $otp));
        }

        return $otp;
    }

    public function verifyOTP($credentialType, $credential, $pin, $usage = null)
    {
        $query = OneTimePin::where($credentialType, $credential)->where('pin', $pin)->where('expired_at',
            '>=', Carbon::now());
        if ($usage) {
            $query->where('usage', 'LIKE', '%'.$usage.'%');
        }
        $otp = $query->first();
        if (!$otp) throw new Exception(__('error/messages.otp_error'), 400);

        return $otp;
    }

    protected function findUser($credential = null)
    {
        if (is_numeric($credential)) {
            $credential = (string) PhoneNumber::make($credential, $this->countryCode);
            $credentialType = 'phone_number';
            $verificationType = VerificationType::PHONE_NUMBER;
        } else if (filter_var($credential, FILTER_VALIDATE_EMAIL)) {
            $credentialType = 'email';
            $verificationType = VerificationType::EMAIL;
        } else {
            throw new Exception(__('error/messages.user_not_found'), 404);
        }

        if (is_null($credentialType)) throw new Exception(__('error/messages.user_not_found'), 404);

        // $user = User::active()->where($credentialType, $credential)->first();
        $user = User::where($credentialType, $credential)->first();
        if (!$user) throw new Exception(__('error/messages.user_credential_not_found'), 404);
        // if ($user->status == ActiveStatus::INACTIVE) throw new Exception(__('error/messages.user_not_active'), 400);
        $response = [
            'user' => $user,
            'credentialType' => $credentialType,
            'verificationType' => $verificationType
        ];
        return (object)$response;
    }
}
