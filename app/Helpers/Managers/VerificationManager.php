<?php

namespace App\Helpers\Managers;

use Exception;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\Verification;
use App\Models\OneTimePin;
use App\Models\Document;
use App\Helpers\Enums\VerificationType;
use App\Helpers\Enums\VerificationStatus;
use App\Facades\HistoryLogger;

class VerificationManager
{
    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function create($verifiable, $type, $reference = null)
    {   
        if (!$this->user) throw new Exception("Failed to create ". VerificationType::getString($type) ." verification, no user binded!", 500);
        
        $verification = $verifiable->verifications()->firstOrNew(['type' => $type]);
        $verification->ownable()->associate($this->user);
        $verification->status = VerificationStatus::PENDING;
        $verification->reference = $reference;

        if (!$verification->save()) {
            throw new Exception('Failed to create verification!', 500);
        }

        return $verification;

    }

    public function verify($type, OneTimePin $otp = null)
    {
        if (!$this->user) throw new Exception("Failed to verify ". VerificationType::getString($type) .", no user binded!", 500);

        $verification = $this->user->verifications()->firstOrNew(['type' => $type]);
        switch ($type) {
            case VerificationType::PHONE_NUMBER:
                $verification->reference = $this->user->phone_number;
                break;
            case VerificationType::EMAIL:
                $verification->reference = $this->user->email;
                break;
            default:
                # code...
                break;
        }

        $verification->status = VerificationStatus::VERIFIED;
        $verification->verified_date = Carbon::now();

        if (!$verification->save()) {
            throw new Exception('Failed to verify verification!', 500);
        }

        if ($otp) {
            $otp->used();
        }

        $history = HistoryLogger::create($verification, $this->user, $verification->status);

        return $verification;
    }

    public function unverify($type)
    {
        if (!$this->user) throw new Exception("Failed to unverify ". VerificationType::getString($type) .", no user binded!", 500);

        $verification = $this->user->verifications()->where(['type' => $type])->first();

        if ($verification) {
            $verification->status = VerificationStatus::PENDING;
            if (!$verification->save()) {
                throw new Exception('Failed to unverify verification!', 500);
            }
            $history = HistoryLogger::create($verification, $this->user, $verification->status);
        }

        return $verification;
    }
    
    public function verifyDocument(Document $document)
    {
        $verification = $document->verifications()->firstOrNew(['type' => VerificationType::DOCUMENT]);
        $verification->status = VerificationStatus::VERIFIED;
        $verification->verified_date = Carbon::now();

        if (!$verification->save()) {
            throw new Exception('Failed to update verification!', 500);
        }

        $history = HistoryLogger::create($verification, $this->user, $verification->status);

        return $document;
    }
    
    public function declineDocument(Document $document, $remarks = null)
    {
        $verification = $document->verifications()->firstOrNew(['type' => VerificationType::DOCUMENT]);
        $verification->status = VerificationStatus::DECLINED;
        $verification->verified_date = Carbon::now();

        if (!$verification->save()) {
            throw new Exception('Failed to update verification!', 500);
        }
        
        $history = HistoryLogger::create($verification, $this->user, $verification->status, $remarks);

        return $document;
    }
}
