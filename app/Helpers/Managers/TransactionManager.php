<?php

namespace App\Helpers\Managers;

use App\Facades\HistoryLogger;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\WalletType;
use App\Helpers\Managers\MutationManager;
use App\Models\Admin;
use App\Models\Business;
use App\Models\Order;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\OrderStatus;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Helpers\Enums\PackageType;
use Carbon\Carbon;
use DB;
use Exception;
use GlobalHelper;
use Illuminate\Support\Facades\Mail;

class TransactionManager
{
    protected $user;
    protected $order;
    protected $transaction;
    protected $relatedTransaction;
    protected $date;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    public function setDate(Carbon $date)
    {
        $this->date = $date;
        return $this;
    }

    public function getOwnTransaction($types = null, $date = null, $offset = null, $limit = null)
    {
        if (!$this->user) throw new Exception("Failed to get transactions, no user binded!", 500);
        $userWalletsId = $this->user->wallets()->pluck('id');

        // Note: Filter type should be shown on live transactions
        $types = $types ?: TransactionType::getList();

        $query = Transaction::query();

        if ($date && is_array($date)) {
            $startDate = $date['start_date'];
            $endDate = $date['end_date'];

            if ($startDate && $endDate) {
                $query->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()]);
            } else if ($startDate && !$endDate) {
                $query->whereDate('created_at', '>=', $startDate->toDateTimeString());
            } else if (!$startDate && $endDate) {
                $query->whereDate('created_at', '<=', $endDate->toDateTimeString());
            }
        } else if ($date && !is_array($date)) {
            $start = $date->toDateTimeString();
            $end = $date->addDay()->toDateTimeString();

            $query->where('created_at', '>=', $start)->where('created_at', '<', $end);
        }

        // Query by owner wallet, either on credit or debit side
        $query->where(function ($query) use ($userWalletsId) {
            $query->whereIn('debit_wallet_id', $userWalletsId)
                  ->orWhereIn('credit_wallet_id', $userWalletsId);
        });

        $query->whereIn('type', $types);


        $query->orderBy('created_at', 'desc')->orderBy('id', 'desc');

        if (isset($offset)) $query->offset($offset);
        if (isset($limit)) $query->limit($limit);

        $transactions = $query->get();
        // Map collection with owned_as, type_string field
        $transactions->map(function($t) use ($userWalletsId) {
            $this->alterTransaction($t, $userWalletsId);
            return $t;
        });

        return $transactions;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    public function setRelatedTransaction(Transaction $relatedTransaction)
    {
        $this->relatedTransaction = $relatedTransaction;
        return $this;
    }

    public function create(Wallet $debitWallet, Wallet $creditWallet, $amount, $type, $referenceNumber = null, $description = null)
    {
      \Log::info('transactionManager->create()');
        if (!$this->order) throw new Exception("No order binded!", 500);
        $adminFee = 0;
        $checkLimit = 0;
        $transaction = new Transaction;
        $transaction->debitWallet()->associate($debitWallet);
        $transaction->creditWallet()->associate($creditWallet);
        \Log::info($this->user->role_id);
        if ($this->user->role_id == 5) {
            $checkLimit = $this->user->orders()->whereMonth('created_at', date('n'))->where('status', OrderStatus::COMPLETED)->where('type', '=', OrderType::PHONE_PREPAID)->get()->sum('total');
        }
            if ($type == TransactionType::PLN) {
              $transaction->total_amount = $amount + $this->order->options->pln_prepaid_admin;
            } else if ($type == TransactionType::PULSA) {
                if ($this->user->packages == PackageType::FREE) {
                    $adminFee = 1500;
                } elseif ($this->user->packages == PackageType::SILVER) {
                    if ($checkLimit > 500000) $adminFee = 1300;
                } else if ($this->user->packages == PackageType::GOLD) {
                    if ($checkLimit > 1000000) $adminFee = 1200;
                } else {
                    if ($checkLimit > 1500000) $adminFee = 1000;
                }
                $transaction->total_amount = $amount + $adminFee;
            } else {
              $transaction->total_amount = $amount;
            }
        
        $transaction->type = $type;
        if ($type == TransactionType::TRANSFER || $type == TransactionType::PAY) {
            $transaction->description = $this->order->options->note ?? null;
        } else if ($type == TransactionType::PULSA) {
            $transaction->description = $this->order->options->description ?? null;
        }

        if ($description) {
            $transaction->description = $description;
        }

        $debitOwnable = $debitWallet->ownable;
        $creditOwnable = $creditWallet->ownable;

        if (!$debitOwnable || !$creditOwnable) {
            throw new Exception("Failed to create transaction, debit or credit ownable nof found!", 400);
        }

        $transaction->reference_number = $referenceNumber ?? $this->order->reference_number;
        $transaction->order()->associate($this->order);

        if ($type == TransactionType::UPGRADE_PACKAGE) {
            $transaction->debit_current_balance = 0;
            $transaction->credit_current_balance = 0;
        } else {
            $transaction->debit_current_balance = $debitOwnable->payWallets()->sum('balance');
            $transaction->credit_current_balance = $creditOwnable->payWallets()->sum('balance');
        }

        if (!$transaction->save()) {
            throw new Exception('Failed to create transaction!', 500);
        }
        if ($transaction->type == TransactionType::PULSA) {
            $creditWallet->deduct($transaction->total_amount);
        } else if ($transaction->type == TransactionType::PLN) {
            // \Log::info($amount);
            $creditWallet->deduct($transaction->total_amount);
        } else if ($transaction->type == TransactionType::GAME){
            $creditWallet->deduct($transaction->total_amount);
        } else if ($transaction->type == TransactionType::TOPUP) {
            if ($transaction->total_amount > 2000) $debitWallet->add($this->order->orderDetails[0]->price);
        } else if ($transaction->type == TransactionType::PAY) {
            $this->createMutations($transaction);
        }

        return $transaction;
    }

    public function createMutations(Transaction $transaction)
    {
        $mutationManager = new MutationManager($this->user);
        $mutationManager->setTransaction($transaction);

        $debitWallet = $transaction->debitWallet;
        $creditWallet = $transaction->creditWallet;
        $debitMutation = $mutationManager->create($debitWallet, $creditWallet, $debitWallet);
        $creditMutation = $mutationManager->create($debitWallet, $creditWallet, $creditWallet);

        return $transaction;
    }

    public function alterTransaction(Transaction $transaction, $userWalletsId)
    {
        $transaction->setRelations([]);
        $ownedAs = null;
        if (in_array($transaction->debit_wallet_id, $userWalletsId->toArray())) {
            $ownedAs = 'debit';
        }
        if (in_array($transaction->credit_wallet_id, $userWalletsId->toArray())) {
            if (!$ownedAs) {
                $ownedAs = 'credit';
            } else {
                $ownedAs = 'both';
            }
        }

        $creditUser = $transaction->creditWallet ? $transaction->creditWallet->ownable : null;
        $debitUser = $transaction->debitWallet ? $transaction->debitWallet->ownable : null;
        $transaction['credit_user_name'] = $creditUser ? ($creditUser instanceof Business) ? $creditUser->name . ' (Business)' : $creditUser->fullname : 'Unknown';
        $transaction['debit_user_name'] = $debitUser ? ($debitUser instanceof Business) ? $debitUser->name . ' (Business)' : $debitUser->fullname : 'Unknown';
        $transaction['credit_user_phone_number'] = $creditUser ? $creditUser->phone_number : '-';
        $transaction['debit_user_phone_number'] = $debitUser ? $debitUser->phone_number : '-';
        $transaction['owned_as'] = $ownedAs;
        $transaction['type_string'] = TransactionType::getString($transaction->type);
        $transaction['formatted_total_amount'] = GlobalHelper::toCurrency($transaction->total_amount);
        $transaction['order_status'] = $transaction->order ? $transaction->order->status : null;
        unset($transaction->order);
        if ($ownedAs == 'debit') {
            $transaction['reference_phone_number'] = $creditUser ? $creditUser->phone_number : '';
        } else if (in_array($ownedAs, ['credit', 'both'])) {
            $transaction['reference_phone_number'] = $debitUser ? $debitUser->phone_number : '';
        } else {
            $transaction['reference_phone_number'] = '';
        }

        return $transaction;
    }
}
