<?php

namespace App\Helpers\Managers;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\User;
use App\Models\Document;
use App\Models\Verification;
use App\Helpers\Enums\DocumentType;
use App\Helpers\Enums\VerificationType;
use App\Facades\HistoryLogger;
use App\Helpers\Managers\AttachmentManager;
use App\Helpers\Managers\VerificationManager;

class DocumentManager
{
    protected $user;
    protected $disk;

    public function __construct($user = null)
    {
        $this->user = $user;
        $this->disk = 's3';
    }

    public function create($type, $data)
    {
        if (!$this->user) throw new Exception("Failed to create ". DocumentType::getString($type) .", no user binded!", 500);
        $document = $this->user->documents()->firstOrNew(['type' => $type]);
        $document->description = isset($data['description']) ? $data['description'] : '';
        $document->identification_number = isset($data['identification_number']) ? $data['identification_number'] : '';
        $document->options = isset($data['options']) ? $data['options'] : null;

        if ($document->attachment) {
            $oldAttachment = $document->attachment;
        }

        if (isset($data['attachment'])) {
            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->setAttachable($document)->create($data['attachment']);
            $document->attachment()->associate($attachment);
        }

        if (!$document->save()) {
            throw new Exception('Failed to create document!', 500);
        }

        if (isset($oldAttachment)) {
            $attachmentManager = new AttachmentManager($this->disk);
            $deteleAttachment = $attachmentManager->delete($oldAttachment, true);
        }

        $verificationManager = new VerificationManager($this->user);
        $verification = $verificationManager->create($document, VerificationType::DOCUMENT);

        return $document;
    }

    public function verify(Document $document, $data = [])
    {
        if (!$this->user) throw new Exception("Failed to verify ". DocumentType::getString($type) .", no user binded!", 500);

        $document->identification_number = $data['identification_number'];
        if (!$document->save()) throw new Exception('Failed to update document!', 500);

        $verificationManager = new VerificationManager($this->user);
        $verificationManager->verifyDocument($document);
        
        return $document;
    }

    public function decline(Document $document, $remarks = null)
    {
        if (!$this->user) throw new Exception("Failed to decline ". DocumentType::getString($type) .", no user binded!", 500);
        
        $verificationManager = new VerificationManager($this->user);
        $verificationManager->declineDocument($document, $remarks);
        
        return $document;
    }
}
