<?php

namespace App\Helpers;

use Exception;
use Carbon\Carbon;
use Twilio\Rest\Client as TwilioClient;

class SMS
{
    protected $user;
    protected $client;
    protected $number;

    public function __construct()
    {
        $sid = config('sms.sid'); // Your Account SID from www.twilio.com/console
        $token = config('sms.token'); // Your Auth Token from www.twilio.com/console
        $this->number = config('sms.number');
        $this->client = new TwilioClient($sid, $token);
    }

    public function send($to, $message = "Hello from the other side.")
    {
        $sms = $this->client->messages->create(
            $to, // Text this number
            array(
                'from' => $this->number, // From a valid Twilio number
                'body' => $message
            )
        );
        return $sms;
    }
}
