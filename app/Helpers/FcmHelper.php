<?php

namespace App\Helpers;

use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

final class FcmHelper {

    public static function pushNotification($tokens, $options)
    {
        if(!$tokens) return (object) ['success' => false];
        if(is_array($tokens)) {
            if (count($tokens) <= 0) return (object) ['success' => false];
        }
        if(!isset($options['sound'])) $options['sound'] = 'default';
        if(!isset($options['data'])) $options['data'] = [];

        $tokens = array_unique($tokens);
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($options['title']);
        $notificationBuilder->setBody($options['body'])
            ->setSound($options['sound']);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($options['data']);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        return (object) [
            'success' => true,
            'numberSuccess' => $downstreamResponse->numberSuccess(),
            'numberFailure' => $downstreamResponse->numberFailure(),
            'numberModification' => $downstreamResponse->numberModification(),
            'tokensToDelete' => $downstreamResponse->tokensToDelete(),
            'tokensToModify' => $downstreamResponse->tokensToModify(),
            'tokensToRetry' => $downstreamResponse->tokensToRetry(),
            'tokensWithError' => $downstreamResponse->tokensWithError(),
        ];
    }
}

?>
