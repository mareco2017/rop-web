<?php

namespace App\Helpers\Customizers;

use Exception;

class PaymentCustomizer
{
    protected $rules;

    public function __construct()
    {
        $this->setRule();
    }

    public function getRule($string = '')
    {
        $rules = isset($this->rules[$string]) ? $this->rules[$string] : [];
        return $rules;
    }

    protected function setRule()
    {
        $this->rules = [
            'PHONE_PREPAID' => [
                'password' => 'required',
                'wallet_type' => 'required|numeric|between:0,4',
                'payment_product_id' => 'required|exists:payment_products,id',
                'phone_number' => 'required|numeric'
            ],
            'PHONE_POSTPAID' => [
                'password' => 'required',
                'wallet_type' => 'required|numeric|between:0,4',
                'order_id' => 'required|exists:orders,id',
            ],
            'GAME' => [
                'password' => 'required',
                'wallet_type' => 'required|numeric|between:0,4',
                'payment_product_id' => 'required|exists:payment_products,id'
            ],
            'PLN_PREPAID' => [
                'password' => 'required',
                'wallet_type' => 'required|numeric|between:0,4',
                'order_id' => 'required|exists:orders,id',
                'payment_product_id' => 'numeric|exists:payment_products,id',
                'customer_id' => 'nullable'
                // 'amount' => 'required|numeric'
            ],
            'PLN_POSTPAID' => [
                'password' => 'required',
                'wallet_type' => 'required|numeric|between:0,4',
                'order_id' => 'required|exists:orders,id',
                'customer_id' => 'nullable'
                // 'amount' => 'required|numeric'
            ],
            // 'PHONE_POSTPAID' => [
            //     'pin' => 'required|numeric',
            //     'wallet_type' => 'required|numeric|between:0,4',
            //     'area_code' => 'required|numeric',
            //     'order_id' => 'required|exists:orders,id',
            //     'payment_product_id' => 'required|exists:payment_products,id'
            // ],
        ];
    }
}
