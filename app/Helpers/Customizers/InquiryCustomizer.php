<?php

namespace App\Helpers\Customizers;

use Exception;

class InquiryCustomizer
{
    protected $rules;
    protected $converters;

    public function __construct()
    {
        $this->setRule();
        $this->setConverter();
    }

    public function getRule($string = '')
    {
        $rules = isset($this->rules[$string]) ? $this->rules[$string] : [];
        return $rules;
    }

    public function getConverter($string = '')
    {
        $converter = isset($this->converters[$string]) ? $this->converters[$string] : [];
        return $converter;
    }

    protected function setRule()
    {
        $this->rules = [
            'PLN_PREPAID' => [
                'customer_id' => 'required|min:11|max:12'
            ],
            'PLN_POSTPAID' => [
                'customer_id' => 'required|digits:12'
            ],
            'PHONE_POSTPAID' => [
                'payment_product_id' => 'numeric|exists:payment_products,id',
                'phone_number' => 'required|numeric'
            ],
            'BPJS' => [
                'customer_id' => 'numeric|exists:payment_products,id',
                'month' => 'required|integer|between:1,12'
            ],
            // 'PHONE_POSTPAID' => [
            //     'payment_product_id' => 'numeric|exists:payment_products,id',
            //     'phone_number' => 'required|numeric',
            //     'area_code' => 'required|numeric'
            // ],
        ];
    }

    protected function setConverter()
    {
        $this->converters = [
            'PLN_PREPAID' => [
                'customer_id1' => '',
                'customer_id2' => '',
                'customer_id3' => '',
                'method' => 'biller_code',
                'payment_product_code' => 'customer_id',
                'amount' => 'customer_phone_number'
            ],
            'PLN_POSTPAID' => [
                'customer_id1' => '',
                'customer_id2' => '',
                'customer_id3' => '',
                'method' => 'biller_code',
                'payment_product_code' => 'customer_id',
                'amount' => 'customer_phone_number'
            ],
            'BPJS' => [
                'customer_id' => 'numeric|exists:payment_products,id',
                'month' => 'required|integer|between:1,12',
            ]
        ];
    }
}
