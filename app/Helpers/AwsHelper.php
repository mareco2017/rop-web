<?php

namespace App\Helpers;

use Config;
use Storage;

final class AwsHelper {

    public static function uploadFile($file, $fieldName = 'file', $folder = '', $fileType = '', $maxSize = 0)
    {
        $response = (object) ['success' => false, 'url' => '', 'key' => '', 'extension' => '', 'message' => ''];
        $rule = 'required';
        if ($fileType) $rule = $rule.'|mimes:'.$fileType;
        if ($maxSize > 0) $rule = $rule.'|max:'.$maxSize; //kilobytes
        $validator = validator()->make([$fieldName => $file], [$fieldName => $rule]);
        if ($validator->fails()) {
            $message = $validator->errors()->first();
            $response->message = $message;
            return $response;
        }
        $ext = $file->getClientOriginalExtension();
        $fileName = time().'_'.str_random(15).'.'.$ext;
        $s3 = Storage::disk('s3');
        $filePath = $folder.$fileName;
        if (!$s3->put($filePath, file_get_contents($file), 'public')) {
            $response->message = 'Upload fail';
            return $response;
        }

        $response->success = true;
        $response->url = $s3->url($filePath);
        $response->key = $filePath;
        $response->extension = $ext;
        return $response;
    }

    public static function deleteFile($key)
    {
        $s3 = Storage::disk('s3');

        if (!$s3->delete($key)) return (object) ['success' => false, 'message' => 'Delete fail'];
        return (object) ['success' => true, 'message' => ''];
    }

    public static function deleteBulkFile($urls)
    {
        $bucket = Config::get('filesystems.disks.s3.bucket');
        $s3 = Storage::disk('s3');
        \Log::info('======2====='.$s3);
        if (!$s3->delete($keys)) return (object) ['success' => false, 'message' => 'Delete fail'];
        return (object) ['success' => true, 'message' => ''];
    }

}

?>
