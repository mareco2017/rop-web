<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Password;
use App\Models\Order;
use App\Helpers\Enums\OrderType;

class UniqueHelper {
    /*
        Generate unique number/string
    */

    public static function generateTopupUniqueNumber($length = 6, $neg = false)
    {
        $rNumber = self::randomNumber($length);
        if ($neg) {
            $rNumber = +$rNumber;
        }
        $exists = Order::active()->type(OrderType::TOPUP)->whereHas('orderDetails', function($q) use ($rNumber) {
            $q->where('discount', '=', $rNumber);
        })->exists();

        if ($exists) {
            self::generateTopupUniqueNumber($length);
        } else {
            return $rNumber;
        }
    }

    public static function generateNumber($length = 6, $modelClass = null, $fieldName = null, $prefix = null)
    {
        $randomNumber = self::randomNumber($length);
        // $code = uniqid($prefix . Carbon::now()->format('Ymd'));
        $code = $prefix . $randomNumber;

        if ($modelClass && $fieldName) {
            if ($modelClass::where($fieldName, '=', $code)->exists()) {
                //Model Found -- call self.
                self::generateCode($length, $modelClass, $fieldName, $prefix);
            } else {
                //Model Not found. is uinque
                return $code;
            }
        } else {
            return $code;
        }
    }

    public static function generate($length = 64, $modelClass = null, $fieldName = null)
    {
        $token = substr(Password::getRepository()->createNewToken(), 0, $length);

        if ($modelClass && $fieldName) {
            if ($modelClass::where($fieldName, '=', $token)->exists()) {
                //Model Found -- call self.
                self::generate($length, $modelClass, $fieldName);
            } else {
                //Model Not found. is uinque
                return $token;
            }
        } else {
            return $token;
        }
    }

    public static function randomNumber($length = 6) {
        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}