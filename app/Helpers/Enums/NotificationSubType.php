<?php 
 
namespace App\Helpers\Enums;
 
final class NotificationSubType { 

    //IDENTITY =============
    const UPGRADED = 0; 
    const UPGRADED_DECLINED = 1; 
    const VERIFIED = 2;
    const EMAIL_CHANGED = 3;
    const PHONE_CHANGED = 4;
    const RESET = 5;
    //TOPUP ================
    const BANK_TRANSFER = 6;
    const MERCHANT = 7;
    const EVENT_EXT_MARKETING_APPROVE = 8;
 
    public static function getList() { 
      return [ 
        NotificationSubType::UPGRADED, 
        NotificationSubType::UPGRADED_DECLINED,
        NotificationSubType::VERIFIED,
        NotificationSubType::EMAIL_CHANGED,
        NotificationSubType::PHONE_CHANGED,
        NotificationSubType::RESET,
        NotificationSubType::BANK_TRANSFER,
        NotificationSubType::MERCHANT,
        NotificationSubType::EVENT_EXT_MARKETING_APPROVE
      ]; 
    } 
 
    public static function getArray() { 
      $result = []; 
      foreach (self::getList() as $arr) { 
        $result[$arr] = self::getString($arr); 
      } 
      return $result; 
    }

    public static function getString($val) {
      switch ($val) {
        case 0:
          return "Upgraded";
          break;
        case 1:
          return "Upgrade Declined";
          break;
        case 2:
          return "Verified";
          break;
        case 3:
          return "Email Changed";
          break;
        case 4:
          return "Phone Changed";
          break;
        case 5:
          return "Reset";
          break;
        case 6:
          return "Bank Transfer";
          break;
        case 7:
          return "Merchant";
          break;
        case 8:
          return "External Marketing Request Approved";
          break;
      }
  }

}
?> 