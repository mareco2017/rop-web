<?php

namespace App\Helpers\Enums;

final class PaymentProductType {

	const PHONE = 0;
	const POSTPAID_PHONE = 1;
	const PLN = 2;
	const GAME = 3;
	const PDAM = 4;

	public static function getList() {
		return [
			PaymentProductType::PHONE,
			PaymentProductType::POSTPAID_PHONE,
			PaymentProductType::PLN,
			PaymentProductType::GAME,
			PaymentProductType::PDAM,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return __('enums/messages.phone');
			case 1:
				return __('enums/messages.postpaid_phone');
			case 2:
				return __('enums/messages.pln');
			case 3:
				return __('enums/messages.game_voucher');
			case 4:
				return __('enums/messages.pdam');
		}
	}

}

?>
