<?php

namespace App\Helpers\Enums;

use App\Helpers\Enums\TransactionType;

final class OrderType {

	const TOPUP = 0;
	const WITHDRAW = 1;
	const PLN_PREPAID = 2; // Token
	const PLN_POSTPAID = 3;
	const PHONE_PREPAID = 4; // Pulsa Biasa
	const PHONE_POSTPAID = 5;
	const TOPUP_CREDIT = 6;
	const PAY = 7;
	const TRANSFER = 8;
	const SPLIT = 9;
	const CASHBACK = 10;
	const GAME = 11;
	const FREEZE = 12;
	const UNFREEZE = 13;
	const WITHDRAW_CREDIT = 14;
	const SPONSOR_BONUS = 15;
	const BPJS = 16;
	const REVERSAL = 17;
	const UPGRADE_PACKAGE = 18;
	const PAIRING_BONUS = 19;

	public static function getList() {
		return [
			OrderType::TOPUP,
			OrderType::WITHDRAW,
			OrderType::PLN_PREPAID,
			OrderType::PLN_POSTPAID,
			OrderType::PHONE_PREPAID,
			OrderType::PHONE_POSTPAID,
			OrderType::TOPUP_CREDIT,
			OrderType::PAY,
			OrderType::TRANSFER,
			OrderType::SPLIT,
		    OrderType::CASHBACK, 
		    OrderType::GAME, 
		    OrderType::FREEZE, 
		    OrderType::UNFREEZE,
		    OrderType::WITHDRAW_CREDIT, 
		    OrderType::SPONSOR_BONUS,
		    OrderType::BPJS,
		    OrderType::REVERSAL,
		    OrderType::UPGRADE_PACKAGE,
		    OrderType::PAIRING_BONUS,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Top Up";
			case 1:
				return "Withdraw";
			case 2:
				return "PLN Prepaid";
			case 3:
				return "PLN Postpaid";
			case 4:
				return "Phone Prepaid";
			case 5:
				return "Phone Postpaid";
			case 6:
				return "Top Up Credit";
			case 7:
				return "Pay";
			case 8:
				return "Transfer";
			case 9:
				return "Split";
			case 10:
				return "Cashback";
		    case 11: 
		        return "Game";
		    case 12: 
		        return "Freeze";
		    case 13: 
		        return "Unfreeze";
		    case 14: 
		        return "Withdraw Credit"; 
		    case 15: 
		        return "Sponsor Bonus"; 
		    case 16: 
		        return "BPJS"; 
		    case 17: 
		        return "Reversal"; 
		    case 18: 
		        return "Upgrade Package"; 
		    case 19: 
		        return "Pairing Bonus"; 
		}
	}

	public static function getStringBusiness($val) {
		switch ($val) {
			case 0:
				return "Top Up";
			case 1:
				return "Withdraw ".__('business/messages.store_balance');
			case 2:
				return "PLN Prepaid";
			case 3:
				return "PLN Postpaid";
			case 4:
				return "Phone Prepaid";
			case 5:
				return "Phone Postpaid";
			case 6:
				return "Top Up Credit";
			case 7:
				return "Pay";
			case 8:
				return "Transfer";
			case 9:
				return "Split";
			case 10:
				return "Cashback";
		    case 11: 
		        return "Game";
		    case 12: 
		        return "Freeze";
		    case 13: 
		        return "Unfreeze";
		    case 14: 
		        return "Withdraw ".__('business/messages.top_up_balance'); 
		    case 15: 
		        return "Sponsor Bonus"; 
		    case 16: 
		        return "BPJS"; 
		    case 17: 
		        return "Reversal"; 
		    case 18: 
		        return "Upgrade Package"; 
		    case 19: 
		        return "Pairing Bonus";
		}
	}

	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "TOP";
			case 1:
				return "WTH";
			case 2:
				return "TOK";
			case 3:
				return "PLN";
			case 4:
				return "PPR";
			case 5:
				return "PPO";
			case 6:
				return "TCR";
			case 7:
				return "PAY";
			case 8:
				return "TRF";
			case 9:
				return "SPT";
			case 10:
				return "CSB";
			case 11:
				return "GME";
			case 12:
				return "FRZ";
			case 13:
				return "UFZ";
			case 14:
				return "WCR";
			case 15:
				return "SPO";
			case 16:
				return "BKS";
			case 17:
				return "RVS";
			case 18:
				return "UPG";
			case 19:
				return "PAIR";
		}
	}

	public static function slugToEnum($val) {
		switch ($val) {
			case 'pln_prepaid':
				return OrderType::PLN_PREPAID;
			case 'pln_postpaid':
				return OrderType::PLN_POSTPAID;
			case 'telkomsel_prepaid':
				return OrderType::TELKOMSEL_PREPAID;
		}
	}

	public static function enumToSlug($val) {
		switch ($val) {
			case OrderType::PHONE_PREPAID:
				return 'PHONE_PREPAID';
			case OrderType::PHONE_POSTPAID:
				return 'PHONE_POSTPAID';
			case OrderType::PLN_PREPAID:
				return 'PLN_PREPAID';
			case OrderType::PLN_POSTPAID:
				return 'PLN_POSTPAID';
			case OrderType::GAME:
				return 'GAME';
			case OrderType::BPJS:
				return 'BPJS';
		}
	}

	public static function getTypeFromInquiry($val) {
		switch ($val) {
			case 1:
				return OrderType::PHONE_PREPAID;
			case 2:
				return OrderType::PHONE_POSTPAID;
			case 3:
				return null;
			case 4:
				return OrderType::GAME;
			case 5:
				return OrderType::BPJS;
		}
			
	}

	public static function getTypeFromPayment($val) {
		switch ($val) {
			case 1:
				return OrderType::PHONE_PREPAID;
			case 2:
				return OrderType::PHONE_POSTPAID;
			case 3:
				return null;
			case 4:
				return OrderType::GAME;
			case 5:
				return OrderType::BPJS;
		}
	}

	public static function getReviewList()
	{
		return [
			OrderType::PAY,
			OrderType::SPLIT
		];
	}

	public static function getPlnList()
	{
		return [
			OrderType::PLN_PREPAID,
			OrderType::PLN_POSTPAID
		];
	}

	public static function getPulsaList()
	{
		return [
			OrderType::PHONE_PREPAID,
			OrderType::PHONE_POSTPAID
		];
	}

	public static function getGameList()
	{
		return [
    		OrderType::GAME
		];
	}

	public static function getOwnableDebitList()
	{
		return [
			OrderType::TOPUP,
			OrderType::TOPUP_CREDIT,
		    OrderType::CASHBACK,
		    OrderType::UNFREEZE,
		    OrderType::SPONSOR_BONUS
		];
	}

	public static function getOwnableCreditList()
	{
		return [
			OrderType::WITHDRAW,
			OrderType::PLN_PREPAID,
			OrderType::PLN_POSTPAID,
			OrderType::PHONE_PREPAID,
			OrderType::PHONE_POSTPAID,
			OrderType::PAY,
			OrderType::SPLIT,
		    OrderType::GAME, 
		    OrderType::FREEZE,
		    OrderType::WITHDRAW_CREDIT, 
		    OrderType::BPJS
		];
	}

	public static function getTransactionType($val) {
		switch ($val) {
			case 0:
				return TransactionType::TOPUP;
			case 1:
				return TransactionType::WITHDRAW;
			case 2:
				return TransactionType::PLN;
			case 3:
				return TransactionType::PLN;
			case 4:
				return TransactionType::PULSA;
			case 5:
				return TransactionType::PULSA;
			case 6:
				return TransactionType::TOPUP_CREDIT;
			case 7:
				return TransactionType::PAY;
			case 8:
				return TransactionType::TRANSFER;
			case 9:
				return TransactionType::SPLIT_PAY;
			case 10:
				return TransactionType::CASHBACK;
			case 11:
				return TransactionType::GAME;
			case 12:
				return TransactionType::FREEZE;
			case 13:
				return TransactionType::UNFREEZE;
			case 14:
				return TransactionType::WITHDRAW_CREDIT;
			case 15:
				return TransactionType::SPONSOR_BONUS;
			case 16:
				return "BKS";
			case 17:
				return TransactionType::REVERSAL;
			case 18:
				return TransactionType::UPGRADE_PACKAGE;
			case 19:
				return TransactionType::PAIRING_BONUS;
		}
	}
}

?>
