<?php 
 
namespace App\Helpers\Enums;

use App\Helpers\Enums\NotificationCategory;
 
final class NotificationType { 

    const IDENTITY = 0; 
    const PROMO = 1; 
    const VOUCHER = 2; 
    const TOPUP = 3; 
    const WITHDRAW = 4; 
    const TRANSFER = 5; 
    const CASHBACK = 6; 
    const REFUND = 7;
    const PAY = 8;
    const NEWLETTER = 9;
    const TOPUP_DECLINED = 10;
    const WITHDRAW_DECLINED = 11;
    const BONUS_REFERRAL = 12;
    const TOPUP_CREDIT = 13; 
    const WITHDRAW_CREDIT = 14;
    const PLN_PREPAID = 15;
    const PLN_POSTPAID = 16;
    const PDAM = 17;
    const PHONE_POSTPAID = 18;
    const PAIRING_BONUS = 19;
    const STATEMENT = 20;
 
    public static function getList() { 
      return [ 
        NotificationType::IDENTITY, 
        NotificationType::PROMO, 
        NotificationType::VOUCHER, 
        NotificationType::TOPUP, 
        NotificationType::WITHDRAW, 
        NotificationType::TRANSFER, 
        NotificationType::CASHBACK,
        NotificationType::REFUND,
        NotificationType::PAY,
        NotificationType::NEWLETTER,
        NotificationType::TOPUP_DECLINED,
        NotificationType::WITHDRAW_DECLINED,
        NotificationType::BONUS_REFERRAL,
        NotificationType::TOPUP_CREDIT,
        NotificationType::WITHDRAW_CREDIT,
        NotificationType::PLN_PREPAID,
        NotificationType::PLN_POSTPAID,
        NotificationType::PDAM,
        NotificationType::PHONE_POSTPAID,
        NotificationType::PAIRING_BONUS,
        NotificationType::STATEMENT,
      ]; 
    } 
 
    public static function getArray() { 
      $result = []; 
      foreach (self::getList() as $arr) { 
        $result[$arr] = self::getString($arr); 
      } 
      return $result; 
    } 

    public static function getCategory($val) {
    	$result = [];
    	switch ($val) {
    		case NotificationType::IDENTITY:
    			$result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
    		case NotificationType::PROMO:
    			$result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
    		case NotificationType::VOUCHER:
    			$result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
    		case NotificationType::TOPUP:
    			$result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
    		case NotificationType::WITHDRAW:
    			$result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL, NotificationCategory::SMS];
          break;
    		case NotificationType::TRANSFER:
    			$result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
    		case NotificationType::CASHBACK:
    			$result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
        case NotificationType::REFUND:
          $result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
    		case NotificationType::PAY:
    			$result = [NotificationCategory::APP];
          break;
        case NotificationType::NEWLETTER:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
        case NotificationType::TOPUP_DECLINED:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
        case NotificationType::WITHDRAW_DECLINED:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
        case NotificationType::BONUS_REFERRAL:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
        case NotificationType::TOPUP_CREDIT:
          $result = [NotificationCategory::APP, NotificationCategory::SMS, NotificationCategory::EMAIL];
          break;
        case NotificationType::WITHDRAW_CREDIT:
          $result = [NotificationCategory::APP, NotificationCategory::SMS, NotificationCategory::EMAIL];
          break;
        case NotificationType::PLN_PREPAID:
          $result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
        case NotificationType::PLN_POSTPAID:
          $result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
        case NotificationType::PDAM:
          $result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
        case NotificationType::PHONE_POSTPAID:
          $result = [NotificationCategory::APP, NotificationCategory::EMAIL];
          break;
        case NotificationType::PAIRING_BONUS:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
        case NotificationType::STATEMENT:
          $result = [NotificationCategory::APP, NotificationCategory::PUSH, NotificationCategory::EMAIL];
          break;
    		}
    	return $result;
    }

    public static function getString($val, $info = "") {
      switch ($val) {
        case 0:
          return "Identity";
          break;
        case 1:
          return "Promo";
          break;
        case 2:
          return "Voucher";
          break;
        case 3:
          return "Top-up " . $info . " berhasil";
          break;
        case 4:
          return "Withdraw " . $info . " berhasil";
          break;
        case 5:
          return "Transfer from " . $info . " berhasil";
          break;
        case 6:
          return "Cashback " . $info . " Point";
          break;
        case 7:
          return "Refund " . $info . "berhasil";
          break;
        case 8:
          return "Pay";
          break;
        case 9:
          return "Newsletter";
          break;
        case 10:
          return "Top Up Fail";
          break;
        case 11:
          return "Withdraw Fail";
          break;
        case 12:
          return "Bonus Referral";
          break;
        case 13:
          return "Top-up Credit";
          break;
        case 14:
          return "Withdraw Credit";
          break;
        case 15:
          return "PLN Prepaid Payment";
          break;
        case 16:
          return "PLN Postpaid Payment";
          break;
        case 17:
          return "Pembayaran Tagihan Air";
          break;
        case 18:
          return "Pembayaran Tagihan Telepon";
          break;
      }
  }

    public static function getSlug($val) {
      switch ($val) { //for setting checking
        case 0:
          return "identity";
          break;
        case 1:
          return "promotion";
          break;
        case 2:
          return "promotion";
          break;
        case 3:
          return "receipt_topup";
          break;
        case 4:
          return "receipt_withdraw";
          break;
        case 5:
          return "receipt_transfer";
          break;
        case 6:
          return "";
          break;
        case 7:
          return "";
          break;
        case 8:
          return "receipt_pay";
          break;
        case 9:
          return "newsletter";
          break;
        case 10:
          return "receipt_topup";
          break;
        case 11:
          return "";
          break;
        case 12:
          return "";
          break;
        case 13:
          return "";
          break;
        case 14:
          return "";
          break;
        case 15:
          return "";
          break;
        case 16:
          return "";
          break;
        case 17:
          return "";
          break;
        case 18:
          return "";
          break;
      }
  }
}
?> 