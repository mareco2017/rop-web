<?php

namespace App\Helpers\Enums;

final class PromoCategory {

	const VOUCHER = 0;
	const BANNER = 1;
	const NEWS = 2;

	public static function getList() {
		return [
			PromoCategory::VOUCHER,
			PromoCategory::BANNER,
			PromoCategory::NEWS
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Voucher";
			case 1:
				return "Banner";
			case 2:
				return "News";
		}
	}

}

?>
