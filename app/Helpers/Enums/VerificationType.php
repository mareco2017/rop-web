<?php

namespace App\Helpers\Enums;

final class VerificationType {

	const PHONE_NUMBER = 0;
	const EMAIL = 1;
	const DOCUMENT = 2;

	public static function getList() {
		return [
			VerificationType::PHONE_NUMBER,
			VerificationType::EMAIL,
			VerificationType::DOCUMENT
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Phone Number";
			case 1:
				return "Email";
			case 2:
				return "Document";
		}
	}

}

?>
