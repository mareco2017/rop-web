<?php

namespace App\Helpers\Enums;

final class DocumentType {

	const NRIC = 0;
	const SELFIE = 1;
	const OTHER = 2;

	public static function getList() {
		return [
			DocumentType::NRIC,
			DocumentType::SELFIE,
			DocumentType::OTHER
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "NRIC";
			case 1:
				return "Selfie";
			case 2:
				return "Other";
		}
	}

}

?>
