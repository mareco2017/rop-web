<?php

namespace App\Helpers\Enums;

final class PairClaim {

	const NO = 0;
	const YES = 1;

	public static function getList() {
		return [
			PairClaim::NO,
			PairClaim::YES
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return 'No';
			case 1:
				return 'Yes';
		}
	}

}

?>
