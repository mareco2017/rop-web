<?php

namespace App\Helpers\Enums;

final class UpgradeRequestStatus {

	const PENDING = 0;
	const WAITING_FOR_PAYMENT = 1;
	const ON_PROCESS = 2;
	const ACCEPTED = 3;
	const DECLINED = 4;
	const CANCELLED = 5;
	const REFUNDED = 6;

	public static function getList() {
		return [
			UpgradeRequestStatus::PENDING,
			UpgradeRequestStatus::WAITING_FOR_PAYMENT,
			UpgradeRequestStatus::ON_PROCESS,
			UpgradeRequestStatus::ACCEPTED,
			UpgradeRequestStatus::DECLINED,
			UpgradeRequestStatus::CANCELLED,
			UpgradeRequestStatus::REFUNDED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return __('enums/messages.pending');
			case 1:
				return __('enums/messages.waiting_for_payment');
			case 2:
				return __('enums/messages.on_process');
			case 3:
				return __('enums/messages.accepted');
			case 4:
				return __('enums/messages.declined');
			case 5:
				return __('enums/messages.cancelled');
			case 6:
				return __('enums/messages.refunded');
		}
	}

}

?>
