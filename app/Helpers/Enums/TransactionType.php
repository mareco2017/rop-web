<?php

namespace App\Helpers\Enums;

final class TransactionType {

	const PAY = 0;
	const SPLIT_PAY = 1;
	const TRANSFER = 2;
	const TOPUP = 3;
	const TOPUP_CREDIT = 4;
	const WITHDRAW = 5;
	const CASHBACK = 6;
	const REFUND = 7;
	const PULSA = 8;
	const PLN = 9;
	const GAME = 10;
	const FREEZE = 11;
	const UNFREEZE = 12;
	const BONUS = 13;
	const WITHDRAW_CREDIT = 14;
	const SPONSOR_BONUS = 15;
	const REVERSAL = 16;
	const UPGRADE_PACKAGE = 17;
	const PAIRING_BONUS = 17;

	public static function getList() {
		return [
			TransactionType::PAY,
			TransactionType::SPLIT_PAY,
			TransactionType::TRANSFER,
			TransactionType::TOPUP,
			TransactionType::TOPUP_CREDIT,
			TransactionType::WITHDRAW,
			TransactionType::CASHBACK,
			TransactionType::REFUND,
			TransactionType::PULSA,
			TransactionType::PLN,
			TransactionType::GAME,
			TransactionType::FREEZE,
			TransactionType::UNFREEZE,
			TransactionType::BONUS,
			TransactionType::WITHDRAW_CREDIT,
			TransactionType::SPONSOR_BONUS,
			TransactionType::REVERSAL,
			TransactionType::UPGRADE_PACKAGE,
			TransactionType::PAIRING_BONUS
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getLimitedTransaction() {
		$result = [
			TransactionType::PAY,
			TransactionType::SPLIT_PAY
		];
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pay";
			case 1:
				return "Split Pay";
			case 2:
				return "Transfer";
			case 3:
				return "Top Up";
			case 4:
				return "Top Up Credit";
			case 5:
				return "Withdraw";
			case 6:
				return "Cashback";
			case 7:
				return "Refund";
			case 8:
				return "Pulsa";
			case 9:
				return "PLN";
			case 10:
				return "Game";
			case 11:
				return "Freeze Balance";
			case 12:
				return "Unfreeze Balance";
			case 13:
				return "Bonus";
			case 14:
				return "Withdraw Credit";
			case 15:
				return "Sponsor Bonus";
			case 16:
				return "Reversal";
			case 17:
				return "Upgrade Package";
			case 18:
				return "Pairing Bonus";

		}
	}

	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "PAY";
			case 1:
				return "SPL";
			case 2:
				return "TRF";
			case 3:
				return "TOP";
			case 4:
				return "TCR";
			case 5:
				return "WTH";
			case 6:
				return "CSB";
			case 7:
				return "REF";
			case 8:
				return "PHN";
			case 9:
				return "PLN";
			case 10:
				return "GAM";
			case 11:
				return "FRZ";
			case 12:
				return "UFR";
			case 13:
				return "BNS";
			case 14:
				return "WCR";
			case 15:
				return "SPO";
			case 16:
				return "RVS";
			case 17:
				return "UPG";
			case 18:
				return "PAIR";
		}
	}

	public static function getTransactionCode($val) {
		switch ($val) {
			case 0:
				return "PAY";
			case 1:
				return "SPL";
			case 2:
				return "TRF";
			case 3:
				return "TP";
			case 4:
				return "TCR";
			case 5:
				return "WT";
			case 6:
				return "CSB";
			case 7:
				return "REF";
			case 8:
				return "PHN";
			case 9:
				return "PLN";
			case 10:
				return "GAM";
			case 11:
				return "FRZ";
			case 12:
				return "UFR";
			case 13:
				return "BNS";
			case 14:
				return "WCR";
			case 15:
				return "SPO";
			case 16:
				return "RVS";
			case 17:
				return "UPG";
			case 18:
				return "PAIR";
		}
	}
}

?>
