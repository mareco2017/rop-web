<?php 
 
namespace App\Helpers\Enums; 
 
final class NotificationCategory { 
 
  const APP = 0; 
  const PUSH = 1; 
  const SMS = 2; 
  const EMAIL = 3;
 
  public static function getList() { 
    return [ 
      NotificationCategory::APP, 
      NotificationCategory::PUSH, 
      NotificationCategory::SMS, 
      NotificationCategory::EMAIL
    ]; 
  } 
 
  public static function getArray() { 
    $result = []; 
    foreach (self::getList() as $arr) { 
      $result[$arr] = self::getString($arr); 
    } 
    return $result; 
  }

  public static function getString($val) {
    switch ($val) {
        case 0:
          return "app";
          break;
        case 1:
          return "push";
          break;
        case 2:
          return "sms";
          break;
        case 3:
          return "email";
          break;
      }
  }
} 
 
?> 