<?php

namespace App\Helpers\Enums;

final class ActionStatus {

	const PENDING = 0;
	const SUCCESS = 1;
	const ERROR = 2;

	public static function getList() {
		return [
			ActionStatus::PENDING,
			ActionStatus::SUCCESS,
			ActionStatus::ERROR
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Success";
			case 2:
				return "Error";
		}
	}

}

?>
