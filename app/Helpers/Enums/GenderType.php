<?php

namespace App\Helpers\Enums;

final class GenderType {

	const MALE = 0;
	const FEMALE = 1;
	const OTHER = 2;

	public static function getList() {
		return [
			GenderType::MALE,
			GenderType::FEMALE,
			GenderType::OTHER
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Male";
			case 1:
				return "Female";
			case 2:
				return "Other";
		}
	}

}

?>
