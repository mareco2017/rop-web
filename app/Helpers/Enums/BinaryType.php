<?php

namespace App\Helpers\Enums;

final class BinaryType {

	const LEFT = 0;
	const RIGHT = 1;
	const ROOT = 2;

	public static function getList() {
		return [
			BinaryType::LEFT,
			BinaryType::RIGHT,
			BinaryType::ROOT,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Left";
			case 1:
				return "Right";
			case 2:
				return "Root";
		}
	}

}

?>
