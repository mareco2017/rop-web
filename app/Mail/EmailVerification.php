<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Admin;
use App\Models\OneTimePin;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, OneTimePin $otp)
    {
        if ($user instanceof User || $user instanceof Admin) {
            $this->user = $user;
        }
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@mareco.id')
                ->subject('Email Verification')
                ->view('templates.mails.email-verification');
    }
}
