<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;

class PostpaidPLNReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $notification;
    public $user;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
        $this->user = $notification->receivable;
        $this->data = $notification->options;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply.richonpay@gmail.com')
                ->subject('Pembayaran Tagihan PLN Berhasil')
                ->view('templates.mails.pln-pascabayar-receipt');
    }
}
