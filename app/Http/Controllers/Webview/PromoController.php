<?php

namespace App\Http\Controllers\Webview;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promo;

class PromoController extends Controller
{

    public function detail(Request $request, Promo $promo)
    {
        return view('webview.promo.detail')
            ->with('promo', $promo);
    }
}