<?php

namespace App\Http\Controllers\Webview;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Binary;

class BinaryController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    public function show(Request $request)
    {
        $user = $this->guard()->user();

        $binaryId = $user->binary ? $user->binary->id : null;

        $binaries = $user->getBinaryTree();
        return view('webview.binary.index')
            ->with('dekstop', false)
            ->with('binaries', $binaries)
            ->with('leftBinaryPoint', $user->total_left_binary_point)
            ->with('rightBinaryPoint', $user->total_right_binary_point)
            ->with('leftBinaryUser', $user->total_left_binary_user)
            ->with('rightBinaryUser', $user->total_right_binary_user);
    }

    protected function loadBinaries($binaries = null)
    {
        if (is_a($binaries, Collection::class)) {
            $binaries->each(function($binary) {
                $binary->load('binaries', 'leftBinary', 'rightBinary');
                $this->loadBinaries($binary->binaries);
            });
        }
    }

    protected function configurateBinary($binary) {
        
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}