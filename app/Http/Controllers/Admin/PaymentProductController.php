<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
use Exception;
use App\Models\PaymentProduct;
use App\Models\PaymentProductCategory;
use App\Models\PaymentProductType;
use App\Helpers\Enums\PaymentProductType as PPType;
use App\Helpers\Enums\ActiveStatus;
use App\Helpers\Managers\PaymentProductManager;

class PaymentProductController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.payment-product.index');
    }

    public function edit(Request $request, PaymentProduct $paymentProduct)
    {
        $types = PaymentProductType::pluck('product_code', 'id');
        foreach ($types as $id => $code) {
            $types[$id] = PPType::getString($code);
        }
        $categories = PaymentProductCategory::pluck('category', 'id');

        return view('admin.payment-product.form')
            ->with('types', $types)
            ->with('categories', $categories)
            ->with('paymentProduct', $paymentProduct);
    }

    public function ajax(Request $request)
    {
        $data = PaymentProduct::query();
    	return Datatables::of($data)
            ->editColumn('status', function ($model) {
                return ActiveStatus::getString($model->status);
            })
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.payment-product.edit', ['paymentProduct' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                // $str .= '<a data-url="'. route('admin.payment-product.delete', ['paymentProduct' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#products-table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                $str .= '</div>';
                return $str;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, PaymentProduct $paymentProduct)
    {
        $messages = [
            'data.*.name.required_with' => 'The name field is required ',
            'data.*.value.required_with' => 'The value field is required ',
        ];

        $validatedData = $request->validate([
            'name' => 'required',
            'amount' => 'required|numeric',
            'price' => 'required|numeric',
            'fee' => 'required|numeric',
            'type_id' => 'nullable',
            'category_id' => 'nullable',
            'status' => 'required',
            'data' => 'nullable|array',
            'data.*.name' => 'required_with:data',
            'data.*.value' => 'required_with:data',
        ], $messages);

        try {
            DB::beginTransaction();
            $paymentProductManager = new PaymentProductManager;
            $updatedPaymentProduct = $paymentProductManager->update($paymentProduct, $request->all());
            DB::commit();
            return redirect()->route('admin.payment-product.index');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, PaymentProduct $paymentProduct)
    {
        try {
            DB::beginTransaction();
            if (!$paymentProduct->delete()) throw new Exception('Failed to delete Payment Product', 500);
            DB::commit();

            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(['message' => 'Payment Product Deleted'], 200);
            }
            return redirect()->route('admin.payment-product.index')->with('success', 'Payment Product deleted');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();

            if ($request->ajax() || $request->wantsJson()) {
                return response($errorMessage, 500);
            }
            return redirect()->back()->with('error', $errorMessage);
        }
    }
}