<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Managers\ConfigManager;
use App\Models\Config as AppConfig;
use Yajra\Datatables\Datatables;
use DB;
use Exception;


class ConfigController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.config.index');
    }

    public function edit(Request $request, AppConfig $config)
    {
        return view('admin.config.form')
            ->with('config', $config);
    }

    public function ajax(Request $request)
    {
    	$data = AppConfig::query();
    	return Datatables::of($data)
                ->addColumn('action', function ($model) {
                    $str = '<div class="btn-group btn-group-circle">';
                    $str .= '<a href='. route('admin.config.edit', ['user' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                    $str .= '<a data-url="'. route('admin.config.delete', ['user' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table" data-message="Are you sure want to delete this Config? This may cause a system <b>ERROR</b> or <b>FAILURE</b> towards this app"><span class="far fa-trash-alt"></span></a>';
                    $str .= '</div>';
                    return $str;
                })
                ->escapeColumns([])
				->make(true);
    }

    public function update(Request $request, AppConfig $config)
    {
        $messageType = $config->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'key' => 'required',
            'value' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $configManager = new ConfigManager;
            $updateConfig = $configManager->update($config, $request->all());
            DB::commit();
            return redirect()->route('admin.config.index')->with('success', 'Config successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, AppConfig $config)
    {
        try {
            DB::beginTransaction();
            $configManager = new ConfigManager($request->user());
            $updatedConfig = $configManager->delete($config);
            DB::commit();
            return response()->json(['message' => 'Config Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}
