<?php

namespace App\Http\Controllers\Admin\User;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\User;
use App\Models\BankAccount;
use App\Models\UpgradeRequest;
use App\Helpers\Managers\UpgradeRequestManager;
use App\Helpers\Enums\UpgradeRequestStatus;
use App\Helpers\Enums\PackageType;
use Yajra\Datatables\Datatables;

class UpgradeRequestController extends Controller
{
    public function index(Request $request)
    {
        $statusList = UpgradeRequestStatus::getArray();
        $packageList = PackageType::getArray();
        return view('admin.users.upgrade-request.index')
            ->with([
                'statusList' => $statusList,
                'packageList' => $packageList,
            ]);
    }

    public function edit(Request $request, UpgradeRequest $upgradeRequest)
    {
        $bankAccount = $upgradeRequest->options ? BankAccount::find($upgradeRequest->options->bank_account_id) : null;
        return view('admin.users.upgrade-request.form')
            ->with('bankAccount', $bankAccount)
            ->with('upgradeRequest', $upgradeRequest);
    }

    public function ajax(Request $request)
    {
    	$data = UpgradeRequest::with('requestedBy', 'reviewedBy');
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                } else {
                    $model->active();
                }
                if ($request->has('package_enum') && $request->package_enum !== null) {
                    $model->where('package', $request->package_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.users.upgrade-request.edit', ['upgradeRequest' => $model->id]) .' class="action"><span class="far fa-eye"></span></a>';
                $str .= '<a data-url="'. route('admin.users.upgrade-request.delete', ['upgradeRequest' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->editColumn('status', function ($model) {
                return UpgradeRequestStatus::getString($model->status);
            })
            ->addColumn('package_name', function ($model) {
                return PackageType::getString($model->package);
            })
            ->addColumn('created_at', function ($model) {
                return $model->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i');
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function approve(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            DB::beginTransaction();
            $upgradeRequestManager = new UpgradeRequestManager($request->user());
            $updatedUser = $upgradeRequestManager->approve($upgradeRequest);
            DB::commit();
            return redirect()->route('admin.users.upgrade-request.index')->with('success', 'Upgrade Package approved');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function decline(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            DB::beginTransaction();
            $upgradeRequestManager = new UpgradeRequestManager($request->user());
            $updatedUser = $upgradeRequestManager->decline($upgradeRequest);
            DB::commit();
            return redirect()->route('admin.users.upgrade-request.index')->with('success', 'Upgrade Package declined');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            DB::beginTransaction();
            $upgradeRequestManager = new UpgradeRequestManager($request->user());
            $updatedUser = $upgradeRequestManager->delete($upgradeRequest);
            DB::commit();
            return response()->json(['message' => 'Upgrade Package Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}