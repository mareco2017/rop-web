<?php

namespace App\Http\Controllers\Admin\User;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\User;
use App\Models\Binary;
use App\Helpers\Managers\UserManager;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Validator;

class VerifyIdentityController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.users.verify-identity.index');
    }

    public function edit(Request $request, User $user)
    {
        $binary = Binary::getRoot()->first();
        $rootUser = $binary ? $binary->user : null;

        $binaries = $rootUser ? $rootUser->getBinaryTree() : [];
        
        return view('admin.users.verify-identity.form')
            ->with('binaries', $binaries)
            ->with('user', $user);
    }

    public function ajax(Request $request)
    {
        $data = User::documentPending();
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.users.verify-identity.edit', ['user' => $model->id]) .' class="action"><span class="far fa-eye"></span></a>';
                $str .= '<a data-url="'. route('admin.users.verify-identity.delete', ['user' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function approve(Request $request, User $user)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'nullable',
            'gender' => 'required',
            'dob' => 'required|date',
            'identification_number' => 'required|unique:documents,identification_number,'.$user->id.',ownable_id',
            'leader_id' => 'nullable',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('leader_id', 'required', function($data) {
            $binary = Binary::first();
            return $binary ? true : false;
        });
        $validator->validate();

        try {
            DB::beginTransaction();
            $userManager = new UserManager($user);
            $updatedUser = $userManager->approveIdentity($request->all(), $request->user());
            DB::commit();
            return redirect()->route('admin.users.verify-identity.index')->with('success', 'User identification approved');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function decline(Request $request, User $user)
    {
        try {
            DB::beginTransaction();
            $userManager = new UserManager($user);
            $updatedUser = $userManager->declineIdentity($request->user());
            DB::commit();
            return redirect()->route('admin.users.verify-identity.index')->with('success', 'User identification rejected');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            DB::beginTransaction();
            $upgradeRequestManager = new UpgradeRequestManager($request->user());
            $updatedUser = $upgradeRequestManager->delete($upgradeRequest);
            DB::commit();
            return response()->json(['message' => 'Upgrade Package Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}