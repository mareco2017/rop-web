<?php

namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Order;
use App\Helpers\Enums\OrderType;
use Yajra\Datatables\Datatables;
use Validator;

class WithdrawCOntroller extends Controller
{
    public function index(Request $request)
    {
        return view('admin.withdraw.index');
    }

    public function edit(Request $request, Order $order)
    {
        return view('admin.withdraw.form')
            ->with('order', $order);
    }

    public function ajax(Request $request)
    {
    	$data = Order::type(OrderType::WITHDRAW);
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.withdraw.edit', ['order' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.withdraw.delete', ['order' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->escapeColumns([])
            ->make(true);
    }

    // public function update(Request $request, Order $order)
    // {
    //     $messageType = $order->id ? 'updated' : 'created';
        
    //     $rules = [
    //         'name' => 'required',
    //         'code' => 'required',
    //         'abbr' => 'required',
    //         'logo' => 'nullable'
    //     ];
    //     $validator = Validator::make($request->all(), $rules);
    //     $validator->sometimes('logo', 'required', function($data) use ($order) {
    //         return !$order->id;
    //     });
    //     $validator->validate();
    //     try {
    //         DB::beginTransaction();
    //         $bankManager = new BankManager;
    //         $updatedBank = $bankManager->update($bank, $request->all());
    //         DB::commit();
    //         return redirect()->route('admin.withdraw.index')->with('success', 'Order successfully '.$messageType);
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         $errorMessage = $e->getMessage();
    //         return redirect()->back()->with('error', $errorMessage);
    //     }
    // }

    // public function delete(Request $request, Bank $bank)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $bankManager = new BankManager;
    //         $updatedBank = $bankManager->delete($bank);
    //         DB::commit();
    //         return response()->json(['message' => 'Bank Deleted'], 200);
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         $errorMessage = $e->getMessage();
    //         return response()->json(['message' => $errorMessage], 500);
    //     }
    // }
}