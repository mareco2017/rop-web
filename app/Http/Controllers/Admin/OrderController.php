<?php

namespace App\Http\Controllers\Admin;
 
use App\Helpers\Enums\NotificationSubType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderType;
use App\Helpers\Managers\ActionManager;
use App\Helpers\Managers\NotificationManager;
use App\Helpers\Managers\TopupManager;
use App\Helpers\Managers\OrderManager;
use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\BankAccount;
use App\Models\Order;
use App\Models\User;
use DB;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;
use GlobalHelper;

class OrderController extends Controller
{
    protected $filename = '/json/reject-reason.json';

    public function index(Request $request)
    {
        $statusList = OrderStatus::getArray();
        return view('admin.order.index')
                ->with('statusList', $statusList);
    }

    public function ajax(Request $request)
    {
    	$data = Order::orderBy('created_at','desc');
    	return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $query->where('status', $request->status_enum);
                }
                if ($request->has('date') && $request->date !== null) {
                    $query->whereDate('created_at', $request->date);
                }
                if ($request->has('user_id') && $request->user_id != null) {
                    $type = (new User)->getMorphClass();
                    $query->where('ownable_type', $type);
                    $query->where('ownable_id', $request->user_id);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a data-url="'. route('admin.order.refund', ['order' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-message="Are you sure want to refund this order?" data-method="POST" data-table-name="#table"><i class="fas fa-undo"></i></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('status', function ($model) {
                return OrderStatus::getString($model->status);
            })
            ->addColumn('type', function ($model) {
                return OrderType::getString($model->type);
            })
            ->addColumn('amount', function ($model) {
                $amount = GlobalHelper::toCurrency($model->total);
                return $amount;
            })
            ->addColumn('created_by', function ($model) {
                $owner = $model->ownable;
                return $owner ? ($owner instanceof Business) ? "{$owner->name} (Business)" : $owner->fullname : 'Unknown';
            })
            ->addColumn('created_at', function ($model) {
                return $model->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i');
            })
            ->addColumn('options', function ($model) {
                $sn = '-';
                if (isset($model->options->phone_number)) $sn = $model->options->phone_number;
                return $sn;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function refund(Request $request, Order $order)
    {
        try {
            // Check if action exists
            $user = $request->user();
            $cAction = Action::active()->object($order)->latest()->first();
            if ($cAction) {
                throw new Exception("Someone is accessing this order!", 400);
            }
            $actionManager = new ActionManager($user);
            $action = $actionManager->create($order, "refund");

            DB::beginTransaction();
            $orderManager = new OrderManager($user);
            $orderManager->forceRefund($order);
            
            DB::commit();

            // Set action to inactive > success
            $actionManager->success($action);
            
            if ($request->ajax() || $request->wantsJson()) {
                return response(OrderType::getString($order->type) .' has been refunded!', 200);
            }
            return redirect()->back()->with('success', OrderType::getString($order->type) .' has been refunded!');
        } catch (Exception $e){
            DB::rollback();
            $errorMessage = $e->getMessage();
            // Set action to inactive > error
            if (isset($action)) {
                $actionManager->error($action, $errorMessage);
            }
            if ($request->ajax() || $request->wantsJson()) {
                return response($errorMessage, 500);
            }
            return redirect()->back()->with('error', $errorMessage);
        }
    }
}