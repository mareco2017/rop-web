<?php

namespace App\Http\Controllers\Admin;
 
use App\Helpers\Enums\NotificationSubType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\PackageType;
use App\Helpers\GlobalHelper;
use App\Helpers\Managers\NotificationManager;
use App\Helpers\Managers\PaymentManager;
use App\Http\Controllers\Controller;
use App\Models\BankAccount;
use App\Models\Order;
use App\Models\Pair;
use App\Models\Payment;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.payment.index');
    }

    public function edit(Request $request, Payment $payment)
    {
        // $pairList = Pair::join('users AS leader','pairs.leader_user_id','=','leader.id')
        // ->join('users AS left', 'pairs.left_binary_user_id','=', 'left.id')
        // ->join('users AS right', 'pairs.right_binary_user_id','=', 'right.id')
        // ->whereNull('pairs.claimed_at')
        // ->select('pairs.id', 'pairs.leader_user_id', 'pairs.left_binary_user_id', 'pairs.right_binary_user_id', DB::raw('CONCAT("Leader : ", leader.first_name," Left : ", left.first_name, " ", left.last_name, ", Right : ", right.first_name, " ", right.last_name) AS leaderList'))
        // ->orderBy('leader.first_name', 'asc')
        // ->pluck('leaderList', 'pairs.id');
        
        $pairList = Order::join('users AS leader', 'orders.ownable_id','=', 'leader.id')
        ->where('orders.type', OrderType::PAIRING_BONUS)
        ->where('orders.status', OrderStatus::COMPLETED)
        ->whereNull('orders.claimed_at')
        ->select('orders.id', 'orders.reference_number', 'orders.type', 'orders.status', 'leader.first_name', 'leader.last_name', DB::raw('CONCAT(orders.reference_number, " - ", leader.first_name," - ", leader.id, " - ", leader.referral_code) AS pairList'))
        ->orderBy('leader.first_name', 'asc')
        ->pluck('pairList', 'orders.id');

        $orderList = Order::join('users AS leader', 'orders.ownable_id','=', 'leader.id')
        ->where('orders.type', OrderType::SPONSOR_BONUS)
        ->where('orders.status', OrderStatus::COMPLETED)
        ->whereNull('orders.claimed_at')
        ->select('orders.id', 'orders.reference_number', 'orders.type', 'orders.status', 'leader.first_name', 'leader.last_name', DB::raw('CONCAT(orders.reference_number, " - ", leader.first_name," - ", leader.id, " - ", leader.referral_code) AS sponsorList'))
        ->orderBy('leader.first_name', 'asc')
        ->pluck('sponsorList', 'orders.id');

        $bankAccountList = BankAccount::join('banks','bank_accounts.bank_id','=', 'banks.id')
        ->where('bank_accounts.ownable_type', '=', 'users')
        ->whereNull('bank_accounts.deleted_at')
        ->select('bank_accounts.id', 'bank_accounts.account_name', 'bank_accounts.account_no' ,'banks.abbr', DB::raw('CONCAT(bank_accounts.account_name, " | ",banks.abbr, " ",bank_accounts.account_no) AS bankAccList'))
        ->orderBy('bank_accounts.account_name', 'asc')
        ->pluck('bankAccList', 'bank_accounts.id');

        return view('admin.payment.form')
            ->with('pairList', $pairList)
            ->with('bankAccountList', $bankAccountList)
            ->with('orderList', $orderList)
            ->with('payment', $payment);
    }

    public function ajax(Request $request)
    {
    	$data = Payment::whereNull('claimed_at');
    	return Datatables::of($data)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.payment.confirmation', ['payment' => $model->id]) .' class="action"><span class="far fa-eye"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('bank_account_id', function ($model) {
                $str = $model->bankAccount->account_name.' | '.$model->bankAccount->bank->abbr.' '.$model->bankAccount->account_no;
                return $str;
            })
            ->addColumn('amount', function ($model) {
                $str = $model->amount;
                return GlobalHelper::toCurrency($str);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Payment $payment)
    {
        $messageType = $payment->id ? 'updated' : 'created';
        try {
            DB::beginTransaction();
            $paymentManager = new PaymentManager($request->user());
            $updatedPayment = $paymentManager->update($payment, $request->all());
            DB::commit();
            return redirect()->route('admin.payment.index')->with('success', 'Payment successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function confirmation(Request $request, Payment $payment)
    {
        $totalBonusPair = 0;
        $totalBonusSponsor = 0;
        $pairIds = json_decode($payment->options)->pair_id;
        $orderIds = json_decode($payment->options)->order_id;
        if ($pairIds) {
            foreach ($pairIds as $key => $pair) {
                $totalPair = Order::find($pair);
                if (count($totalPair->orderDetails) != 0) $totalBonusPair += $totalPair->orderDetails[0]->price;
            }
        }
        if ($orderIds) {
            foreach ($orderIds as $key => $order) {
                $totalOrder = Order::find($order);
                if (count($totalOrder->orderDetails) != 0) $totalBonusSponsor += $totalOrder->orderDetails[0]->price;
            }
        }
       return view('admin.payment.confirmation')
       ->with('totalBonusPair', $totalBonusPair)
       ->with('totalBonusSponsor', $totalBonusSponsor)
       ->with('payment', $payment);
    }

    public function sendEmail(Request $request, Payment $payment)
    {
        $payment->claimed_at = Carbon::now();
        $payment->save();

        $totalBonusPair = 0;
        $totalBonusSponsor = 0;
        $pairIds = json_decode($payment->options)->pair_id;
        $orderIds = json_decode($payment->options)->order_id;
        if ($pairIds) {
            foreach ($pairIds as $key => $pair) {
                $totalPair = Order::find($pair);
                $totalPair->claimed_at = Carbon::now();
                $totalPair->save();
                if (count($totalPair->orderDetails) != 0) $totalBonusPair += $totalPair->orderDetails[0]->price;
            }
        }

        if ($orderIds) {
            foreach ($orderIds as $key => $order) {
                $totalOrder = Order::find($order);
                $totalOrder->claimed_at = Carbon::now();
                $totalOrder->save();
                if (count($totalOrder->orderDetails) != 0) $totalBonusSponsor += $totalOrder->orderDetails[0]->price;
            }
        }

        $notificationType = NotificationType::STATEMENT;
        $notificationSubType = NotificationSubType::BANK_TRANSFER;
        $notificationManager = new NotificationManager($request->user());
        $bank = $payment->bankAccount->bank->abbr;
        $name = $payment->bankAccount->ownable->first_name.' '.$payment->bankAccount->ownable->last_name;
        $referralCode = $payment->bankAccount->ownable->referral_code;
        $packages = $payment->bankAccount->ownable->packages;
        $joined_at = $payment->bankAccount->ownable->created_at;
        $totalLeftBinary = $payment->bankAccount->ownable->total_left_binary_user;
        $totalRightBinary = $payment->bankAccount->ownable->total_right_binary_user;
        $totalLeftPoin = $payment->bankAccount->ownable->total_left_binary_point;
        $totalRightPoin = $payment->bankAccount->ownable->total_right_binary_point;
        $accountNo = $payment->bankAccount->account_no;
        $accountName = $payment->bankAccount->account_name;
        $created_at = $payment->created_at->setTimezone('Asia/Jakarta')->format('F Y');
        $startOfWeek = Carbon::today()->startOfWeek()->subDays(7)->format('d-M-y');
        $endOfPeriod = $payment->created_at->setTimezone('Asia/Jakarta')->format('d-M-y');
        if ($payment->bankAccount->ownable->sponsor) {
            $sponsor = $payment->bankAccount->ownable->sponsor->first_name.' '.$payment->bankAccount->ownable->sponsor->last_name;
        } else {
            $sponsor = null;
        }
        $notificationData = [
            'category' => NotificationType::getCategory($notificationType),
            'bank_destination' => $payment->bankAccount->id,
            'reference_number' => $payment->statement_no,
            'order_id' => $orderIds,
            'amount' => $payment->amount,
            'totalBonusPair' => $totalBonusPair,
            'totalBonusSponsor' => $totalBonusSponsor,
            'name' => $name,
            'sponsor' => $sponsor,
            'referralCode' => $referralCode,
            'packages' => PackageType::getString($packages),
            'payment' => $payment,
            'joined_at' => $joined_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i'),
            'totalLeftBinary' => $totalLeftBinary,
            'totalRightBinary' => $totalRightBinary,
            'totalLeftPoin' => $totalLeftPoin,
            'totalRightPoin' => $totalRightPoin,
            'bank' => $bank,
            'accountNo' => $accountNo,
            'accountName' => $accountName,
            'created_at' => $created_at,
            'startOfWeek' => $startOfWeek,
            'endOfPeriod' => $endOfPeriod,

        ];
        if (!$orderIds) {
            $receiver = $totalPair->ownable;
        } else {
            $receiver = $totalOrder->ownable;
        }
        $notification = $notificationManager->setType($notificationType)->update($receiver, $notificationData, $notificationSubType);
        return redirect()->route('admin.payment.index')->with('success', 'Email Successfully Send');

    }
}