<?php

namespace App\Http\Controllers\Admin;
 
use App\Helpers\Enums\NotificationSubType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderType;
use App\Helpers\Managers\ActionManager;
use App\Helpers\Managers\NotificationManager;
use App\Helpers\Managers\TopupManager;
use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\BankAccount;
use App\Models\Order;
use DB;
use Exception;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Datatables;

class TopUpController extends Controller
{
    protected $filename = '/json/reject-reason.json';

    public function index(Request $request)
    {
        $statusList = OrderStatus::getArray();
        $floatingWallet = Order::join('order_details','orders.id','=','order_details.order_id')->where('orders.type', OrderType::TOPUP)->where('orders.status', OrderStatus::COMPLETED)->sum('order_details.total');
        return view('admin.top-up.index')
        ->with('floatingWallet', $floatingWallet)
            ->with('statusList', $statusList);
    }

    public function edit(Request $request, Order $order)
    {
        return view('admin.top-up.form')
            ->with('order', $order);
    }

    public function ajax(Request $request)
    {
    	$data = Order::type(OrderType::TOPUP)->orderBy('created_at','desc');
    	return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $query->where('status', $request->status_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.top-up.confirmation', ['order' => $model]) .' class="action"><span class="far fa-eye"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('status', function ($model) {
                return OrderStatus::getString($model->status);
            })
            ->addColumn('created_by', function ($model) {
                $owner = $model->ownable;
                return $owner ? ($owner instanceof Business) ? "{$owner->name} (Business)" : $owner->fullname : 'Unknown';
            })
            ->addColumn('created_at', function ($model) {
                return $model->created_at->setTimezone('Asia/Jakarta')->format('d-M-y H:i');
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function confirmation(Request $request, Order $order)
    {
        $orderDetails = $order->orderDetails;
        $destBankAccount = $order->virtual && isset($order->virtual->dest_bank_account) ? $order->virtual->dest_bank_account : null;
        $destBank = $destBankAccount ? $destBankAccount->bank : null;
        $path = storage_path().$this->filename;
        $choices = null;
        if (file_exists($path)) {
            $choices = json_decode(file_get_contents($path), true);
        }

        return view('admin.top-up.confirmation')
                ->with('order', $order)
                ->with('ownable', $order->ownable)
                ->with('orderDetails', $orderDetails)
                ->with('destBankAccount', $destBankAccount)
                ->with('destBank', $destBank)
                ->with('choices', $choices);
    }

    public function approve(Request $request, Order $order)
    {
        try {
            // Check if action exists
            $user = $request->user();
            $cAction = Action::active()->object($order)->latest()->first();
            if ($cAction) {
                throw new Exception("Someone is accessing this order!", 400);
            }
            $actionManager = new ActionManager($user);
            $action = $actionManager->create($order, "approve");

            DB::beginTransaction();
            $topupManager = new TopupManager($user);
            if ($order->type == OrderType::TOPUP) {
                $approveOrder = $topupManager->setOrder($order)->approve();
                $notificationType = NotificationType::TOPUP;
                $notificationSubType = NotificationSubType::BANK_TRANSFER;
                $notificationManager = new NotificationManager($user);
                $bankAccount = BankAccount::find($approveOrder->options->approvable_id)->with('bank')->first();
                $bank = $bankAccount->bank->abbr;
                $notificationData = [
                    'category' => NotificationType::getCategory($notificationType),
                    'bank_destination' => $bank,
                    'reference_number' => $approveOrder->reference_number,
                    'order_id' => $approveOrder->id,
                    'amount' => $approveOrder->total_before_discount
                ];
                $notification = $notificationManager->setType($notificationType)->update($approveOrder->ownable, $notificationData, $notificationSubType);
            } else if ($order->type == OrderType::TOPUP_CREDIT) {
                $approveOrder = $topupManager->setOrder($order)->approveCredit();

                $notificationType = NotificationType::TOPUP_CREDIT;
                $notificationSubType = null;

            }
            
            DB::commit();

            // Set action to inactive > success
            $actionManager->success($action);
            
            if ($request->ajax() || $request->wantsJson()) {
                return response(OrderType::getString($order->type) .' has been approved!', 200);
            }
            return redirect()->route('admin.top-up.index')->with('success', OrderType::getString($order->type) .' has been approved!');
        } catch (Exception $e){
            DB::rollback();
            $errorMessage = $e->getMessage();
            // Set action to inactive > error
            if (isset($action)) {
                $actionManager->error($action, $errorMessage);
            }
            if ($request->ajax() || $request->wantsJson()) {
                return response($errorMessage, 500);
            }
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function decline(Request $request, Order $order)
    {
        try {
            // Check if action exists
            $user = $request->user();
            $cAction = Action::active()->object($order)->latest()->first();
            if ($cAction) {
                throw new Exception("Someone is accessing this order!", 400);
            }
            $actionManager = new ActionManager($user);
            $action = $actionManager->create($order, "decline");
            DB::beginTransaction();
            $topupManager = new TopupManager($user);
            $approveOrder = $topupManager->setOrder($order)->decline();
            $notificationManager = new NotificationManager($user);
            $bankAccount = BankAccount::find($approveOrder->options->dest_bank_account_id)->with('bank')->first();
            $bank = $bankAccount->bank->abbr;
            $msg = $request->msg;
            $notificationData = [
                'category' => NotificationType::getCategory(NotificationType::TOPUP_DECLINED),
                'message' => $msg,
                'bank_destination' => $bank,
                'reference_number' => $approveOrder->reference_number,
                'amount' => $approveOrder->total_before_discount
            ];
            $notification = $notificationManager->setType(NotificationType::TOPUP_DECLINED)->update($approveOrder->ownable, $notificationData, NotificationSubType::BANK_TRANSFER);
            DB::commit();

            // Set action to inactive > success
            $actionManager->success($action);

            if ($request->ajax() || $request->wantsJson()) {
                return response(OrderType::getString($order->type) .' has been declined!', 200);
            }
            return redirect()->route('admin.top-up.index');
        } catch (Exception $e){
            DB::rollback();
            $errorMessage = $e->getMessage();
            // Set action to inactive > error
            if (isset($action)) {
                $actionManager->error($action, $errorMessage);
            }
            if ($request->ajax() || $request->wantsJson()) {
                return response($errorMessage, 500);
            }
            return redirect()->back()->with('error', $errorMessage);
        }
    }
    // public function update(Request $request, Order $order)
    // {
    //     $messageType = $order->id ? 'updated' : 'created';
        
    //     $rules = [
    //         'name' => 'required',
    //         'code' => 'required',
    //         'abbr' => 'required',
    //         'logo' => 'nullable'
    //     ];
    //     $validator = Validator::make($request->all(), $rules);
    //     $validator->sometimes('logo', 'required', function($data) use ($order) {
    //         return !$order->id;
    //     });
    //     $validator->validate();
    //     try {
    //         DB::beginTransaction();
    //         $bankManager = new BankManager;
    //         $updatedBank = $bankManager->update($bank, $request->all());
    //         DB::commit();
    //         return redirect()->route('admin.top-up.index')->with('success', 'Order successfully '.$messageType);
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         $errorMessage = $e->getMessage();
    //         return redirect()->back()->with('error', $errorMessage);
    //     }
    // }

    // public function delete(Request $request, Bank $bank)
    // {
    //     try {
    //         DB::beginTransaction();
    //         $bankManager = new BankManager;
    //         $updatedBank = $bankManager->delete($bank);
    //         DB::commit();
    //         return response()->json(['message' => 'Bank Deleted'], 200);
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         $errorMessage = $e->getMessage();
    //         return response()->json(['message' => $errorMessage], 500);
    //     }
    // }
}