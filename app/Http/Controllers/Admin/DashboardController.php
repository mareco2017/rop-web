<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Admin;
use App\Models\Binary;
use Illuminate\Support\Collection;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $binary = Binary::getRoot()->first();
        $rootUser = $binary ? $binary->user : null;

        $binaries = $rootUser ? $rootUser->getBinaryTree() : [];
        
        $totalBinary = count($binaries);
        return view('admin.dashboard')
            ->with('binaries', $binaries)
            ->with('totalBinary', $totalBinary);
    }
}