<?php

namespace App\Http\Controllers\Admin\Promo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Exception;
use App\Models\Promo;
use App\Helpers\Enums\ActiveStatus;
use App\Helpers\Enums\PromoCategory;
use Carbon\Carbon;
use App\Helpers\Managers\PromoManager;
use Validator;
use Yajra\Datatables\Datatables;

class BannerController extends Controller
{
    public function index(Request $request) 
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.promos.banner.index')
            ->with('statusList', $statusList);
    }

    public function ajax(Request $request) 
    {
        $data = Promo::where('deleted_at', NULL);
        return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $query->where('status', $request->status_enum);
                }
            }, true)
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->addColumn('start_end_date', function ($model) {
                $str = $model->start_date->format('j F Y').' / '.$model->end_date->format('j F Y');
                return $str;
            })
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.promos.banner.edit', ['promo' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.promos.banner.delete', ['promo' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->editColumn('status', function($model) {
                return ActiveStatus::getString($model->status);
            })
            ->make(true);
    }

    public function edit(Request $request, Promo $promo) 
    {
        // $ownerList = Business::pluck('name', 'id');
        $statusList = ActiveStatus::getArray();
        $today = Carbon::now();

        // $livePromos = Promo::where([
        //     ['start_date', '<=', $today],
        //     ['end_date', '>=', $today],
        //     ['status', ActiveStatus::ACTIVE],
        // ])->orderBy('urut', 'desc')->get();

        // $incomingPromos = Promo::where([
        //     ['start_date', '>', $today],
        //     ['status', ActiveStatus::ACTIVE],
        // ])->orderBy('urut', 'desc')->get();
        
        return view('admin.promos.banner.form')
            ->with('today', $today)
            // ->with('livePromos', $livePromos)
            // ->with('incomingPromos', $incomingPromos)
            ->with('promo', $promo)
            // ->with('ownerList', $ownerList)
            ->with('statusList', $statusList);
    }

    public function update(Request $request, Promo $promo) 
    {
        $messageType = $promo->id ? 'updated' : 'created';

        $rules = [
            'title' => 'required',
            'status' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'description' => 'nullable',
            'cover' => 'file',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('cover', 'required', function($data) use ($promo) {
            return !$promo->id;
        });
        $validator->validate();

        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['category'] = PromoCategory::BANNER;

            $promoManager = new PromoManager($request->user());
            if (!$promo->id) {
                $promo = $promoManager->generateUrut($promo, $request->urut);
            }
            $updatedPromo = $promoManager->update($promo, $data);

            DB::commit();
            return redirect()->route('admin.promos.banner.index')->with('success', 'Promo successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function deletePromo(Request $request, Promo $promo) {
        try {
            DB::beginTransaction();
            $promoManager = new PromoManager($request->user());
            $updatedPromo = $promoManager->delete($promo);
            DB::commit();
            return response()->json(['message' => 'Promo Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }

    public function pushNotif(Request $request, Promo $promo)
    {
        return view('backend.promos.banner.push-notif')
            ->with('promo', $promo);
    }

    public function postPushNotif(Request $request, Promo $promo)
    {
        $data = [
            'title' => $request->title,
            'message' => $request->message
        ];
        SendNotifications::dispatch($data);
        return redirect()->route('admin.promos.banner.index')->with('success', 'Notification Sent');

    }
}