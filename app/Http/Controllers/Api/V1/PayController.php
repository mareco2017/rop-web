<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Carbon\Carbon;
use App\Traits\ApiResponse;
use App\Helpers\Enums\WalletType;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Managers\UserManager;
use App\Helpers\Managers\TransactionManager;
use App\Helpers\Managers\NotificationManager;
use App\Models\User;

class PayController extends Controller
{
    use ApiResponse;

    public function payByPhoneNumber(Request $request, $phoneNumber)
    {
        $validatedData = $request->validate([
            'pin' => 'required|numeric',
            'amount' => 'required|numeric',
            'note' => 'nullable|string'
        ]);

        try {
            $user = $request->user();
            $amount = $request->amount;
            $receiver = User::where('phone_number', $phoneNumber)->first();
            if (!$receiver) throw new Exception("Unknown receiver!", 404);
            if ($user->is($receiver)) throw new Exception("Invalid receiver!", 400);
            if ($amount < 500) throw new Exception("Payment price / total must be at least Rp500", 406);
            
            $userManager = new UserManager($user);
            $credential = $userManager->checkPin($request->pin);
            $discountAmount = 0;
            
            $orderData = [
                'options' => [
                    'note' => $request->note ? $request->note : null,
                    'title' => $request->title ? $request->title : null,
                    // 'hide_name' => $request->hide_name
                ],
                'products' => [
                    (object) [
                        'qty' => 1,
                        'price' => $amount,
                        'discount' => $discountAmount
                    ]
                ]
            ];

            $orderManager = new OrderManager($user);
            $order = $orderManager->create($receiver, OrderType::PAY, $orderData);
            // $order = $orderManager->create($receiver, OrderType::TRANSFER, $orderData);

            $debitWallet = $receiver->getWallet(WalletType::PRIMARY);
            $creditWallet = $order->ownable ? $order->ownable->getWallet(WalletType::PRIMARY) : null;

            if ($amount > $creditWallet->balance) throw new Exception('Failed to pay, insufficient balance!', 400);
            
            $transactionManager = new TransactionManager($user);
            $transaction = $transactionManager->setOrder($order)->create($debitWallet, $creditWallet, $amount, TransactionType::PAY);
            // $transaction = $transactionManager->setOrder($order)->create($debitWallet, $creditWallet, $amount, TransactionType::TRANSFER);
            $receipt = ['reference_number' => $transaction->reference_number, 'time' => $transaction->created_at->format('Y-m-d H:i:s'), 'recipient' => $receiver->fullname, 'receiver_phone' => $receiver->phone_number,'amount' => $amount];
            DB::commit();
            
            $notificationManager = new NotificationManager($user);
            $notificationType = NotificationType::PAY;
            // $notificationType = NotificationType::TRANSFER;
            $notificationData = [
                'category' => NotificationType::getCategory($notificationType),
                'reference_number' => $transaction->reference_number,
                'amount' => $amount
            ];
            $notification = $notificationManager->setType($notificationType)->update($receiver, $notificationData);
            return $this->jsonResponse('Success', ['order' => $order->fresh()], 200);
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e->getMessage());
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}
