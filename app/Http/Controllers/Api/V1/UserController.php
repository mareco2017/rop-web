<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\User;
use App\Models\UpgradeRequest;
use App\Helpers\Enums\VerificationType;
use App\Helpers\Enums\VerificationStatus;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\UpgradeRequestStatus;
use App\Helpers\Enums\DocumentType;
use App\Helpers\Managers\DocumentManager;
use App\Helpers\Managers\OTPManager;
use App\Helpers\Managers\VerificationManager;
use App\Helpers\Managers\WalletManager;
use App\Helpers\Managers\UserManager;
use App\Helpers\UniqueHelper;

class UserController extends Controller
{
    use ApiResponse;

    public function profile(Request $request)
    {
        $user = $this->guard()->user();
        $wallets = $user->wallets;

        return $this->jsonResponse("Success", array('user' => $user, 'wallets' => $wallets));
    }

    public function getPoint(Request $request)
    {
        $user = $this->guard()->user();

        return $this->jsonResponse("Success", array('left_binary_point' => $user->total_left_binary_point, 'right_binary_point' => $user->total_right_binary_point));
    }

    public function checkPassword(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required'
        ]);

        try {
            $user = $this->guard()->user();

            if (!Hash::check($request->password, $user->password)) {
                throw new Exception("Invalid password", 400);
            }

            return $this->jsonResponse("Success", array());
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required',
            'new_password' => 'required|min:6|confirmed'
        ]);

        try {
            $user = $this->guard()->user();

            if (!Hash::check($request->password, $user->password)) {
                throw new Exception("Invalid password", 400);
            }

            $user->password = Hash::make($request->new_password);

            if (!$user->save()) {
                throw new Exception(__('error/messages.failed_update_user'), 500);
            }

            return $this->jsonResponse("Success", array('user' => $user));
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function changeEmail(Request $request)
    {
        $validatedData = $request->validate([
            'new_email' => 'required|email|unique:users,email',
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($request->new_email, 'change_email');

            DB::commit();
            return $this->jsonResponse("Success", array());
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function changePhoneNumber(Request $request)
    {
        $validatedData = $request->validate([
            'new_phone_number' => 'required|phone:ID|unique:users,phone_number',
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($request->new_phone_number, 'change_phone_number');

            DB::commit();
            return $this->jsonResponse("Success", array());
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function updateEmail(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|unique:users,email',
            'pin' => 'required|numeric|min:4'
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->verifyOTP('email', $request->email, $request->pin, 'change_email');

            $user->email = $request->email;

            if (!$user->save()) {
                throw new Exception(__('error/messages.failed_update_user'), 500);
            }

            $verificationManager = new VerificationManager($user);
            $verify = $verificationManager->verify(VerificationType::EMAIL, $otp);

            DB::commit();
            return $this->jsonResponse("Success", array('user' => $user));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function updatePhoneNumber(Request $request)
    {
        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number',
            'pin' => 'required|numeric|min:4'
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->verifyOTP('phone_number', $request->phone_number, $request->pin, 'change_phone_number');

            $user->phone_number = $request->phone_number;

            if (!$user->save()) {
                throw new Exception(__('error/messages.failed_update_user'), 500);
            }

            $verificationManager = new VerificationManager($user);
            $verify = $verificationManager->verify(VerificationType::PHONE_NUMBER, $otp);

            DB::commit();
            return $this->jsonResponse("Success", array('user' => $user));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function verifyPhoneNumber(Request $request)
    {
        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number',
            'pin' => 'required|numeric|min:4'
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->verifyOTP('phone_number', $request->phone_number, $request->pin, 'verify_phone_number');

            $verificationManager = new VerificationManager($user);
            $verify = $verificationManager->verify(VerificationType::PHONE_NUMBER, $otp);

            DB::commit();
            return $this->jsonResponse("Success", array('user' => $user));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function resendOTP(Request $request)
    {
        $validatedData = $request->validate([
            'usage' => 'required|string'
        ]);

        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($user->phone_number, $request->usage);

            DB::commit();
            return $this->jsonResponse("Success", array());
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function binaries(Request $request)
    {
        $user = $this->guard()->user();
        $user->load('binaries', 'leftBinary', 'rightBinary');
        $binaries = $user->binaries;
        $this->loadBinaries($binaries);
        // if ($binaries) $binaries->load('binaries');
        return $this->jsonResponse("Success", array('binary' => $user));
    }

    public function historyPackage(Request $request)
    {
        $user = $this->guard()->user();
        $histories = $user->upgradeRequests()->orderBy('created_at', 'DESC')->get();
        return $this->jsonResponse('Success', array('upgrade_requests' => $histories));
    }

    public function upgradePackage(Request $request)
    {
        $validatedData = $request->validate([
            'package' => 'required|in:' . implode(',', PackageType::getList())
        ]);

        try {
            $user = $this->guard()->user();

            if ($user->verifications_status['nric'] != VerificationStatus::VERIFIED) {
                throw new Exception("Please upload and verify your NRIC first!", 400);
            }

            if ($user->upgradeRequests()->pending()->exists()) {
                throw new Exception("You still have pending upgrade package request", 400);
            }

            DB::beginTransaction();
            $upgradeRequest = new UpgradeRequest;
            $upgradeRequest->package = $request->package;
            if ($user->packages == PackageType::FREE) $upgradeRequest->price = $this->generateUniquePrice(PackageType::getPrice($request->package), 3);
            if ($user->packages == PackageType::SILVER) {
                if ($request->package == PackageType::GOLD) $upgradeRequest->price = $this->generateUniquePrice(PackageType::getPrice(PackageType::GOLD)-PackageType::getPrice(PackageType::SILVER), 3);
                if ($request->package == PackageType::PLATINUM) $upgradeRequest->price = $this->generateUniquePrice(PackageType::getPrice(PackageType::PLATINUM)-PackageType::getPrice(PackageType::SILVER), 3);
            }
            if ($user->packages == PackageType::GOLD) {
                if ($request->package == PackageType::PLATINUM) $upgradeRequest->price = $this->generateUniquePrice(PackageType::getPrice(PackageType::PLATINUM)-PackageType::getPrice(PackageType::GOLD), 3);
            }
            $upgradeRequest->requestedBy()->associate($user);
            $upgradeRequest->expired_at = Carbon::now()->addHours(2);

            if (!$upgradeRequest->save()) {
                throw new Exception("Failed while saving upgrade request!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Your request has been submitted, please wait for it to be processed.", array('upgrade_request' => $upgradeRequest));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function payUpgradePackage(Request $request, UpgradeRequest $upgradeRequest)
    {
        $validatedData = $request->validate([
            'bank_account_id' => 'required|exists:bank_accounts,id'
        ]);

        try {
            $user = $this->guard()->user();

            if ($upgradeRequest->status !== UpgradeRequestStatus::PENDING) {
                throw new Exception("Upgrade request is not active anymore!", 400);
            }

            DB::beginTransaction();

            // $upgradeRequest->expired_at = Carbon::now()->addHours(2);
            $upgradeRequest->status = UpgradeRequestStatus::WAITING_FOR_PAYMENT;
            $upgradeRequest->options = [
                'bank_account_id' => $request->bank_account_id
            ];

            if (!$upgradeRequest->save()) {
                throw new Exception("Failed while saving upgrade request!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Success.", array('upgrade_request' => $upgradeRequest));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function confirmPaymentUpgradePackage(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            $user = $this->guard()->user();

            if ($upgradeRequest->status !== UpgradeRequestStatus::WAITING_FOR_PAYMENT) {
                throw new Exception("Upgrade request is not active anymore!", 400);
            }

            DB::beginTransaction();

            $upgradeRequest->status = UpgradeRequestStatus::ON_PROCESS;

            if (!$upgradeRequest->save()) {
                throw new Exception("Failed while saving upgrade request!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Success.", array('upgrade_request' => $upgradeRequest));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function cancelUpgradePackage(Request $request, UpgradeRequest $upgradeRequest)
    {
        try {
            $user = $this->guard()->user();

            if ($upgradeRequest->status == UpgradeRequestStatus::ON_PROCESS || $upgradeRequest->status == UpgradeRequestStatus::CANCELLED) {
                throw new Exception("Upgrade request is not active anymore!", 400);
            }

            DB::beginTransaction();
            
            $upgradeRequest->status = UpgradeRequestStatus::CANCELLED;

            if (!$upgradeRequest->save()) {
                throw new Exception("Failed while saving upgrade request!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Success.", array('upgrade_request' => $upgradeRequest));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function uploadIdentity(Request $request)
    {
        $validatedData = $request->validate([
            'nric_picture' => 'required|image',
            'selfie_picture' => 'required|image'
        ]);

        try {
            $user = $request->user();

            $activeNRIC = $user->documents()->type(DocumentType::NRIC)->first();
            if ($activeNRIC && !$activeNRIC->is_declined) throw new Exception("You have uploaded your NRIC picture!", 400);

            $activeSelfie = $user->documents()->type(DocumentType::SELFIE)->first();
            if ($activeSelfie && !$activeSelfie->is_declined) throw new Exception("You have uploaded your Selfie picture!", 400);

            DB::beginTransaction();
            $documentManager = new DocumentManager($user);
            $nricData = [
                'attachment' => $request->nric_picture,
                'description' => ''
            ];

            $nric = $documentManager->create(DocumentType::NRIC, $nricData);
            $nric->setRelations([]);

            $selfieData = [
                'attachment' => $request->selfie_picture,
                'description' => ''
            ];

            $selfie = $documentManager->create(DocumentType::SELFIE, $selfieData);
            $selfie->setRelations([]);
            // $history = HistoryLogger::create($order, $user, $order->status);
            DB::commit();

            return $this->jsonResponse("Success", ['nric' => $nric, 'selfie' => $selfie], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function initializeWallets(Request $request)
    {
        try {
            $user = $this->guard()->user();

            DB::beginTransaction();

            $walletManager = new WalletManager;
            $initWallets = $walletManager->initializeWallets($user);

            DB::commit();

            return $this->jsonResponse("Success", ['user' => $user], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function loadBinaries($binaries = null)
    {
        if (is_a($binaries, Collection::class)) {
            $binaries->each(function($binary) {
                $binary->load('binaries', 'leftBinary', 'rightBinary');
                $this->loadBinaries($binary->binaries);
            });
        }
    }

    protected function generateUniquePrice($price = 0, $length = 0)
    {
        $rNumber = $price + UniqueHelper::randomNumber($length);
        $exists = UpgradeRequest::active()->where('price', '=', $rNumber)->exists();

        if ($exists) {
            return $this->generateUniquePrice($price, $length);
        } else {
            return $rNumber;
        }
    }

    public function setPin(Request $request)
    {
      $validatedData = $request->validate([
            'pin' => 'required|numeric|confirmed|digits_between:4,8',
            'pin_confirmation' => 'required|numeric'
        ]);

    	try {
            DB::beginTransaction();
        	$user = $request->user();

        	$userManager = new UserManager($user);
        	$updatedUser = $userManager->setPin($request->pin);

            DB::commit();
            return $this->jsonResponse("Success", array('user' => $user), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
    	}
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}
