<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Helpers\Enums\TransactionType;
use App\Helpers\Enums\WalletType;
use App\Models\Transaction;

class TransactionController extends Controller
{
    use ApiResponse;

    public function owned(Request $request)
    {
        $validatedData = $request->validate([
            'start_date' => 'nullable|date_format:Y-m-d',
            'end_date' => 'nullable|date_format:Y-m-d',
            'offset' => 'nullable|numeric',
            'limit' => 'nullable|numeric'
        ]);
        
        try {
            $startDate = $request->start_date ? Carbon::createFromFormat('Y-m-d', $request->start_date, $request->header('Timezone'))->startOfDay()->setTimezone('UTC') : null;
            $endDate = $request->end_date ? Carbon::createFromFormat('Y-m-d', $request->end_date, $request->header('Timezone'))->endOfDay()->setTimezone('UTC') : null;

            $user = $this->guard()->user();
            $userWalletsId = $user->wallets()->where('type', WalletType::SECONDARY)->pluck('id');

            // Note: Filter type should be shown
            $types = TransactionType::getList();

            $query = Transaction::query();

            if ($startDate && $endDate) {
                $query->whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()]);
            } else if ($startDate && !$endDate) {
                $query->whereDate('created_at', '>=', $startDate->toDateTimeString());
            } else if (!$startDate && $endDate) {
                $query->whereDate('created_at', '<=', $endDate->toDateTimeString());
            }

            // Query by owner wallet, either on credit or debit side
            $query->where(function ($query) use ($userWalletsId) {
                $query->whereIn('debit_wallet_id', $userWalletsId)
                      ->orWhereIn('credit_wallet_id', $userWalletsId);
            });

            $query->whereIn('type', $types);

            $query->orderBy('created_at', 'desc')->orderBy('id', 'desc');

            if (isset($offset)) $query->offset($offset);
            if (isset($limit)) $query->limit($limit);

            $transactions = $query->get();
            // Map collection with owned_as, type_string field
            // $transactions->map(function($t) use ($userWalletsId) {
            //     $this->alterTransaction($t, $userWalletsId);
            //     return $t;
            // });

            return $this->jsonResponse("Success", array('transactions' => $transactions), 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}