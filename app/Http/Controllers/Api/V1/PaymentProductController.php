<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use App\Helpers\Managers\PaymentProductManager;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Managers\NotificationManager;
use App\Helpers\Managers\UserManager;
use App\Helpers\Enums\OrderType;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\WalletType;
use App\Helpers\Enums\NotificationType;
use App\Helpers\Enums\PaymentProductGroupType;
use App\Helpers\Customizers\PaymentCustomizer;
use App\Helpers\Customizers\InquiryCustomizer;
use App\Models\PaymentProduct;
use App\Models\PaymentInquiry;
use App\Models\PaymentProductTransaction;
use App\Models\User;
use App\Models\Order;
use App\Models\Wallet;
use App\Models\PaymentProductCategory;
use App\Models\Config as AppConfig;

class PaymentProductController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        try {
            $this->client = new \GuzzleHttp\Client();
            // $this->client = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));

            $this->bimasaktiUrl = config('paymentproduct.fastpay.url');
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
    }
    
  
    public function setPaymentInquiry(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    public function getPaymentProductCategories(Request $request)
    {  
        try {
            $user = $request->user();
            $id = isset($request->productType) ? $request->productType : null;
            $limit = isset($request->limit) ? $request->limit : null;
            $offset = isset($request->offset) ? $request->offset : null;
            $keyword = isset($request->keyword) ? $request->keyword : null;

            $paymentProductManager = new PaymentProductManager;
            $categories = $paymentProductManager->getPaymentProductCategories($id, $limit, $offset, $keyword);
            $checkLimit = $user->orders()->whereMonth('created_at', date('n'))->where('status', OrderStatus::COMPLETED)->where('type', '=', OrderType::PHONE_PREPAID)->get()->sum('total');
            $adminFee = 0;
            $limitTrans = 0;

            if ($user->packages == PackageType::FREE) {
                $adminFee = 1500;
            } elseif ($user->packages == PackageType::SILVER) {
                $adminFee = 1300;
                $limitTrans = 500000;
            } else if ($user->packages == PackageType::GOLD) {
                $adminFee = 1200;
                $limitTrans = 1000000;
            } else {
                $adminFee = 1000;
                $limitTrans = 1500000;
            }
            $categories['currentLimitUser'] = $checkLimit;
            $categories['limitUser'] = $limitTrans;
            $categories['adminFee'] = $adminFee;
            return response()->json(['message' => 'Success', 'data' => $categories]);

        } catch (Exception $e) {
            $error = $e->getMessage();
            return response()->json(['error' => $error], 400);
        }
    }

    public function getPaymentProduct(Request $request)
    {   
        try {
            $id = isset($request->categories) ? $request->categories : null;

            $paymentProductManager = new PaymentProductManager;
            $paymentProducts = $paymentProductManager->getPaymentProduct($id);

            return response()->json(['message' => 'Success', 'data' => $paymentProducts]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            return response()->json(['error' => $error], 400);
        }
    }

    public function getPhoneCategory(Request $request)
    {
        try {
           
            $phoneHeader = $request->phone_number;
            // if(substr($phoneHeader, 0, 2) == '08'){
            //     $phoneHeader = str_replace('08', '+628', $phoneHeader);   
            // }
            $phoneHeader = substr($phoneHeader, 0, 6);
            $paymentProductCategory = PaymentProductCategory::where('data', 'like', '%'.$phoneHeader.'%')->first();
            $paymentProducts = PaymentProduct::where('category_id', $paymentProductCategory->id)->get();

            return response()->json(['message' => 'Success', 'payment_categories' => $paymentProductCategory, 'products' => $paymentProducts]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            return response()->json(['error' => $error], 400);   
        }
    }

    public function inquiry(Request $request)
    {
        try {
            $user = $request->user();
            $inquiryType = $request->inquiryType;

            switch ($inquiryType) {
                case AppConfig::where('key', 'pp_type_pln_id')->value('value');
                    if ($request->inquiry_type == 0){
                        $orderType = OrderType::PLN_PREPAID;
                        $paymentProductId = AppConfig::where('key', 'pln_prepaid_inquiry_id')->value('value');
                    } else if ($request->inquiry_type == 1){
                        $orderType = OrderType::PLN_POSTPAID;
                        $paymentProductId = AppConfig::where('key', 'pln_postpaid_inquiry_id')->value('value');
                    }
                    break;
                case AppConfig::where('key', 'pp_type_pasca_phone_id')->value('value'):
                    $orderType = OrderType::PHONE_POSTPAID;
                    $paymentProductId = $request->payment_product_id;
                    break;
                case AppConfig::where('key', 'pp_type_pdam_id')->value('value'):
                    $orderType = OrderType::PDAM;
                    $paymentProductId = $request->payment_product_id;
                    break;
            }
            if (!$orderType) throw new Exception(__('error/messages.went_wrong'));

            $inquiryCustomizer = new InquiryCustomizer();
            $rules = $inquiryCustomizer->getRule(OrderType::enumToSlug($orderType));
            $validatedData = $request->validate($rules);
            $paymentProduct = PaymentProduct::find($paymentProductId);
            if (!$paymentProduct) throw new Exception('Payment product not found!', 404);

            DB::beginTransaction();

            // $converter = $inquiryCustomizer->getConverter($inquiryType);
            // if (!$converter) throw new Exception('Invalid inquiry type!', 400);

            // foreach ($converter as $key => $value) {
            //     $merge[$key] = $request->{$value};
            // }
            $paymentProductManager = new PaymentProductManager($paymentProduct);
            $result = $paymentProductManager->setUser($user)->inquiry($orderType, $request->all());
            DB::commit();
            return $this->jsonResponse('Success', $result, 200);
        } catch (Exception $e) {
            \Log::info($e);
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());  
        }
    }

    public function payment(Request $request)
    {

        try {
            $orderType = OrderType::getTypeFromPayment($request->productType); // 1 PHONE PREPAID | 2 PHONE POSTPAID | 3 PLN | 4 GAME
            if(isset($request->order_id)){
                $order = Order::find($request->order_id);
                if (!$order) throw new Exception('Payment inquiry not found!', 404);            
            }

            if (!$orderType) {
                $orderType = $order->type;
            }
            if ($orderType == OrderType::PLN_POSTPAID) {
                $payment_product_id = AppConfig::where('key', 'pln_postpaid_inquiry_id')->value('value');
            } else if ($orderType == OrderType::PHONE_POSTPAID) {
                $payment_product_id = $order->orderDetails[0]->productable->id;
            } else {
                $payment_product_id = $request->payment_product_id;
            }

            $paymentCustomizer = new PaymentCustomizer();
            // $paymentProductType = $paymentProduct->type->product;
            $rules = $paymentCustomizer->getRule(OrderType::enumToSlug($orderType));
            
            $validatedData = $request->validate($rules);

            $user = $request->user();
            \Log::info($user);
            \Log::info($orderType);
            // $suspendedId = [432, 454, 472, 495, 500, 507, 519, 528, 534, 547, 565, 568, 578, 588, 596, 601, 615, 621, 626, 630, 632, 638, 645, 732, 756, 774, 797, 804, 805, 808];
            // $suspendedId = [432, 13];
            // if ($user->packages == PackageType::FREE && $orderType == OrderType::PHONE_PREPAID) {
            //     $checkLimit = $user->orders()->whereMonth('created_at', date('n'))->where('status', OrderStatus::COMPLETED)->where('type', '=', OrderType::PHONE_PREPAID)->get()->sum('total');
            //     if ($user->verifications_status['nric'] == 1 && $user->verifications_status['selfie'] == 1){
            //         \Log::info('Terverifikasi');
            //         if ($checkLimit > 500000) throw new Exception('Akun anda telah melebihi transaksi bulanan, silahkan upgade paket untuk menikmati transaksi tanpa batas.', 500);
            //     } else {
            //         \Log::info('Tidak Terverifikasi');
            //         if ($checkLimit > 100000) throw new Exception('Akun anda telah melebihi batas transaksi, silahkan verifikasi data diri anda.', 500);
            //     }
            //     \Log::info($checkLimit);
            // }
            $userManager = new UserManager($user);
            $credential = $userManager->checkPassword($request->password);

            $walletType = $request->wallet_type ? $request->wallet_type : WalletType::PRIMARY;
            $wallet = $user->wallets()->where('type', $walletType)->first();

            $paymentProduct = PaymentProduct::find($payment_product_id);
            if (!$paymentProduct) throw new Exception('Payment product not found!', 404);

            $paymentProductCode = $paymentProduct->data['code'];

            $price = 0;
            if ($paymentProduct->price != 0) {
                $price = $paymentProduct->price;
            } else {
                if ($order) {
                    if ($order->total != 0){
                        $price = $order->total;
                    }
                }
            }
            if ($price > $wallet->balance) throw new Exception('Balance not enough!', 400);
            $phone_number = $request->phone_number ?? null;
            $method = $paymentProduct->type->method;
            DB::beginTransaction();

            $data = [
                'wallet_type' => $walletType,
                'payment_product_id' => $request->payment_product_id,
                'customer_id' => $request->customer_id ?? '',
                'method' => $method,
                'payment_product_code' => $paymentProductCode,
                'price' => $price,
                'phone_number' =>$phone_number
            ];
            $merge = [];
            if (isset($request->order_id) && isset($order->options)) {
                if ($orderType == OrderType::PLN_PREPAID) {
                    if (strlen($request->customer_id) == 11) {
                        $customer_id1 = $request->customer_id; //no meteran
                        $customer_id2 = "";
                        $customer_id3 = "";
                    } else if (strlen($request->customer_id) == 12) {
                        $customer_id1 = "";
                        $customer_id2 = $request->customer_id; //no id pelanggan
                        $customer_id3 = "";
                    } else throw new Exception("Your ID is not valid", 400);
                } else if ($orderType == OrderType::PLN_POSTPAID) {
                    $customer_id1 = $request->customer_id; //no id pelanggan
                    $customer_id2 = "";
                    $customer_id3 = "";
                } else if ($orderType == OrderType::PHONE_POSTPAID) {
                    $customer_id1 = $request->area_code; //kode area
                    $customer_id2 = $request->phone_number; //no telp
                    $customer_id3 = "";
                } else if ($orderType == OrderType::PDAM) {
                    $customer_id1 = $request->customer_id; //no pelanggan
                    $customer_id2 = ""; //no sambungan
                    $customer_id3 = "";
                } else throw new Exception("Your ". ($orderType == OrderType::PHONE_POSTPAID ? "phone number" : "ID") ." is not valid", 400);
                
                $merge = [
                    'order_id' => $order->id,
                    'customer_id1' => $customer_id1,
                    'customer_id2' => $customer_id2,
                    'customer_id3' => $customer_id3,
                    'ref2' => trim($order->options->ref2)
                ];
            }
            $data = array_merge($data, $merge);
            $paymentProductManager = new PaymentProductManager($paymentProduct);
            if (isset($order)){
                $transaction = $paymentProductManager->setUser($user)->setPaymentInquiry($order)->payment($orderType, $data);
            } else {
                $transaction = $paymentProductManager->setUser($user)->payment($orderType, $data);
            }
            $productId = $transaction->order->orderDetails[0]->productable_id;
            if ($productId) {
                $product = PaymentProduct::find($productId)->name;
                // $product = $transaction->order->orderDetails[0]->productable->name;
            } else {
                $product = null;
            }
            $uniqueId = null;
            if ($orderType == OrderType::PLN_PREPAID){
                $uniqueId = $transaction->order->options->meter_no;
                $notificationType = NotificationType::PLN_PREPAID;
            } else if ($orderType == OrderType::PLN_POSTPAID){
                $uniqueId = $transaction->order->options->customer_id;
                $notificationType = NotificationType::PLN_POSTPAID;
            } else if ($orderType == OrderType::PHONE_PREPAID){
                $uniqueId = $transaction->order->options->phone_number;
            } else if ($orderType == OrderType::GAME){
                $uniqueId = null;
            } else if ($orderType == OrderType::PDAM){
                $uniqueId = $transaction->order->option->customer_id;
                $notificationType = NotificationType::PDAM;
            } else if ($orderType == OrderType::PHONE_POSTPAID){
                $uniqueId = $transaction->order->options->phone_number;
                $notificationType = NotificationType::PHONE_POSTPAID;
            }
            $receipt = [
                'reference_number' => $transaction->reference_number,
                'time' => $transaction->created_at->format('Y-m-d H:i:s'),
                'amount' => $transaction->total_amount,
                'product' => $product,
                'sn' => $transaction->order->options->sn,
                'customer_id' => $uniqueId,
                'wallet_type' => WalletType::getString($walletType)
            ];
            
            $result = ['receipt' => $receipt];
            if (isset($notificationType)) {
                $notificationManager = new NotificationManager($user);
                $notificationData = [
                        'category' => NotificationType::getCategory($notificationType),
                        'order_id' => $transaction->order->id,
                        'reference_number' => $transaction->reference_number,
                        'amount' => $transaction->order->total,
                    ];
                $convertToArray = (array)$transaction->order->options;
                $notificationData = array_merge($notificationData, $convertToArray);
                $notificationManager->setType($notificationType)->update($user, $notificationData);
            }
            DB::commit();
            return $this->jsonResponse('Success', $result, 200);
        } catch (Exception $e){
            \Log::info($e);
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}
