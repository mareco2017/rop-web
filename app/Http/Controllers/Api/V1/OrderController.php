<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\Enums\OrderStatus;
use App\Helpers\Managers\OrderManager;
use App\Http\Controllers\Controller;
use App\Models\BankAccount;
use App\Models\Order;
use App\Models\Payment;
use App\Traits\ApiResponse;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    use ApiResponse;

    public function owned(Request $request)
    {
        $orderStatus = implode(',', OrderStatus::getArray());

        $validatedData = $request->validate([
            'limit' => 'nullable|numeric',
            'offset' => 'nullable|numeric',
            'status' => 'nullable|string'
        ]);

        try {
            $user = $request->user();
            $ownerWalletIds = $user->wallets()->pluck('id')->toArray();

            $limit = $request->limit ?? 50;
            $offset = $request->offset ?? 0;

            $status = $request->status ? explode(',', $request->status) : [];
            $type = $request->type ? explode (',', $request->type) : [];

            $orderManager = new OrderManager($user);

            $query = $user->orders()->with('orderDetails')->limit($limit)->offset($offset)->orderBy('created_at', 'DESC');

            if (count($status) > 0) {
                $query->whereIn('status', $status);
            }

            if (count($type) > 0) {
                $query->whereIn('type', $type);
            }

            $orders = $query->get();

            $result = ['orders' => $orders];
            return $this->jsonResponse("Success", $result, 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function detail(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            $ownerWalletIds = $user->wallets->pluck('id')->toArray();
            $orderId = $request->order->id;
            $orderManager = new OrderManager($user);
            $transaction = $orderManager->transactionDetail($orderId);

            $result = [
                'order' => $order->load('orderDetails'),
                'transaction' => $transaction,
                'owner_wallet_ids' => $ownerWalletIds
            ];
            return $this->jsonResponse("Success", $result, 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function statement(Request $request)
    {
        try {
            $user = $request->user();
            $bankAccount = BankAccount::where('ownable_id', $user->id)->where('is_primary', 1)->first();
            if (!$bankAccount) throw new Exception("Please make sure you set the bank account!", 400);
            $payment = Payment::where('bank_account_id', $bankAccount->id)->with('bankAccount');
            if (!$payment) throw new Exception("You don't have any statement yet!", 400);
            if ($request->claimed == 0) $payment->whereNull('claimed_at')->orderBy('created_at', 'DESC');
            if ($request->claimed == 1) $payment->whereNotNull('claimed_at')->orderBy('created_at', 'DESC');
            $payments = $payment->get();
            \Log::info($payments);
            $result = ['result' => $payments];
            return $this->jsonResponse("Success", $result, 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function history(Request $request)
    {
        $validatedData = $request->validate([
            'status' => 'numeric',
            'page' => 'numeric',
            'types' => 'nullable|string'
        ]);

        try {
            $user = $request->user();
            $ownerWalletIds = $user->wallets->pluck('id')->toArray();
            $limit = 10;
            $offset = ($request->page - 1) * $limit;
            $types = $request->types ? explode(',', $request->types) : null;

            if ($request->status == 1) {
                $status = [OrderStatus::COMPLETED];
            } else {
                $status = [OrderStatus::WAITING_FOR_PAYMENT, OrderStatus::PENDING, OrderStatus::ONGOING];
            }

            $query = $user->orders()->whereIn('type', $types)->whereIn('status', $status)->with('orderDetails', 'transactions');
            $orders = $query->orderBy('created_at', 'desc')->limit($limit)->offset($offset)->get();     
            $count = $user->orders()->whereIn('type', $types)->whereIn('status', $status)->count();
            
            return $this->jsonResponse("Success", ['orders' => $orders, 'count' => $count, 'owner_wallet_ids' => $ownerWalletIds], 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function onGoingdetail(Request $request, Order $order)
    {
        try {
            return $this->jsonResponse("Success", ['order' => $order->load('orderDetails')], 200);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }
}
