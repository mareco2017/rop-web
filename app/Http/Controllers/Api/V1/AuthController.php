<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use DB;
use App\Models\SessionToken;
use App\Models\User;
use App\Models\RightBinary;
use App\Models\LeftBinary;
use App\Helpers\UniqueHelper;
use App\Helpers\Managers\OTPManager;
use App\Helpers\Managers\UserManager;
use App\Helpers\Managers\WalletManager;
use Propaganistas\LaravelPhone\PhoneNumber;

class AuthController extends Controller
{
    use ApiResponse;

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|string|alpha_spaces',
            'last_name' => 'nullable|string|alpha_spaces',
            'phone_number' => 'required|unique:users,phone_number',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'has_referral' => 'required|boolean',
            // 'sponsor_code' => 'required_if:has_referral,1',
            // 'leader_code' => 'required_if:has_referral,1'
        ]);

        try {
            DB::beginTransaction();
            if ($request->has_referral) {
                $sponsorUser = User::where('referral_code', $request->sponsor_code)->first();
                if (!$sponsorUser) throw new Exception("Sponsor user not found!", 400);
                $leaderUser = User::where('referral_code', $request->leader_code)->first();
                if (!$leaderUser) throw new Exception("Leader user not found!", 400);
            }

            $user = new User;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->phone_number = $request->phone_number;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role_id = 5;

            $prefix = strtoupper(substr($request->first_name, 0, 3));
            $user->referral_code = UniqueHelper::generateNumber(3, User::class, 'referral_code', $prefix);
            
            if ($request->has_referral) {
                $user->sponsor()->associate($sponsorUser);
            }

            if (!$user->save()) {
                throw new Exception("Failed while saving user!", 500);
            }

            $user = $user->fresh();

            if ($request->has_referral) {
                $this->assignReferral($request, $leaderUser, $user);
            }

            $walletManager = new WalletManager;
            $initWallets = $walletManager->initializeWallets($user);

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($user->phone_number, 'verify_phone_number');

            DB::commit();

            $sessionToken = $this->loginAndCreateToken($request, $user);
            $loggedInUser = $this->guard()->user();

            return $this->jsonResponse("Success", array('token' => $sessionToken->token, 'user' => $loggedInUser), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        try {
            DB::beginTransaction();
            if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $credentialType = 'email';
            } else if (is_numeric($request->email)) {
                $credentialType = 'phone_number';
                $request->merge(['phone_number' => $request->email]);
            }

            $credentials = $request->only($credentialType, 'password');

            if (!$tokenString = $this->guard()->attempt($credentials)) {
                throw new Exception("Wrong username or password!", 400);
            }

            $loggedInUser = $this->guard()->user();
            $sessionToken = $this->loginAndCreateToken($request, $loggedInUser, $tokenString);

            $responseData = ['token' => $sessionToken->token, 'user' => $loggedInUser];
            DB::commit();
            return $this->jsonResponse("Success", $responseData, 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function activate(Request $request)
    {
        $validatedData = $request->validate([
            'fullname' => 'required|string|alpha_spaces',
            'phone_number' => 'required',
            'email' => 'required|email'
        ]);

        try {
            DB::beginTransaction();
            $userManager = new UserManager($this->guard()->user(), $this->guard());
            $user = $userManager->findUserByPhoneNumber($request->phone_number, true);
            if (!$user) {
                $existUser = User::where('email', $request->email)->first();
                if ($existUser) throw new Exception(__('error/messages.create_user_email_used'), 409);

                $user = new User;
                $user->phone_number = PhoneNumber::make($request->phone_number, 'ID');
                if (isset($request->fullname)) {
                    $a = explode(' ', $request->fullname, 2);
                    $user->first_name = $a[0];
                    $user->last_name = isset($a[1]) ? $a[1] : '';
                }
                $user->email = $request->email;
                $user->role_id = 5;

                $prefix = strtoupper(substr($a[0], 0, 3));
                $user->referral_code = UniqueHelper::generateNumber(3, User::class, 'referral_code', $prefix);

                if (!$user->save()) {
                    throw new Exception("Failed while saving user!", 500);
                }
            }

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($user->phone_number, 'verify_phone_number');

            $token = '';
            // $token = $userManager->forceLogin($user);
            // $user = $userManager->getUser();
        
            // $payload = $this->guard()->payload();
            // $exp = $payload->get('exp');
            // $expiredAt = date("Y-m-d H:i:s", $exp);

            // $data = [
            //     'token' => $token,
            //     'expired_at' => $expiredAt,
            //     'device_type' => $request->header('Device-Type') ?: $request->header('User-Agent'),
            //     'device_token' => $request->header('Device-Token')
            // ];

            // $session = $userManager->loggedIn($data, 'api');

            DB::commit();
            return $this->jsonResponse("Success", ['token' => $token, 'user' => $user], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function requestOTP(Request $request)
    {
        $validatedData = $request->validate([
            'credential' => 'required'
        ]);

        try {
            DB::beginTransaction();
            $userManager = new UserManager($this->guard()->user(), $this->guard());
            $user = $userManager->findUserByPhoneNumber($request->credential, true);

            $otpManager = new OTPManager($user);
            $otp = $otpManager->sendOTP($user->phone_number);
            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
        } catch (Exception $e) {
            \Log::info($e->getMessage());
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function verifyOTPAndLogin(Request $request)
    {
        $validatedData = $request->validate([
            'credential' => 'required',
            'pin' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $userManager = new UserManager($this->guard()->user(), $this->guard());
            $token = $userManager->login($request->input('credential'), $request->input('pin'));
            $user = $userManager->getUser();
        
            $payload = $this->guard()->payload();
            $exp = $payload->get('exp');
            $expiredAt = date("Y-m-d H:i:s", $exp);

            $data = [
                'token' => $token,
                'expired_at' => $expiredAt,
                'device_type' => $request->header('Device-Type') ?: $request->header('User-Agent'),
                'device_token' => $request->header('Device-Token')
            ];

            $session = $userManager->loggedIn($data, 'api');

            $responseData = ['token' => $token, 'user' => $user];
            DB::commit();
            return $this->jsonResponse("Success", $responseData, 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function loginAndCreateToken(Request $request, User $user, $tokenString = null)
    {
        try {
            DB::beginTransaction();
            if (!$tokenString) {
                $tokenString = $this->guard()->login($user);
            }
            $payload = $this->guard()->payload();
            $exp = $payload->get('exp');
            $expiredAt = date("Y-m-d H:i:s", $exp);
            $sessionToken = new SessionToken;
            $sessionToken->token = $tokenString;
            $sessionToken->type = 'api';
            $sessionToken->device_type = $request->header('Device-Type') ?: $request->header('User-Agent');
            $sessionToken->ip_address = null;
            $sessionToken->device_token = $request->header('Device-Token');
            $sessionToken->ownable()->associate($user);
            $sessionToken->expired_at = $expiredAt;

            if (!$sessionToken->save()) {
                throw new Exception("Failed while saving session token!", 500);
            }
            DB::commit();

            return $sessionToken;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        try {
            DB::beginTransaction();
            $oldToken = $request->bearerToken();
            $oldSessionToken = SessionToken::withTrashed()->where('token', $oldToken)->first();

            $tokenString = $this->guard()->refresh(true, true);
            $this->guard()->setToken($tokenString);
            $user = $this->guard()->user();
            
            // $logoutSession = $userManager->loggedOut($oldToken);

            if (!$user) throw new Exception("Something went wrong!", 500);

            $sessionToken = $this->loginAndCreateToken($request, $user, $tokenString);

            $responseData = ['token' => $sessionToken->token, 'user' => $user];
            DB::commit();
            return $this->jsonResponse("Success", $responseData, 200);
        } catch (Exception $e) {
            \Log::info($e);
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    public function profile(Request $request)
    {
        $user = $this->guard()->user();
        return $this->jsonResponse("Success", array('user' => $user));
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only('phone_number_or_email', 'password');
        if (is_numeric($request->input('phone_number_or_email'))) {
            $credentials['phone_number'] = $request->input('phone_number_or_email');
        } elseif (filter_var($request->input('phone_number_or_email'), FILTER_VALIDATE_EMAIL)) {
            $credentials['email'] = $request->input('phone_number_or_email');
        }
        unset($credentials['phone_number_or_email']);
        return $credentials;
    }

    protected function checkRedirectPath(Request $request)
    {
        $this->redirectTo = $this->guard()->user()->role->redirect_on_login;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $this->checkRedirectPath($request);

        $data = [
            'token' => $request->session()->getId(),
            'ip' => $request->ip(),
            'device_type' => $request->header('User-Agent')
        ];
        // Save user login session
        $userManager = new UserManager($this->guard()->user());
        $session = $userManager->loggedIn($data, 'web');

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            DB::beginTransaction();
            $token = $request->bearerToken();
            $sessionToken = SessionToken::where('token', $token)->first();
            $sessionToken->delete();

            $this->guard()->logout();
            DB::commit();
            return $this->jsonResponse("Success", array(), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());   
        }
    }

    protected function assignReferral(Request $request, User $leaderUser, User $user)
    {
        $availableBinary = $leaderUser->availableBinary();
        if (!$availableBinary) return;
        if ($availableBinary == 'l') {
            $binary = LeftBinary::create(['user_id' => $user->id]);
            $leaderUser->leftBinary()->associate($binary);
        } else {
            $binary = RightBinary::create(['user_id' => $user->id]);
            $leaderUser->rightBinary()->associate($binary);
        }

        if (!$leaderUser->save()) {
            throw new Exception("Failed while saving leader user!", 500);
        }

        $user->leader()->associate($leaderUser);
        
        if (!$user->save()) {
            throw new Exception("Failed while updating user!", 500);
        }

        return $user;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}