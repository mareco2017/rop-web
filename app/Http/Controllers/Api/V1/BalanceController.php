<?php

namespace App\Http\Controllers\Api\V1;

use App\Facades\HistoryLogger;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderType;
use App\Helpers\GlobalHelper;
use App\Helpers\Managers\OrderManager;
use App\Helpers\Managers\PaymentManager;
use App\Helpers\Managers\TopupManager;
use App\Helpers\UniqueHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Config as AppConfig;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class BalanceController extends Controller
{
    use ApiResponse;

    public function topupList(Request $request)
    {
        $user = $request->user();

        $path = storage_path() . "/json/topup-list.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $json = json_decode(file_get_contents($path), true);

        $serviceTimePath = storage_path() . "/json/service-time.json";
        $serviceTimeJson = json_decode(file_get_contents($serviceTimePath), true);
        $startTime = $serviceTimeJson[OrderType::TOPUP]['start'];
        $endTime = $serviceTimeJson[OrderType::TOPUP]['end'];

        $note = __('config/messages.time_messages', ['object' => 'Top Up', 'start_time' => $startTime, 'end_time' => $endTime]);

        return $this->jsonResponse("Success", array('topup' => $json, 'note' => $note), 200);
    }


    public function topup(Request $request)
    {
        $validatedData = $request->validate([
            'dest_bank_account_id' => 'required|exists:bank_accounts,id',
            'amount' => 'required|numeric'
        ]);

        try {
            $user = $request->user();

            $amount = $request->amount;
            $minimumAmount = 50000;

            if ($amount < $minimumAmount) throw new Exception(__('error/messages.min_topup', ['object' => GlobalHelper::toCurrency($minimumAmount)]), 400);

            $activeTopupOrder = $user->orders()->active()->type(OrderType::TOPUP)->first();
            if ($activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            $id = AppConfig::where('key', 'rop_wallet_id')->value('value');
            $referable = Admin::find($id);
            if (!$referable) throw new Exception(__('error/messages.went_wrong_pls_contact'), 400);

            DB::beginTransaction();
            $orderManager = new OrderManager($user);
            $orderData = [
                'options' => [
                    'dest_bank_account_id' => (int)$request->dest_bank_account_id
                ],
                'products' => [
                    (object) [
                        'qty' => 1,
                        'price' => $amount,
                        'discount' => UniqueHelper::generateTopupUniqueNumber(3, true) // Unique
                    ]
                ]
            ];

            $order = $orderManager->create($referable, OrderType::TOPUP, $orderData);

            DB::commit();

            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function topupPaid(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            $activeTopupOrder = $order ?? $user->orders()->active()->type(OrderType::TOPUP)->first();
            if (!$activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            DB::beginTransaction();
            $topupManager = new TopupManager($user);

            $order = $topupManager->setOrder($activeTopupOrder)->paid();

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function topupCancel(Request $request, Order $order)
    {
        try {
            $user = $request->user();

            $activeTopupOrder = $order ?? $user->orders()->active()->type(OrderType::TOPUP)->first();
            if (!$activeTopupOrder) throw new Exception(__('error/messages.still_active', ['object' => OrderType::getString(OrderType::TOPUP)]), 400);

            if (!in_array($activeTopupOrder->status, [OrderStatus::PENDING, OrderStatus::WAITING_FOR_PAYMENT, OrderStatus::ONGOING])) {
                $str = implode(" or ", [OrderStatus::getString(OrderStatus::PENDING), OrderStatus::getString(OrderStatus::WAITING_FOR_PAYMENT)]);
                throw new Exception(__('error/messages.order_cannot_cancel', ['object' => $str]), 400);
            }

            DB::beginTransaction();
            $topupManager = new TopupManager($user);
            $cancelOrder = $topupManager->setOrder($activeTopupOrder)->cancel();

            DB::commit();
            return $this->jsonResponse("Success", array('order' => $activeTopupOrder->load('orderDetails')), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function bonusWithdraw(Request $request)
    {
      \Log::info($request->all());

        $pairId = [];
        $orderId = [];
        $user = $request->user();
        $sponsors = Order::where('ownable_id', $user->id)->whereNull('claimed_at')->where('type', OrderType::SPONSOR_BONUS)->get();
        $pairs = Order::where('ownable_id', $user->id)->whereNull('claimed_at')->where('type', OrderType::PAIRING_BONUS)->get();
        if (count($pairs) != 0) {
            foreach ($pairs as $key => $pair) {
                $pair->claimed_at = Carbon::now();
                $pair->save();
                $pairId[] .= $pair->id;
            }
        }
        if (count($sponsors) != 0) {
            foreach ($sponsors as $key => $sponsor) {
                $sponsor->claimed_at = Carbon::now();
                $sponsor->save();
                $orderId[] .= $sponsor->id;
            }
        }
        if (count($pairs) != 0) {
            $pairToArray['pair_ids'] = $pairId;
        } else {
            $pairToArray['pair_ids'] = null;
        }
        if (count($sponsors) != 0) {
            $orderToArray['order_ids'] = $orderId;
        } else {
            $orderToArray['order_ids'] = null;
        }
        $arrays = array_merge($pairToArray, $orderToArray, $request->all());
        try {
            DB::beginTransaction();
            $paymentManager = new PaymentManager($user);
            $updatedPayment = $paymentManager->update(new Payment, $arrays);
            DB::commit();
            unset($updatedPayment->options);
            return $this->jsonResponse("Success", array('order' => $updatedPayment), 200);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

}
