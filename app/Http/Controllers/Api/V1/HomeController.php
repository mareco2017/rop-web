<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;
use App\Models\Config as AppConfig;
use Illuminate\Validation\ValidationException;
use App\Traits\ApiResponse;
use App\Models\User;
use App\Models\Promo;

class HomeController extends Controller
{
    use ApiResponse;

    public function index(Request $request)
    {
        $user = $this->guard()->user();

        $banners = Promo::banner()->active()->orderBy('urut')->get();

        $wallets = $user->wallets;

        return $this->jsonResponse("Success", array('banners' => $banners, 'wallets' => $wallets, 'version' => '1.1.8'));
    }

    public function contactUs()
    {
      $email = AppConfig::where('key', 'support_email')->value('value');
      $phone = AppConfig::where('key', 'office_number')->value('value');
      $mobile = AppConfig::where('key', 'whatsapp_number')->value('value');
      
      return $this->jsonResponse("Success", array('email' => $email, 'phone' => $phone, 'whatsapp' => $mobile, 'cs' => 'Customer Service kami siap melayani anda setiap hari dari jam 09:00 s/d 18:00.'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api');
    }
}
