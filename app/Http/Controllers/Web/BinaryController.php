<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Binary;

class BinaryController extends Controller
{
    public function show(Request $request)
    {
        $user = $this->guard()->user();
        
        $binaryId = $user->binary ? $user->binary->id : null;

        $binaries = $user->getBinaryTree();
        $leftBinaryPoint = 0;
        $rightBinaryPoint = 0;

        if ($user->packages != 0) {
            $leftBinaryPoint = $user->total_left_binary_point;
            $rightBinaryPoint = $user->total_right_binary_point;
        } 

        return view('webview.binary.index')
            ->with('dekstop', true)
            ->with('binaries', $binaries)
            ->with('leftBinaryPoint', $leftBinaryPoint)
            ->with('rightBinaryPoint', $rightBinaryPoint)
            ->with('leftBinaryUser', $user->total_left_binary_user)
            ->with('rightBinaryUser', $user->total_right_binary_user);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }
}