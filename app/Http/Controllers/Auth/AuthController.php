<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\RedirectsUsers;
use DB;
use Exception;

class AuthController extends Controller
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        try {
            $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
            $request->merge([$field => $request->input('username')]);
            
            $credentials = $request->only($field, 'password');
            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if (!$this->guard()->attempt($credentials)) {
                $this->incrementLoginAttempts($request);
                throw new Exception("Wrong username or password!", 400);
            }

            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            // $this->checkRedirectPath($request);

            $loggedInUser = $this->guard()->user();

            return redirect()->route('web.binary');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        }
    }

    public function username()
    {
        return 'email';
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function logout()
    {
        $this->guard()->logout();

        return redirect()->route('dashboard');
    }
}
