<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateUsingParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!$request->token) {
            $this->unauthenticated($request);
        }

        $token = $request->token;

        auth('api')->setToken($token);
        auth()->shouldUse('api');

        if (auth()->check()) {
            return $next($request);
        }

        $this->unauthenticated($request);
    }

    protected function unauthenticated($request) {
        if ($request->expectsJson()) {
            return response()->json(['message' => 'Unauthenticated.', 'data' => (object)[]], 401);
        }
        abort(403);
        // return redirect()->route('admin.dashboard');
    }
}