<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pair extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'left_binary_user_id', 'right_binary_user_id', 'leader_user_id', 'leader_package_type', 'is_leader_claimed'
    ];
    
    protected $dates = ['claimed_at', 'created_at', 'updated_at', 'deleted_at'];

    public function leaderUser()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function leftBinaryUser()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function rightBinaryUser()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
