<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class OneTimePin extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number', 'email', 'pin', 'usage', 'expired_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['expired_at', 'deleted_at'];
    
    public function isExipred()
    {
        $now = Carbon::now();
        return $now->gt($this->expired_at);
    }

    public function used()
    {
        return $this->forceDelete();
    }
}
