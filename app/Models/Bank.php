<?php

namespace App\Models;

use App\Helpers\AwsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Bank extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'abbr'
    ];

    protected $appends = [
        'cover_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['cover'];
    protected $dates = ['deleted_at'];

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment', 'attachment_id');
    }

    public function getCoverUrlAttribute($value)
    {
        return $this->cover ? $this->cover->url : '';
    }
}
