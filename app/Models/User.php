<?php

namespace App\Models;

use App\Helpers\Enums\DocumentType;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\VerificationStatus;
use App\Helpers\Enums\VerificationType;
use App\Helpers\Enums\WalletType;
use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sponsor_id', 'leader_id', 'left_binary_id', 'right_binary_id', 'role_id', 'phone_number', 'device_type', 'email', 'fist_name', 'last_name', 'packages', 'password', 'gender', 'dob', 'profile_picture_id', 'referral_code', 'last_login', 'pin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $casts = [
        'google' => 'object',
        'facebook' => 'object',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'fullname', 'profile_picture_url', 'verifications_status', 'balances', 'is_pin_set'
    ];

    protected $dates = [
        'deleted_at',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'ownable');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'ownable');
    }

    public function getFullnameAttribute($value)
    {
        return $this->last_name ? $this->first_name . ' ' . $this->last_name : $this->first_name;
    }
    
    public function sponsor()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function leader()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function rightBinary()
    {
        return $this->belongsTo('App\Models\RightBinary');
    }

    public function leftBinary()
    {
        return $this->belongsTo('App\Models\LeftBinary');
    }

    public function sessionTokens()
    {
        return $this->morphMany('App\Models\SessionToken', 'ownable');
    }
    
    public function binary()
    {
        return $this->hasOne('App\Models\Binary');
    }

    public function verifications()
    {
        return $this->morphMany('App\Models\Verification', 'ownable');
    }

    public function documents()
    {
        return $this->morphMany('App\Models\Document', 'ownable');
    }

    public function wallets()
    {
        return $this->morphMany('App\Models\Wallet', 'ownable');
    }

    public function binaries()
    {
        return $this->hasMany('App\Models\User', 'leader_id');
    }

    public function pairs()
    {
        return $this->hasMany('App\Models\Pair', 'leader_user_id');
    }

    public function upgradeRequests()
    {
        return $this->hasMany('App\Models\UpgradeRequest', 'requested_by_id');
    }

    public function profilePicture()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function bankAccounts()
    {
        return $this->morphMany('App\Models\BankAccount', 'ownable');
    }

    public function availableBinary()
    {
        if (!$this->leftBinary) {
            return 'l';
        } else if (!$this->rightBinary) {
            return 'r';
        } else {
            return null;
        }
    }

    public function getBinaryTree()
    {
        $binaryId = $this->binary ? $this->binary->id : null;

        $binaries = DB::select("WITH RECURSIVE
                        treeSearch AS (
                            SELECT
                                id,
                                leader_binary_id,
                                left_binary_id,
                                right_binary_id,
                                0 as level,
                                user_id,
                                'n' as position,
                                'n' as parent_position,
                                type,
                                0 as binary_point
                            FROM binaries
                            WHERE id = '$binaryId'
                            
                            UNION ALL

                            SELECT
                                t.id,
                                ts.id,
                                t.left_binary_id,
                                t.right_binary_id,
                                ts.level + 1 as level,
                                t.user_id,
                                IF(t.id = ts.left_binary_id, 'l', IF(t.id = ts.right_binary_id, 'r', 'n')) as position,
                                IF(level = 0, IF(t.id = ts.left_binary_id, 'l', IF(t.id = ts.right_binary_id, 'r', 'n')), ts.parent_position) as parent_position,
                                t.type,
                                (CASE t.type
                                    WHEN 0 THEN ". PackageType::getPoint(0) ."
                                    WHEN 1 THEN ". PackageType::getPoint(1) ."
                                    WHEN 2 THEN ". PackageType::getPoint(2) ."
                                    WHEN 3 THEN ". PackageType::getPoint(3) ."
                                    ELSE 0
                                END) as binary_point
                            FROM binaries t, treeSearch ts
                            WHERE t.leader_binary_id = ts.id
                        )
                        SELECT
                        treeSearch.*,
                        users.referral_code,
                        users.packages,
                        CONCAT_WS(' ', users.first_name, IF(users.last_name = '', NULL, users.last_name)) AS fullname,
                        users.role_id
                        FROM treeSearch
                        JOIN users ON treeSearch.user_id = users.id
                        ORDER BY level, position, leader_binary_id;
                ");

        return $binaries;
    }

    public function getTotalLeftBinaryPointAttribute($value)
    {
        $binaries = $this->getBinaryTree();
        $col = collect($binaries);
        $totalLeftBinaryPoint = $col->where('role_id', '!=', 7)->where('parent_position', 'l')->sum('binary_point');
        return $totalLeftBinaryPoint;
    }

    public function getTotalLeftBinaryUserAttribute($value)
    {
        $binaries = $this->getBinaryTree();
        $col = collect($binaries);
        $totalLeftBinaryUser = count($col->where('parent_position', 'l'));
        return $totalLeftBinaryUser;
    }

    public function getTotalRightBinaryPointAttribute($value)
    {
        $binaries = $this->getBinaryTree();
        $col = collect($binaries);
        $totalRightBinaryPoint = $col->where('role_id', '!=', 7)->where('parent_position', 'r')->sum('binary_point');
        return $totalRightBinaryPoint;
    }

    public function getTotalRightBinaryUserAttribute($value)
    {
        $binaries = $this->getBinaryTree();
        $col = collect($binaries);
        $totalRightBinaryUser = count($col->where('parent_position', 'r'));
        return $totalRightBinaryUser;
    }

    public function getVerificationsStatusAttribute($value)
    {
        $nric = $this->documents()->type(DocumentType::NRIC)->first();
        $selfie = $this->documents()->type(DocumentType::SELFIE)->first();
        $verifications = [
            'phone_number' => $this->verifications()->where('type', VerificationType::PHONE_NUMBER)->where('status', VerificationStatus::VERIFIED)->first() ? true : false,
            'email' => $this->verifications()->where('type', VerificationType::EMAIL)->where('status', VerificationStatus::VERIFIED)->first() ? true : false,
            'nric' => $nric ? $nric->verification_status : 2,
            'selfie' => $selfie ? $selfie->verification_status : 2
        ];
        return $verifications;
    }

    public function getProfilePictureUrlAttribute($value)
    {
        if ($this->profilePicture) {
            return Storage::disk('s3')->url($this->profilePicture->key);
        }
        return '';
    }

    public function getDocument($type)
    {
        return $this->documents()->where('type', $type)->first();
    }

    public function getWallet($type)
    {
        return $this->wallets()->where('type', $type)->first();
    }

    public function payWallets()
    {
        return $this->wallets()->whereIn('type', [WalletType::PRIMARY, WalletType::SECONDARY, WalletType::POINT, WalletType::FROZEN, WalletType::CREDIT]);
    }

    public function scopeDocumentPending($query)
    {
        return $query->whereHas('documents', function($q) {
            return $q->where('type', DocumentType::NRIC)->whereHas('verifications', function($q) {
                return $q->where('status', VerificationStatus::PENDING);
            });
        });
    }

    public function scopeDocumentVerified($query)
    {
        return $query->whereHas('documents', function($q) {
            return $q->where('type', DocumentType::NRIC)->whereHas('verifications', function($q) {
                return $q->where('status', '!=', VerificationStatus::PENDING);
            });
        });
    }

    public function getBalancesAttribute($value)
    {
        $balances = null;
        foreach (WalletType::getList() as $walletType) {
            $wallet = $this->getWallet($walletType);
            if (!$wallet) {
                // $balances = (object)[];
                break;
            }
            $balances[WalletType::getName($walletType)] =  $wallet ? $wallet->balance : 0;
        }
        return $balances;
    }

    public function getIsPinSetAttribute($value)
    {
        return $this->pin != null;
    }
}
