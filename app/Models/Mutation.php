<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mutation extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'debit_current_balance', 'credit_current_balance'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'debit_current_balances' => 'array',
        'credit_current_balances' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function debitWallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function creditWallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function ownerWallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction');
    }
}
