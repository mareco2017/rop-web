<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PaymentProduct extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'expired_at', 'amount', 'price', 'fee', 'type_id', 'category_id', 'name', 'data'
    ];

    protected $casts = [
    	'data' => 'array'
    ];

    protected $dates = ['deleted_at'];

    public function type()
    {
        return $this->belongsTo('App\Models\PaymentProductType');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\PaymentProductCategory');
    }
}