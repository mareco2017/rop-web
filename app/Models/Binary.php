<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Binary extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'left_binary_id', 'right_binary_id', 'leader_binary_id', 'user_id', 'sponsor_user_id', 'type'
    ];
    protected $appends = [];
    protected $hidden = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function binaries()
    {
        return $this->hasMany('App\Models\Binary', 'leader_binary_id');
    }

    public function leftBinary()
    {
        return $this->belongsTo('App\Models\Binary');
    }

    public function rightBinary()
    {
        return $this->belongsTo('App\Models\Binary');
    }

    public function leaderBinary()
    {
        return $this->belongsTo('App\Models\Binary');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function sponsorUser()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function isFull()
    {
        return $this->left_binary_id && $this->right_binary_id;
    }

    public function scopeGetRoot($query)
    {
    return $query->whereNull('leader_binary_id');
    }
}
