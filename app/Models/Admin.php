<?php

namespace App\Models;

use App\Helpers\Enums\WalletType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number', 'email', 'first_name', 'last_name', 'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'fullname'
    ];

    protected $dates = [
        'deleted_at',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'ownable');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function bankAccounts()
    {
        return $this->morphMany('App\Models\BankAccount', 'ownable');
    }

    public function getFullnameAttribute($value)
    {
        return $this->last_name ? $this->first_name . ' ' . $this->last_name : $this->first_name;
    }

    public function wallets()
    {
        return $this->morphMany('App\Models\Wallet', 'ownable');
    }
    
    public function getWallet($type)
    {
        return $this->wallets()->where('type', $type)->first();
    }

    public function payWallets()
    {
        return $this->wallets()->whereIn('type', [WalletType::PRIMARY, WalletType::SECONDARY, WalletType::POINT, WalletType::FROZEN, WalletType::CREDIT]);
    }
}
