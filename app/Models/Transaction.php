<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Enums\TransactionType;

class Transaction extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_number', 'serial_number', 'description', 'total_amount', 'debit_current_balance', 'credit_current_balance', 'type', 'debit_wallet_id', 'credit_wallet_id'
    ];

    //Type -> Pay(0) / Split(1) / Transfer(2) / Top Up(3) / Withdraw(4)

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'debit_current_balances' => 'array',
        'credit_current_balances' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['related_reference_number', 'combined_serial_number'];
    protected $hidden = ['updated_at', 'debit_current_balance', 'credit_current_balance', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function relatedTransaction()
    {
        return $this->belongsTo('App\Models\Transaction');
    }

    public function relatedTransactions()
    {
        return $this->hasMany('App\Models\Transaction', 'related_transaction_id');
    }

    public function debitWallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function creditWallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function mutations()
    {
        return $this->hasMany('App\Models\Mutation');
    }

    public function scopeType($query, $type)
    {
        if (is_array($type)) {
            return $query->whereIn('type', $type);
        } else {
            return $query->where('type', $type);
        }
    }

    public function getRelatedReferenceNumberAttribute()
    {
        return $this->relatedTransaction->reference_number ?? null;
    }

    public function getCombinedSerialNumberAttribute()
    {
        if (is_null($this->serial_number)) return null;
        $code = TransactionType::getTransactionCode($this->type);
        $reference = $code.$this->created_at->format('ymd');
        $minLength = 3;

        $number = (int) $this->serial_number;

        if (strlen((string) $number) > $minLength) {
            $minLength = strlen((string) $number);
        }
        $str = $reference.str_pad($number, $minLength, "0", STR_PAD_LEFT);
        return $str;
    }
}
