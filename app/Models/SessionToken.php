<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class SessionToken extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'device_type', 'device_token', 'ip_address', 'options', 'ownable_type', 'ownable_id',
        'expired_at', 'type'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['token', 'device_token'];
    protected $dates = ['expire_at', 'deleted_at'];

    public function ownable()
    {
        return $this->morphTo();
    }

    public function scopeActive($query)
    {
        $now = Carbon::now();
        return $query->where(function($query) use ($now) {
            $query->where('expired_at', '>', $now->toDateTimeString())->orWhereNull('expired_at');
        });
    }
}
