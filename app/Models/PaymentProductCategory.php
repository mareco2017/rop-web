<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class PaymentProductCategory extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category', 'data'
    ];

    protected $casts =[
    	'data' => 'array'
    ];

    protected $appends = ['picture_url'];
    protected $dates = ['deleted_at'];

    public function paymentProducts()
    {
        return $this->hasMany('App\Models\PaymentProduct');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function picture()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function getPictureUrlAttribute($value)
    {
        if ($this->picture) {
            return Storage::disk('s3')->url($this->picture->key);
        }
        return '';
    }
}
