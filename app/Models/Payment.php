<?php

namespace App\Models;

use App\Helpers\AwsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Payment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'options', 'claimed_at', 'bank_account_id', 'reviewed_by_id', 'amount', 'statement_no'
    ];

    // protected $appends = [
    //     'cover_url'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['cover'];
    protected $dates = ['deleted_at'];
    protected $casts = [
        'options' => 'object'
    ];

    public function bankAccount()
    {
        return $this->belongsTo('App\Models\BankAccount');
    }
}
