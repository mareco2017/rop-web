<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Enums\ActiveStatus;
use App\Helpers\Enums\PaymentProductType as Type; 

class PaymentProductType extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_code', 'active'
    ];

    protected $appends = ['cover_url', 'product'];

    protected $casts =[
    	'options' => 'array'
    ];

    protected $dates = ['deleted_at'];

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function getCoverUrlAttribute($value)
    {
        if ($this->cover) {
            return Storage::disk('s3')->url($this->cover->key);
        }
        return '';
    }

    public function paymentProducts()
    {
        return $this->hasMany('App\Models\PaymentProduct');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE);
    }

    public function getProductAttribute($value)
    {
        return Type::getString($this->product_code);
    }
}
