<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order;


class OrderDetail extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'qty', 'price', 'total', 'discount', 'productable_type', 'productable_id', 'options'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['sub_total'];
    
    protected $hidden = ['deleted_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];
    
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function productable()
    {
        return $this->morphTo();
    }

    public function getSubTotalAttribute($value)
    {
        return $this->total + $this->discount;
    }
}
