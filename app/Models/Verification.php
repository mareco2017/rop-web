<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Enums\VerificationType;
use App\Helpers\Enums\VerificationStatus;

class Verification extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ownable_type', 'ownable_id', 'type', 'status', 'verifiable_type', 'verifiable_id', 'verified_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['verified_date', 'deleted_at'];

    public function ownable()
    {
        return $this->morphTo();
    }

    public function verifiable()
    {
        return $this->morphTo();
    }

    public function histories()
    {
        return $this->morphMany('App\Models\History', 'historiable');
    }

    public function scopeVerified($query, $type = VerificationType::PHONE_NUMBER)
    {
        return $query->where('type', $type)->where('status', VerificationStatus::VERIFIED);
    }

    public function scopeVerifiedOrDeclined($query, $type = VerificationType::DOCUMENT)
    {
        return $query->where('type', $type)->whereIn('status', [VerificationStatus::VERIFIED, VerificationStatus::DECLINED]);
    }

    public function isPending()
    {
        return $this->status == VerificationStatus::PENDING;
    }

    public function isVerified()
    {
        return $this->status == VerificationStatus::VERIFIED;
    }

    public function isDeclined()
    {
        return $this->status == VerificationStatus::DECLINED;
    }
}
