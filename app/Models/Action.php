<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Enums\ActionStatus;

class Action extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'objectable_type', 'objectable_id', 'userable_type', 'userable_id', 'name', 'description', 'options', 'status'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $casts = [      
        'options'=>'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function objectable()
    {
        return $this->morphTo();
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActionStatus::PENDING);
    }

    public function scopeObject($query, $object)
    {
        $id = null;
        if (!is_object($object)) {
            $className = "";
        } else {
            $className = get_class($object);
            $id = $object->id;
        }

        if (class_exists($className)) {
            $morphClass = (new $className())->getMorphClass();
        } else {
            $morphClass = "ABCD1234";
        }

        return $query->where('objectable_type', $morphClass)->where('objectable_id', $id);
    }
}
