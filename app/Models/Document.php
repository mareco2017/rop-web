<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ownable_type', 'ownable_id', 'verifiable_type', 'verifiable_id', 'type', 'description', 'identification_number'
    ];

    protected $appends = [
        'attachment_url', 'is_verified', 'is_declined', 'verification_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];
    protected $hidden = ['attachment'];
    protected $dates = ['verified_date', 'deleted_at'];

    public function ownable()
    {
        return $this->morphTo();
    }

    public function verifiable()
    {
        return $this->morphTo();
    }

    public function verifications()
    {
        return $this->morphMany('App\Models\Verification', 'verifiable');
    }

    public function attachment()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachable');
    }

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function verification()
    {
        return $this->verifications()->first();
    }

    public function getAttachmentUrlAttribute($value)
    {
        \Log::info(Storage::disk('local')->url($this->attachment->key));
        return $this->attachment->url;
    }

    public function getIsVerifiedAttribute($value)
    {
        $verification = $this->verifications()->first();
        return $verification ? $verification->isVerified() : false;
    }

    public function getIsDeclinedAttribute($value)
    {
        $verification = $this->verifications()->first();
        return $verification ? $verification->isDeclined() : false;
    }

    public function getVerificationStatusAttribute($value)
    {
        $verification = $this->verifications()->first();
        return $verification ? $verification->status : null;
    }
}
