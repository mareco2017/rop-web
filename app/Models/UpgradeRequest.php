<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Helpers\Enums\PackageType;
use App\Helpers\Enums\UpgradeRequestStatus;
use App\Models\BankAccount;

class UpgradeRequest extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package', 'price', 'options', 'status', 'requested_by_id', 'reviewed_by_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'virtual', 'status_string', 'code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object',
        'package' => 'integer'
    ];

    protected $dates = ['expired_at', 'deleted_at'];

    public function requestedBy()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function reviewedBy()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [UpgradeRequestStatus::PENDING, UpgradeRequestStatus::WAITING_FOR_PAYMENT, UpgradeRequestStatus::ON_PROCESS]);
    }

    public function scopePending($query)
    {
        return $query->where('status', UpgradeRequestStatus::PENDING);
    }

    public function isActive()
    {
        return in_array($this->status, [UpgradeRequestStatus::PENDING, UpgradeRequestStatus::WAITING_FOR_PAYMENT, UpgradeRequestStatus::ON_PROCESS]);
    }

    public function isPending()
    {
        return $this->status == UpgradeRequestStatus::PENDING;
    }

    public function isAccepted()
    {
        return $this->status == UpgradeRequestStatus::ACCEPTED;
    }

    public function isDeclined()
    {
        return $this->status == UpgradeRequestStatus::DECLINED;
    }

    public function isExpired()
    {
        if (!$this->expired_at);
        $now = Carbon::now();
        return $now->gt($this->expired_at);
    }

    public function getVirtualAttribute($value)
    {
        $options = $this->options;
        if (isset($options->dest_bank_account_id)) {
            $bankAccount = BankAccount::find($options->dest_bank_account_id);
            if ($bankAccount) $bankAccount->load('bank');
            $options->dest_bank_account = $bankAccount;
        }
        if (isset($options->bank_account_id)) {
            $bankAccount = BankAccount::find($options->bank_account_id);
            if ($bankAccount) $bankAccount->load('bank');
            $options->bank_account = $bankAccount;
        }
        return $options;
    }

    public function getStatusStringAttribute($value)
    {
        return UpgradeRequestStatus::getString($this->status);
    }

    public function getCodeAttribute($value)
    {
        $code = "UPP" . str_pad($this->id, 5, "0", STR_PAD_LEFT);
        return $code;
    }
}
